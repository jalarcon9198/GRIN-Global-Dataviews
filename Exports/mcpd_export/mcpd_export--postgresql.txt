select
            s.fao_institute_number as INSTCODE,
            coalesce(a.accession_number_part1, '') + coalesce(CONVERT(nvarchar, a.accession_number_part2), '') + coalesce(a.accession_number_part3, '') as ACCENUMB,
            (select MAX(an.plant_name) from accession_name an where an.accession_id = a.accession_id and category_code = 'COLLECTOR') as COLLNUMB,
            tg.genus_name as GENUS,
            ts.species_name as SPECIES,
            ts.species_authority as SPAUTHOR,
            ts.subspecies_name as SUBTAXA,
            ts.subspecies_authority as SUBTAUTHOR,
            cr.name as CROPNAME,
            (select top 1 an.plant_name from accession_name an where an.accession_id = a.accession_id order by an.plant_name_rank) as ACCENAME,
            convert(nvarchar, a.initial_received_date, 112) as ACQDATE,
            g.country_code as ORIGCTY,
            as_col.collector_verbatim_locality as COLLSITE,
           
            right('00' + convert(nvarchar, abs(cast(as_col.latitude as integer))), 2)
            + RIGHT('00' + convert(nvarchar, abs(CAST((as_col.latitude - cast(as_col.latitude as integer)) * 60 as integer))), 2)
            + RIGHT('00' + convert(nvarchar, abs(((as_col.latitude - CAST(as_col.latitude as integer)) * 60 - CAST((as_col.latitude - cast(as_col.latitude as integer)) * 60        as integer)) * 60)), 2)
            + case when as_col.latitude > 0 then 'N' else 'S' end
            as LATITUDE,
           
            + RIGHT('00' + convert(nvarchar, abs(CAST(as_col.longitude as integer))), 2)
            + RIGHT('00' + convert(nvarchar, abs(CAST((as_col.longitude - cast(as_col.longitude as integer)) * 60 as integer))), 2)
            + RIGHT('00' + convert(nvarchar, abs(((as_col.longitude - CAST(as_col.longitude as integer)) * 60 - CAST((as_col.longitude - cast(as_col.longitude as integer)) * 60        as integer)) * 60)), 2)
            + case when as_col.longitude > 0 then 'E' else 'W' end
            as LONGITUDE,
            as_col.elevation_meters as ELEVATION,
            convert(nvarchar, as_col.source_date, 112) as COLLDATE,
            (select top 1 s_bred.fao_institute_number from accession_source as_bred inner join accession_source_map asm_bred
                     on as_bred.accession_source_id = asm_bred.accession_source_id
                     inner join cooperator c_bred on asm_bred.cooperator_id = c_bred.cooperator_id
                     inner join site s_bred on c_bred.site_id = s_bred.site_id where as_bred.source_type_code = 'DEVELOPED') as BREDCODE,
            a.improvement_status_code as SAMPSTAT,
            ap.description as ANCEST,
            as_col.acquisition_source_code as COLLSRC,
            as_don.source_type_code as DONORCODE,
            (select top 1 sd.fao_institute_number from site sd inner join cooperator cd on sd.site_id = cd.site_id inner join accession_source_map asm on cd.cooperator_id = asm.cooperator_id where asm.accession_source_id = as_don.accession_source_id) as DONORNUMB,
            (select top 1 so.fao_institute_number from site so inner join cooperator co on so.site_id = co.site_id inner join accession_source_map asm2 on co.cooperator_id = asm2.cooperator_id inner join accession_source aso on asm2.accession_source_id = aso.accession_source_id where aso.accession_id = a.accession_id and aso.source_type_code <> 'DONATED') as OTHERNUMB,
            (select top 1 sbl.fao_institute_number from site sbl where sbl.site_id = a.backup_location1_site_id) as DUPLSITE,
            (select top 1 case when s_s.type_code = 'BOTH' then 'SEED;CLONAL' when s_s.type_code = 'CLNL' then 'CLONAL' when s_s.type_code is not null then 'SEED' else null end from site s_s where s_s.site_id = c.site_id) as STORAGE,
            (select top 1 aa_rem.note from accession_action aa_rem where aa_rem.accession_id = a.accession_id and action_name_code = 'FAO_REMARKS') as REMARKS
from
            accession a left join cooperator c
                        on a.owned_by = c.cooperator_id
            left join site s
                        on c.site_id = s.site_id
            left join taxonomy_species ts on a.taxonomy_species_id = ts.taxonomy_species_id
            left join taxonomy_genus tg on ts.taxonomy_genus_id = tg.taxonomy_genus_id
            left join taxonomy_family tf on tg.taxonomy_family_id = tf.taxonomy_family_id
            left join taxonomy_crop_map tcm on ts.taxonomy_species_id = tcm.taxonomy_species_id
            left join crop cr on tcm.crop_id = cr.crop_id
            left join accession_source as_col on a.accession_id = as_col.accession_id and as_col.source_type_code = 'COLLECTED'
            left join geography g on as_col.geography_id = g.geography_id
            left join accession_pedigree ap on a.accession_id = ap.accession_id
            left join accession_source as_don on a.accession_id = as_don.accession_id and as_don.source_type_code = 'DONATED'
order by 2


