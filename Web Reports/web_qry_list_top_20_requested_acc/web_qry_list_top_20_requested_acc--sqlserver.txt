select top 50 acc.acp,acc.acno,acc.taxon,topname,count(*) as times_shipped  from topname t,
   acc,oi,ord
    where ord.orno = oi.orno and acc.acid=oi.acid and acc.acid=t.acid
    and ortype in ('DI','RP') and
     oi.status in ('INSPECT','PSHIP','SHIPPED') and year(oi.acted) between :start_year and :end_year  
and genus = :genus and ord.site = :site group by 
	 acc.acp,acc.acno,acc.taxon,topname
	 order by 5 desc