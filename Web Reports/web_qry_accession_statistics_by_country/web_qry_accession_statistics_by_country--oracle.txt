SELECT cvl.title as country, s.site_short_name,count(distinct a.accession_id) as accession_count
  FROM accession a, accession_source asr, site s, cooperator c, geography g, code_value cv,code_value_lang cvl
  WHERE a.accession_id = asr.accession_id and asr.geography_id = g.geography_id and 
    a.owned_by = c.cooperator_id and c.site_id = s.site_id and asr.is_origin = 'Y'
    and g.country_code = cv.value and cv.group_name = 'GEOGRAPHY_COUNTRY_CODE'
and cv.code_value_id = cvl.code_value_id and cvl.sys_lang_id = 1
    and g.country_code not between '000' and '999' and s.site_short_name like :site_short_name 
  GROUP BY  cvl.title, s.site_short_name
  ORDER BY  cvl.title, s.site_short_name