SELECT DISTINCT c.cooperator_id,
       LTRIM(RTRIM(COALESCE(c.last_name, '') + ', ' + 
        COALESCE(c.first_name, '') + ', ' + COALESCE(c.organization, ''))) AS cooperator,s.site_short_name,
       g.country_code,g.adm1,c.city, asr.source_type_code, YEAR(asr.source_date) as srcdate,
       LTRIM(RTRIM(LTRIM(COALESCE(a.accession_number_part1, '') + ' ') + 
       LTRIM(COALESCE(convert(varchar, a.accession_number_part2), '') + ' ') + 
       COALESCE(a.accession_number_part3, ''))) AS accession,
concat('<nobr><a onclick="javascript:return true;" href="../accessiondetail.aspx?id=', convert(nvarchar, a.accession_id), '">',
 trim(CONCAT (COALESCE (a.accession_number_part1, ''), ' ', COALESCE (CONVERT(nvarchar, a.accession_number_part2), ''), ' ', 
COALESCE (a.accession_number_part3, ''))), '</a></nobr>') as pi_number,
       ts.name as taxon
  FROM cooperator c 
	   Left JOIN accession_source_map asm
	    on asm.cooperator_id = c.cooperator_id
	   LEFT JOIN accession_source asr
	    on asr.accession_source_id = asm.accession_source_id
	   LEFT JOIN accession a
	    on asr.accession_id = a.accession_id
	   LEFT JOIN cooperator c2
	    on c2.cooperator_id = a.owned_by
	   LEFT JOIN taxonomy_species ts
	    on a.taxonomy_species_id = ts.taxonomy_species_id 
	   LEFT JOIN geography g
	    on g.geography_id = c.geography_id
	   LEFT JOIN site s
	    on c2.site_id = s.site_id
  WHERE c.cooperator_id in (select current_cooperator_id from cooperator c3 where c3.last_name like :last_name) 
  ORDER BY cooperator,srcdate,asr.source_type_code,accession