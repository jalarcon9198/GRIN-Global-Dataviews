SELECT 
  (SELECT site_short_name FROM site WHERE site_id IN (SELECT site_id FROM cooperator WHERE cooperator_id = o.owned_by)) AS site, 
  o.order_request_id,
  o.order_type_code,
  o.ordered_date,
  o.completed_date,
  CASE WHEN icnt.non_cancel_count = 0
       THEN 'CANCEL'
	   ELSE CASE WHEN icnt.status_count = 1
	             THEN only_status
				 ELSE 'MIX'
				 END
	   END AS status,
  CASE WHEN EXISTS (SELECT * FROM order_request_action
                      WHERE order_request_id = o.order_request_id
                        AND action_name_code = 'SMTAACCEPT' ) 
       THEN 'YES'
       ELSE 'NO'
    END AS SMTA_accepted,
  CASE WHEN EXISTS (SELECT * FROM order_request_item
                       INNER JOIN inventory ON order_request_item.inventory_id = inventory.inventory_id
                       INNER JOIN accession_ipr ON accession_ipr.accession_id = inventory.accession_id 
                                               AND accession_ipr.type_code = 'MTA-SMTA'
                      WHERE order_request_id = o.order_request_id
                        AND COALESCE(status_code, 'NEW') <> 'CANCEL')  
       THEN 'NO'
       ELSE 'YES'
    END AS SMTA_canceled,
  COALESCE(c.last_name,'') + ', ' + COALESCE(c.first_name,'') + ', ' + COALESCE(c.organization,'') AS recipient
FROM order_request o
    INNER JOIN cooperator c ON o.final_recipient_cooperator_id = c.cooperator_id
    INNER JOIN (
	    SELECT
            order_request_id,
            --(SELECT count(order_request_item_id) from order_request_item WHERE order_request_id = ord.order_request_id ) AS item_count,
            (SELECT count(order_request_item_id) from order_request_item WHERE order_request_id = ord.order_request_id AND status_code != 'CANCEL') AS non_cancel_count,
            (SELECT count(distinct status_code) FROM order_request_item WHERE ord.order_request_id = order_request_id AND status_code != 'CANCEL') AS status_count,
            (SELECT TOP 1 status_code FROM order_request_item
	                             WHERE ord.order_request_id = order_request_id AND status_code != 'CANCEL') AS only_status
          FROM order_request ord
    ) icnt ON o.order_request_id = icnt.order_request_id
WHERE o.order_type_code in ('DI','RP')
  AND o.order_request_id IN 
      (SELECT distinct order_request_id
	     FROM order_request_item
		 INNER JOIN inventory ON order_request_item.inventory_id = inventory.inventory_id
		 INNER JOIN accession_ipr ON accession_ipr.accession_id = inventory.accession_id
		 WHERE order_request_item.order_request_id = o.order_request_id
		   AND type_code = 'MTA-SMTA') 
ORDER BY 1,2