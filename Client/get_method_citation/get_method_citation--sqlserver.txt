SELECT
  c.citation_id,
  c.method_id,
  c.type_code,
  c.author_name,
  c.citation_year,
  c.title,
  c.citation_title,
  l.abbreviation,
  c.reference,
  c.literature_id,
  c.doi_reference,
  c.url,
  c.description,
  c.note,
  c.created_date,
  c.created_by,
  c.modified_date,
  c.modified_by,
  c.owned_date,
  c.owned_by
FROM
    citation c
    LEFT JOIN literature l ON c.literature_id = l.literature_id
    INNER JOIN (
        SELECT citation_id FROM citation WHERE citation_id IN (:citationid)
        UNION SELECT citation_id FROM citation WHERE method_id IN (:methodid)
        UNION SELECT citation_id FROM citation WHERE literature_id IN (:literatureid) AND method_id IS NOT NULL
        UNION SELECT citation_id FROM citation
            INNER JOIN method ON citation.method_id = method.method_id
            WHERE method.geography_id IN (:geographyid)
     ) list on list.citation_id = c.citation_id AND method_id IS NOT NULL