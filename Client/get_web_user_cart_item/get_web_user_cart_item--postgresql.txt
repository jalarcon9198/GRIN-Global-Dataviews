SELECT
  wuci.web_user_cart_item_id,
  wuci.web_user_cart_id,
  wuci.accession_id,
  wuci.quantity,
  wuci.form_type_code,
  wuci.usage_code,
  wuci.note,
  wu.user_name,
  wc.first_name,
  wc.last_name,
  (SELECT taxonomy_species_id FROM accession WHERE accession_id = wuci.accession_id) AS taxonomy_species_id,
  wuci.created_date,
  wuci.created_by,
  wuci.modified_date,
  wuci.modified_by,
  wuci.owned_date,
  wuci.owned_by
FROM
    web_user_cart_item wuci
    INNER JOIN web_user_cart wuc ON wuci.web_user_cart_id = wuc.web_user_cart_id
    INNER JOIN web_user wu ON wuc.web_user_id = wu.web_user_id
    LEFT JOIN web_cooperator wc ON wu.web_cooperator_id = wc.web_cooperator_id
    INNER JOIN (
	SELECT web_user_cart_item_id FROM web_user_cart_item
            WHERE web_user_cart_item_id in (:webusercartitemid)

	UNION SELECT web_user_cart_item_id FROM web_user_cart_item
            WHERE web_user_cart_id in (:webusercartid)

	UNION SELECT web_user_cart_item_id FROM web_user_cart_item
            WHERE accession_id in (:accessionid)

    ) list ON wuci.web_user_cart_item_id = list.web_user_cart_item_id
