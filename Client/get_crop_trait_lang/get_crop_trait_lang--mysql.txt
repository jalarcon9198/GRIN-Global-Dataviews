SELECT
  ctl.crop_trait_lang_id,
  ct.crop_id,
  ctl.crop_trait_id,
  ctl.sys_lang_id,
  ctl.title,
  ctl.description,
  ctl.created_date,
  ctl.created_by,
  ctl.modified_date,
  ctl.modified_by,
  ctl.owned_date,
  ctl.owned_by
FROM
    crop_trait_lang ctl
    INNER JOIN crop_trait ct ON ct.crop_trait_id = ctl.crop_trait_id
    INNER JOIN (
	SELECT crop_trait_lang_id FROM crop_trait_lang
            WHERE crop_trait_lang_id in (:croptraitlangid)

        UNION SELECT crop_trait_lang_id FROM crop_trait_lang
            WHERE crop_trait_id in (:croptraitid)

	UNION SELECT crop_trait_lang_id FROM crop_trait_lang ctl
            INNER JOIN crop_trait ct ON ct.crop_trait_id = ctl.crop_trait_id
            WHERE ct.crop_id in (:cropid)

	UNION SELECT crop_trait_lang_id FROM crop_trait_lang ctl
            INNER JOIN crop_trait ct ON ct.crop_trait_id = ctl.crop_trait_id
            INNER JOIN crop_trait_observation cto ON cto.crop_trait_id = ct.crop_trait_id
            WHERE cto.inventory_id in (:inventoryid)

	UNION SELECT crop_trait_lang_id FROM crop_trait_lang ctl
            INNER JOIN crop_trait ct ON ct.crop_trait_id = ctl.crop_trait_id
            INNER JOIN crop_trait_observation cto ON cto.crop_trait_id = ct.crop_trait_id
            WHERE cto.crop_trait_observation_id in (:croptraitobservationid)

	UNION SELECT crop_trait_lang_id FROM crop_trait_lang ctl
            INNER JOIN crop_trait ct ON ct.crop_trait_id = ctl.crop_trait_id
            INNER JOIN crop_trait_observation cto ON cto.crop_trait_id = ct.crop_trait_id
            WHERE cto.crop_trait_code_id in (:croptraitcodeid)

	UNION SELECT crop_trait_lang_id FROM crop_trait_lang ctl
            INNER JOIN crop_trait ct ON ct.crop_trait_id = ctl.crop_trait_id
            INNER JOIN crop_trait_observation cto ON cto.crop_trait_id = ct.crop_trait_id
            INNER JOIN inventory i ON i.inventory_id = cto.inventory_id
            WHERE i.accession_id in (:accessionid)

	UNION SELECT crop_trait_lang_id FROM crop_trait_lang ctl
            INNER JOIN crop_trait ct ON ct.crop_trait_id = ctl.crop_trait_id
            INNER JOIN crop_trait_observation cto ON cto.crop_trait_id = ct.crop_trait_id
            INNER JOIN order_request_item ori ON ori.inventory_id = cto.inventory_id
            WHERE ori.order_request_id in (:orderrequestid)

    ) list ON list.crop_trait_lang_id = ctl.crop_trait_lang_id
