SELECT     
   ip.accession_ipr_id
  ,ip.accession_id
  ,(select taxonomy_species_id from accession acc where ip.accession_id = acc.accession_id) as taxonomy_species_id
  ,ip.type_code
  ,ip.ipr_number 
  ,ip.ipr_crop_name
  ,ip.ipr_full_name
  ,ip.issued_date
  ,ip.expired_date
  ,ip.accepted_date
  ,ip.expected_date
  ,ip.cooperator_id
  ,(select geography_id from cooperator coop where ip.cooperator_id = coop.cooperator_id) as geography_id
  ,ip.note
  ,ip.created_date
  ,ip.created_by
  ,ip.modified_date
  ,ip.modified_by
  ,ip.owned_date
  ,ip.owned_by

FROM accession_ipr ip

WHERE
  ip.accession_ipr_id in (:accessioniprid)

UNION
SELECT     
   ip.accession_ipr_id
  ,ip.accession_id
  ,(select taxonomy_species_id from accession acc where ip.accession_id = acc.accession_id) as taxonomy_id
  ,ip.type_code
  ,ip.ipr_number 
  ,ip.ipr_crop_name
  ,ip.ipr_full_name
  ,ip.issued_date
  ,ip.expired_date
  ,ip.accepted_date
  ,ip.expected_date
  ,ip.cooperator_id
  ,(select geography_id from cooperator coop where ip.cooperator_id = coop.cooperator_id) as geography_id
  ,ip.note
  ,ip.created_date
  ,ip.created_by
  ,ip.modified_date
  ,ip.modified_by
  ,ip.owned_date
  ,ip.owned_by

FROM accession_ipr ip

WHERE
  ip.accession_id in (:accessionid)

UNION

SELECT     
   ip.accession_ipr_id
  ,ip.accession_id
  ,(select taxonomy_species_id from accession acc where ip.accession_id = acc.accession_id) as taxonomy_id
  ,ip.type_code
  ,ip.ipr_number 
  ,ip.ipr_crop_name
  ,ip.ipr_full_name
  ,ip.issued_date
  ,ip.expired_date
  ,ip.accepted_date
  ,ip.expected_date
  ,ip.cooperator_id
  ,(select geography_id from cooperator coop where ip.cooperator_id = coop.cooperator_id) as geography_id
  ,ip.note
  ,ip.created_date
  ,ip.created_by
  ,ip.modified_date
  ,ip.modified_by
  ,ip.owned_date
  ,ip.owned_by
FROM
  accession_ipr ip, inventory i
WHERE
  ip.accession_id = i.accession_id
  and i.inventory_id in (:inventoryid)

UNION 

SELECT     
   ip.accession_ipr_id
  ,ip.accession_id
  ,(select taxonomy_species_id from accession acc where ip.accession_id = acc.accession_id) as taxonomy_id
  ,ip.type_code
  ,ip.ipr_number 
  ,ip.ipr_crop_name
  ,ip.ipr_full_name
  ,ip.issued_date
  ,ip.expired_date
  ,ip.accepted_date
  ,ip.expected_date
  ,ip.cooperator_id
  ,(select geography_id from cooperator coop where ip.cooperator_id = coop.cooperator_id) as geography_id
  ,ip.note
  ,ip.created_date
  ,ip.created_by
  ,ip.modified_date
  ,ip.modified_by
  ,ip.owned_date
  ,ip.owned_by

FROM
  accession_ipr ip, order_request_item oi, inventory i

where
  oi.inventory_id = i.inventory_id
  and ip.accession_id = i.accession_id
  and oi.order_request_id in (:orderrequestid)


UNION


SELECT     
   ip.accession_ipr_id
  ,ip.accession_id
  ,(select taxonomy_species_id from accession acc where ip.accession_id = acc.accession_id) as taxonomy_id
  ,ip.type_code
  ,ip.ipr_number 
  ,ip.ipr_crop_name
  ,ip.ipr_full_name
  ,ip.issued_date
  ,ip.expired_date
  ,ip.accepted_date
  ,ip.expected_date
  ,ip.cooperator_id
  ,(select geography_id from cooperator coop where ip.cooperator_id = coop.cooperator_id) as geography_id
  ,ip.note
  ,ip.created_date
  ,ip.created_by
  ,ip.modified_date
  ,ip.modified_by
  ,ip.owned_date
  ,ip.owned_by
FROM 
  accession_ipr ip 
	inner join accession a
	on ip.accession_id = a.accession_id
  inner join taxonomy_species ts
	on a.taxonomy_species_id = ts.taxonomy_species_id
WHERE
  ts.taxonomy_genus_id in (:taxonomygenusid)

UNION


SELECT     
   ip.accession_ipr_id
  ,ip.accession_id
  ,(select taxonomy_species_id from accession acc where ip.accession_id = acc.accession_id) as taxonomy_id
  ,ip.type_code
  ,ip.ipr_number 
  ,ip.ipr_crop_name
  ,ip.ipr_full_name
  ,ip.issued_date
  ,ip.expired_date
  ,ip.accepted_date
  ,ip.expected_date
  ,ip.cooperator_id
  ,(select geography_id from cooperator coop where ip.cooperator_id = coop.cooperator_id) as geography_id
  ,ip.note
  ,ip.created_date
  ,ip.created_by
  ,ip.modified_date
  ,ip.modified_by
  ,ip.owned_date
  ,ip.owned_by
FROM 
  accession_ipr ip 
	inner join accession a
	on ip.accession_id = a.accession_id
  inner join taxonomy_species ts
	on a.taxonomy_species_id = ts.taxonomy_species_id
  inner join taxonomy_crop_map tcm
	on ts.taxonomy_species_id = tcm.taxonomy_species_id
WHERE
  tcm.crop_id in (:cropid)