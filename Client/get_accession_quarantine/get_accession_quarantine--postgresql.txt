SELECT  q.accession_quarantine_id
    ,q.accession_id
    ,(SELECT taxonomy_species_id FROM accession acc WHERE (q.accession_id = acc.accession_id)) AS taxonomy_species_id
    ,q.quarantine_type_code
    ,q.progress_status_code
    ,q.custodial_cooperator_id
    ,(select geography_id from cooperator coop where q.custodial_cooperator_id = coop.cooperator_id) as geography_id
    ,q.entered_date
    ,q.established_date
    ,q.expected_release_date
    ,q.released_date
    ,q.note
    ,q.created_date
    ,q.created_by
    ,q.modified_date
    ,q.modified_by
    ,q.owned_date
    ,q.owned_by
FROM   accession_quarantine q
WHERE  q.accession_quarantine_id in (:accessionquarantineid)

UNION
SELECT  q.accession_quarantine_id
    ,q.accession_id
    ,(SELECT taxonomy_species_id FROM accession acc WHERE (q.accession_id = acc.accession_id)) AS taxonomy_id
    ,q.quarantine_type_code
    ,q.progress_status_code
    ,q.custodial_cooperator_id
    ,(select geography_id from cooperator coop where q.custodial_cooperator_id = coop.cooperator_id) as geography_id
    ,q.entered_date
    ,q.established_date
    ,q.expected_release_date
    ,q.released_date
    ,q.note
    ,q.created_date
    ,q.created_by
    ,q.modified_date
    ,q.modified_by
    ,q.owned_date
    ,q.owned_by
FROM   accession_quarantine q
WHERE  q.accession_id in (:accessionid)

UNION

SELECT  q.accession_quarantine_id
    ,q.accession_id
    ,(SELECT taxonomy_species_id FROM accession acc WHERE (q.accession_id = acc.accession_id)) AS taxonomy_id
    ,q.quarantine_type_code
    ,q.progress_status_code
    ,q.custodial_cooperator_id
    ,(select geography_id from cooperator coop where q.custodial_cooperator_id = coop.cooperator_id) as geography_id
    ,q.entered_date
    ,q.established_date
    ,q.expected_release_date
    ,q.released_date
    ,q.note
    ,q.created_date
    ,q.created_by
    ,q.modified_date
    ,q.modified_by
    ,q.owned_date
    ,q.owned_by

FROM  accession_quarantine q, inventory i
WHERE  q.accession_id = i.accession_id 
       and i.inventory_id in (:inventoryid)

UNION

SELECT  q.accession_quarantine_id
    ,q.accession_id
    ,(SELECT taxonomy_species_id FROM accession acc WHERE (q.accession_id = acc.accession_id)) AS taxonomy_id
    ,q.quarantine_type_code
    ,q.progress_status_code
    ,q.custodial_cooperator_id
    ,(select geography_id from cooperator coop where q.custodial_cooperator_id = coop.cooperator_id) as geography_id
    ,q.entered_date
    ,q.established_date
    ,q.expected_release_date
    ,q.released_date
    ,q.note
    ,q.created_date
    ,q.created_by
    ,q.modified_date
    ,q.modified_by
    ,q.owned_date
    ,q.owned_by

FROM accession_quarantine q, inventory i, order_request_item oi
WHERE  q.accession_id = i.accession_id
       and i.inventory_id = oi.inventory_id
       and oi.order_request_id in (:orderrequestid)

UNION


SELECT  q.accession_quarantine_id
    ,q.accession_id
    ,a.taxonomy_species_id AS taxonomy_id
    ,q.quarantine_type_code
    ,q.progress_status_code
    ,q.custodial_cooperator_id
    ,(select geography_id from cooperator coop where q.custodial_cooperator_id = coop.cooperator_id) as geography_id
    ,q.entered_date
    ,q.established_date
    ,q.expected_release_date
    ,q.released_date
    ,q.note
    ,q.created_date
    ,q.created_by
    ,q.modified_date
    ,q.modified_by
    ,q.owned_date
    ,q.owned_by

FROM  accession_quarantine q
	inner join accession a
		on q.accession_id = a.accession_id
	inner join taxonomy_species ts
		on a.taxonomy_species_id = ts.taxonomy_species_id
WHERE  
       ts.taxonomy_genus_id in (:taxonomygenusid)


UNION
SELECT  q.accession_quarantine_id
    ,q.accession_id
    ,a.taxonomy_species_id AS taxonomy_id
    ,q.quarantine_type_code
    ,q.progress_status_code
    ,q.custodial_cooperator_id
    ,(select geography_id from cooperator coop where q.custodial_cooperator_id = coop.cooperator_id) as geography_id
    ,q.entered_date
    ,q.established_date
    ,q.expected_release_date
    ,q.released_date
    ,q.note
    ,q.created_date
    ,q.created_by
    ,q.modified_date
    ,q.modified_by
    ,q.owned_date
    ,q.owned_by

FROM  accession_quarantine q
	inner join accession a
		on q.accession_id = a.accession_id
	inner join taxonomy_species ts
		on a.taxonomy_species_id = ts.taxonomy_species_id
	inner join taxonomy_crop_map tcm
		on tcm.taxonomy_species_id = ts.taxonomy_species_id
WHERE  
       tcm.crop_id in (:cropid)
