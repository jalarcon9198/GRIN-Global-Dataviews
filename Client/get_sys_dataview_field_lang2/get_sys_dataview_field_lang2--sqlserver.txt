SELECT
  sdfl.sys_dataview_field_lang_id,
  sdf.sys_dataview_id,
  sdfl.sys_dataview_field_id,
  sdfl.sys_lang_id,
  sdfl.title,
  sdfl.description,
  sdfl.created_date,
  sdfl.created_by,
  sdfl.modified_date,
  sdfl.modified_by,
  sdfl.owned_date,
  sdfl.owned_by
FROM
    sys_dataview_field_lang sdfl
    LEFT JOIN sys_dataview_field sdf
      ON  sdfl.sys_dataview_field_id = sdf.sys_dataview_field_id 
    INNER JOIN (
        SELECT sys_dataview_field_lang_id FROM sys_dataview_field_lang
            WHERE sys_dataview_field_lang_id IN (:sysdataviewfieldlangid)

        UNION SELECT sys_dataview_field_lang_id FROM sys_dataview_field_lang
            WHERE sys_dataview_field_id IN (:sysdataviewfieldid)

        UNION SELECT sys_dataview_field_lang_id FROM sys_dataview_field_lang
            WHERE sys_lang_id IN (:syslangid)

        UNION SELECT sys_dataview_field_lang_id FROM sys_dataview_field_lang
            INNER JOIN sys_dataview_field ON sys_dataview_field_lang.sys_dataview_field_id = sys_dataview_field.sys_dataview_field_id
            WHERE sys_dataview_id IN (:sysdataviewid)

    ) list ON list.sys_dataview_field_lang_id = sdfl.sys_dataview_field_lang_id
