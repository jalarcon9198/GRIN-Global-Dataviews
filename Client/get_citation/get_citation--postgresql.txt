SELECT
  c.citation_id,
  c.literature_id,
  c.citation_title,
  c.author_name,
  c.citation_year,
  c.reference,
  c.doi_reference,
  c.url,
  c.title,
  c.description,
  c.accession_id,
  c.method_id,
  c.taxonomy_species_id,
  c.taxonomy_genus_id,
  c.taxonomy_family_id,
  c.accession_ipr_id,
  c.accession_pedigree_id,
  c.genetic_marker_id,
  c.type_code,
  c.unique_key,
  c.note,
  c.created_date,
  c.created_by,
  c.modified_date,
  c.modified_by,
  c.owned_date,
  c.owned_by
FROM citation c
    INNER JOIN (
        SELECT citation_id FROM citation WHERE citation_id IN (:citationid)

        UNION SELECT citation_id FROM citation WHERE literature_id IN (:literatureid)
        UNION SELECT citation_id FROM citation WHERE accession_id IN (:accessionid)
        UNION SELECT citation_id FROM citation WHERE method_id in (:methodid)
        UNION SELECT citation_id FROM citation WHERE taxonomy_species_id in (:taxonomyspeciesid)
        UNION SELECT citation_id FROM citation WHERE taxonomy_genus_id in (:taxonomygenusid)
        UNION SELECT citation_id FROM citation WHERE taxonomy_family_id in (:taxonomyfamilyid)
        UNION SELECT citation_id FROM citation WHERE accession_ipr_id in (:accessioniprid)
        UNION SELECT citation_id FROM citation WHERE accession_pedigree_id in (:accessionpedigreeid)
        UNION SELECT citation_id FROM citation WHERE genetic_marker_id in (:geneticmarkerid)

        UNION SELECT DISTINCT citation_id
            FROM citation
            INNER JOIN inventory i ON i.accession_id = citation.accession_id
            WHERE i.inventory_id IN (:inventoryid)

        UNION SELECT DISTINCT citation_id
            FROM citation
            INNER JOIN inventory i ON i.accession_id = citation.accession_id
            INNER JOIN order_request_item ori ON ori.inventory_id = i.inventory_id
            WHERE ori.order_request_id IN (:orderrequestid)

     ) list on list.citation_id = c.citation_id