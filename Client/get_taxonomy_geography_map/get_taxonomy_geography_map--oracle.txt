SELECT
  tgm.taxonomy_geography_map_id,
  tgm.taxonomy_species_id,
  tgm.geography_id,
  tgm.geography_status_code,
  tgm.citation_id,
  r.continent,
  r.subcontinent,
  dbms_lob.substr(tgm.note,32767,1) as note,
  tgm.created_date,
  tgm.created_by,
  tgm.modified_date,
  tgm.modified_by,
  tgm.owned_date,
  tgm.owned_by
FROM
    taxonomy_geography_map tgm
    LEFT JOIN geography_region_map grm ON grm.geography_id = tgm.geography_id 
    LEFT JOIN region r ON grm.region_id = r.region_id
    INNER JOIN (
        SELECT taxonomy_geography_map_id FROM taxonomy_geography_map WHERE taxonomy_geography_map_id IN (:taxonomygeographymapid)

        UNION SELECT taxonomy_geography_map_id FROM taxonomy_geography_map
            WHERE taxonomy_species_id in (:taxonomyspeciesid)

        UNION SELECT taxonomy_geography_map_id FROM taxonomy_geography_map
            WHERE geography_id in (:geographyid)

        UNION SELECT taxonomy_geography_map_id FROM taxonomy_geography_map
            WHERE citation_id in (:citationid)

        UNION SELECT taxonomy_geography_map_id FROM taxonomy_geography_map
            INNER JOIN citation ON taxonomy_geography_map.citation_id = citation.citation_id
            WHERE literature_id in (:literatureid)

        UNION SELECT DISTINCT taxonomy_geography_map_id FROM taxonomy_geography_map
            INNER JOIN accession ON taxonomy_geography_map.taxonomy_species_id = accession.taxonomy_species_id
            WHERE accession.accession_id IN (:accessionid)

        UNION SELECT DISTINCT taxonomy_geography_map_id FROM taxonomy_geography_map
            INNER JOIN accession ON taxonomy_geography_map.taxonomy_species_id = accession.taxonomy_species_id
            INNER JOIN inventory ON accession.accession_id = inventory.accession_id
            WHERE inventory.inventory_id IN (:inventoryid)

        UNION SELECT DISTINCT taxonomy_geography_map_id FROM taxonomy_geography_map
            INNER JOIN taxonomy_species ON taxonomy_geography_map.taxonomy_species_id = taxonomy_species.taxonomy_species_id
            WHERE taxonomy_species.taxonomy_genus_id in (:taxonomygenusid)

     ) list on list.taxonomy_geography_map_id = tgm.taxonomy_geography_map_id
