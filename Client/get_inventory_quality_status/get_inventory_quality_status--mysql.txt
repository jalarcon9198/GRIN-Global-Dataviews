SELECT	iqs.inventory_quality_status_id,
	iqs.inventory_id,
	iqs.test_type_code,
	iqs.contaminant_code,
        iqs.plant_part_tested_code,
	iqs.test_result_code,
        iqs.test_results_score,
        iqs.test_results_score_type_code,
        iqs.started_date_code,
	iqs.started_date,
        iqs.completed_date_code,
	iqs.completed_date,
	iqs.required_replication_count,
	iqs.started_count,
	iqs.completed_count,
        iqs.replicate,
        iqs.plate_or_assay_number,
	iqs.method_id,
	iqs.tester_cooperator_id,
	iqs.note,
	i.accession_id,
	(SELECT taxonomy_species_id FROM accession WHERE accession_id = i.accession_id) AS taxonomy_species_id,
	iqs.created_date,
	iqs.created_by,
	iqs.modified_date,
	iqs.modified_by,
	iqs.owned_date,
	iqs.owned_by
FROM inventory_quality_status iqs
	INNER JOIN inventory i ON i.inventory_id = iqs.inventory_id
	INNER JOIN (
		SELECT inventory_quality_status_id
			FROM inventory_quality_status WHERE inventory_quality_status_id IN (:inventoryqualitystatusid)

		UNION SELECT inventory_quality_status_id
			FROM inventory_quality_status WHERE inventory_id IN (:inventoryid)

		UNION SELECT inventory_quality_status_id
			FROM inventory_quality_status iqs
			INNER JOIN inventory i ON i.inventory_id = iqs.inventory_id
			WHERE i.accession_id IN (:accessionid)

		UNION SELECT inventory_quality_status_id
			FROM inventory_quality_status iqs
			INNER JOIN inventory i ON i.inventory_id = iqs.inventory_id
			WHERE inventory_maint_policy_id IN (:inventorymaintpolicyid)

		UNION SELECT inventory_quality_status_id
			FROM inventory_quality_status iqs
			INNER JOIN  order_request_item ori ON ori.inventory_id = iqs.inventory_id
			WHERE ori.order_request_id IN (:orderrequestid)

		UNION SELECT inventory_quality_status_id
			FROM inventory_quality_status iqs
			INNER JOIN inventory i ON i.inventory_id = iqs.inventory_id
			INNER JOIN accession a ON i.accession_id = a.accession_id
			INNER JOIN taxonomy_species ts ON ts.taxonomy_species_id = a.taxonomy_species_id
			WHERE  ts.taxonomy_genus_id IN (:taxonomygenusid)
	) list ON iqs.inventory_quality_status_id = list.inventory_quality_status_id