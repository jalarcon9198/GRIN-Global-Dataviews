SELECT
  ctcl.crop_trait_code_lang_id,
  ct.crop_id,
  ctc.crop_trait_id,
  ct.coded_name,
  ctcl.crop_trait_code_id,
  ctl.title AS trait_title,
  ctl.description AS trait_description,
  ctcl.sys_lang_id,
  ctcl.title,
  ctcl.description,
  ctcl.created_date,
  ctcl.created_by,
  ctcl.modified_date,
  ctcl.modified_by,
  ctcl.owned_date,
  ctcl.owned_by
FROM
    crop_trait_code_lang ctcl
    INNER JOIN crop_trait_code ctc ON ctc.crop_trait_code_id = ctcl.crop_trait_code_id
    INNER JOIN crop_trait      ct  ON ct.crop_trait_id = ctc.crop_trait_id
    LEFT JOIN crop_trait_lang  ctl ON ctl.crop_trait_id = ct.crop_trait_id
                                      AND ctl.sys_lang_id = __LANGUAGEID__
    INNER JOIN (
	SELECT crop_trait_code_lang_id FROM crop_trait_code_lang
            WHERE crop_trait_code_lang_id in (:croptraitcodelangid)

	UNION SELECT crop_trait_code_lang_id FROM crop_trait_code_lang
            WHERE crop_trait_code_id in (:croptraitcodeid)

	UNION SELECT crop_trait_code_lang_id FROM crop_trait_code_lang ctcl
            INNER JOIN crop_trait_code ctc ON ctc.crop_trait_code_id = ctcl.crop_trait_code_id
            WHERE ctc.crop_trait_id in (:croptraitid)

	UNION SELECT crop_trait_code_lang_id FROM crop_trait_code_lang ctcl
            INNER JOIN crop_trait_code ctc ON ctc.crop_trait_code_id = ctcl.crop_trait_code_id
            INNER JOIN crop_trait ct ON ct.crop_trait_id = ctc.crop_trait_id
            WHERE ct.crop_id in (:cropid)

	UNION SELECT crop_trait_code_lang_id FROM crop_trait_code_lang ctcl
            INNER JOIN crop_trait_code ctc ON ctc.crop_trait_code_id = ctcl.crop_trait_code_id
            INNER JOIN crop_trait_observation cto ON cto.crop_trait_code_id = ctc.crop_trait_code_id
            WHERE cto.crop_trait_observation_id in (:croptraitobservationid)

	UNION SELECT crop_trait_code_lang_id FROM crop_trait_code_lang ctcl
            INNER JOIN crop_trait_code ctc ON ctc.crop_trait_code_id = ctcl.crop_trait_code_id
            INNER JOIN crop_trait_observation cto ON cto.crop_trait_code_id = ctc.crop_trait_code_id
            WHERE cto.inventory_id in (:inventoryid)

	UNION SELECT crop_trait_code_lang_id FROM crop_trait_code_lang ctcl
            INNER JOIN crop_trait_code ctc ON ctc.crop_trait_code_id = ctcl.crop_trait_code_id
            INNER JOIN crop_trait_observation cto ON cto.crop_trait_code_id = ctc.crop_trait_code_id
            INNER JOIN inventory i ON i.inventory_id = cto.inventory_id
            WHERE i.accession_id in (:accessionid)

	UNION SELECT crop_trait_code_lang_id FROM crop_trait_code_lang ctcl
            INNER JOIN crop_trait_code ctc ON ctc.crop_trait_code_id = ctcl.crop_trait_code_id
            INNER JOIN crop_trait_observation cto ON cto.crop_trait_code_id = ctc.crop_trait_code_id
            INNER JOIN order_request_item ori ON ori.inventory_id = cto.inventory_id
            WHERE ori.order_request_id in (:orderrequestid)

    ) list ON list.crop_trait_code_lang_id = ctcl.crop_trait_code_lang_id