SELECT
     gd.genetic_observation_data_id
    ,i.accession_id
    ,gd.inventory_id
    ,(SELECT crop_id FROM genetic_marker WHERE genetic_marker_id = 
       (SELECT genetic_marker_id FROM genetic_annotation ga WHERE ga.genetic_annotation_id = gd.genetic_annotation_id)) AS crop_id
    ,gd.genetic_annotation_id
    ,gd.genetic_observation_id
    ,gd.individual
    ,gd.individual_allele_number
    ,gd.value
    ,(SELECT taxonomy_species_id FROM accession WHERE accession_id = i.accession_id) AS taxonomy_species_id
    ,gd.created_date
    ,gd.created_by
    ,gd.modified_date
    ,gd.modified_by
    ,gd.owned_date
    ,gd.owned_by
FROM 
    genetic_observation_data gd
    INNER JOIN inventory i ON gd.inventory_id = i.inventory_id
    INNER JOIN (
	SELECT genetic_observation_data_id FROM genetic_observation_data
            WHERE genetic_observation_data_id IN (:geneticobservationdataid)

	UNION SELECT genetic_observation_data_id FROM genetic_observation_data
            WHERE genetic_observation_id IN (:geneticobservationid)

	UNION SELECT genetic_observation_data_id FROM genetic_observation_data
            WHERE genetic_annotation_id IN (:geneticannotationid)

	UNION SELECT genetic_observation_data_id FROM genetic_observation_data
            WHERE inventory_id IN (:inventoryid)

	UNION SELECT genetic_observation_data_id FROM genetic_observation_data
	    INNER JOIN inventory i ON genetic_observation_data.inventory_id = i.inventory_id
            WHERE accession_id IN (:accessionid)

	UNION SELECT genetic_observation_data_id FROM genetic_observation_data
	    INNER JOIN inventory i ON genetic_observation_data.inventory_id = i.inventory_id
	    INNER JOIN order_request_item ori ON i.inventory_id = ori.inventory_id
            WHERE ori.order_request_id in (:orderrequestid)

	UNION SELECT genetic_observation_data_id FROM genetic_observation_data
	    INNER JOIN genetic_annotation ga ON genetic_observation_data.genetic_annotation_id = ga.genetic_annotation_id
            WHERE genetic_marker_id IN (:geneticmarkerid)

	UNION SELECT genetic_observation_data_id FROM genetic_observation_data
	    INNER JOIN genetic_annotation ga ON genetic_observation_data.genetic_annotation_id = ga.genetic_annotation_id
            WHERE method_id IN (:methodid)

	UNION SELECT genetic_observation_data_id FROM genetic_observation_data
	    INNER JOIN genetic_annotation ga ON genetic_observation_data.genetic_annotation_id = ga.genetic_annotation_id
	    INNER JOIN genetic_marker gm ON ga.genetic_marker_id = gm.genetic_marker_id
            WHERE crop_id IN (:cropid)

    ) list ON gd.genetic_observation_data_id = list.genetic_observation_data_id
