SELECT wor.web_order_request_id
      ,wor.web_cooperator_id
      ,wc.last_name
      ,wc.title
      ,wc.first_name
      ,wc.organization
      ,wora.address_line1
      ,wora.address_line2
      ,wora.address_line3
      ,wora.city
      ,wora.postal_index
      ,wora.geography_id
      ,wc.primary_phone
      ,wc.email
      ,wor.ordered_date
      ,wor.intended_use_code
      ,wor.intended_use_note
      ,wor.status_code
      ,wor.note
      ,wor.special_instruction
      ,wor.created_date
      ,wor.created_by
      ,wor.modified_date
      ,wor.modified_by
      ,wor.owned_date
      ,wor.owned_by
FROM
    web_order_request wor
    LEFT JOIN web_cooperator wc ON wc.web_cooperator_id = wor.web_cooperator_id
    LEFT JOIN web_order_request_address wora on wora.web_order_request_id = wor.web_order_request_id
    INNER JOIN (
        SELECT web_order_request_id FROM web_order_request
            WHERE web_order_request_id IN (:weborderrequestid)

        UNION SELECT wor.web_order_request_id FROM web_order_request wor
            INNER JOIN web_order_request_item wori ON wori.web_order_request_id = wor.web_order_request_id
            INNER JOIN accession a ON a.accession_id = wori.accession_id
            WHERE a.owned_by IN (:ownedby) AND wor.status_code in (:orderstatus)

        UNION SELECT web_order_request_id FROM web_order_request
            WHERE web_cooperator_id IN (:webcooperatorid)

        UNION SELECT web_order_request_id FROM web_order_request_item
            WHERE web_order_request_item_id IN (:weborderrequestitemid)

        UNION SELECT web_order_request_id FROM order_request
            WHERE order_request_id IN (:orderrequestid)

        UNION SELECT wor.web_order_request_id FROM web_order_request wor
            INNER JOIN cooperator c ON c.web_cooperator_id = wor.web_cooperator_id
            WHERE c.cooperator_id IN (:cooperatorid)

        UNION SELECT wor.web_order_request_id FROM web_order_request wor
            INNER JOIN web_order_request_item wori ON wori.web_order_request_id = wor.web_order_request_id
            WHERE wori.accession_id IN (:accessionid)

        UNION SELECT wor.web_order_request_id FROM web_order_request wor
            INNER JOIN web_order_request_item wori ON wori.web_order_request_id = wor.web_order_request_id
            INNER JOIN inventory i ON i.accession_id = wori.accession_id
            WHERE i.inventory_id IN (:inventoryid)

    ) list ON list.web_order_request_id = wor.web_order_request_id
