SELECT
  wc.web_cooperator_id,
  wc.last_name,
  wc.title,
  wc.first_name,
  wc.job,
  wc.organization,
  wc.organization_code,
  wc.address_line1,
  wc.address_line2,
  wc.address_line3,
  wc.city,
  wc.postal_index,
  wc.geography_id,
  wc.primary_phone,
  wc.secondary_phone,
  wc.fax,
  wc.email,
  wc.is_active,
  wc.category_code,
  wc.organization_region,
  wc.discipline,
  wc.initials,
  dbms_lob.substr(wc.note,32767,1) as note,
  wc.created_date,
  wc.created_by,
  wc.modified_date,
  wc.modified_by,
  wc.owned_date,
  wc.owned_by
FROM
    web_cooperator wc
    INNER JOIN (
        SELECT web_cooperator_id FROM web_cooperator
            WHERE web_cooperator_id IN (:webcooperatorid)

        UNION SELECT web_cooperator_id FROM web_cooperator
            WHERE geography_id IN (:geographyid)

        UNION SELECT wc.web_cooperator_id FROM web_cooperator wc
            INNER JOIN cooperator c ON c.web_cooperator_id = wc.web_cooperator_id
            WHERE c.cooperator_id IN (:cooperatorid)

        UNION SELECT wc.web_cooperator_id FROM web_cooperator wc
            INNER JOIN web_order_request wor ON wor.web_cooperator_id = wc.web_cooperator_id
            WHERE wor.web_order_request_id IN (:weborderrequestid)

        UNION SELECT wc.web_cooperator_id FROM web_cooperator wc
            INNER JOIN web_order_request_item wori ON wori.web_cooperator_id = wc.web_cooperator_id
            WHERE wori.web_order_request_item_id IN (:weborderrequestitemid)

        UNION SELECT wc.web_cooperator_id FROM web_cooperator wc
            INNER JOIN web_order_request_item wori ON wori.web_cooperator_id = wc.web_cooperator_id
            INNER JOIN accession a ON a.accession_id = wori.accession_id
            WHERE a.accession_id IN (:accessionid)

        UNION SELECT wc.web_cooperator_id FROM web_cooperator wc
            INNER JOIN web_order_request_item wori ON wori.web_cooperator_id = wc.web_cooperator_id
            INNER JOIN inventory i ON i.accession_id = wori.accession_id
            WHERE i.inventory_id IN (:inventoryid)

        UNION SELECT wc.web_cooperator_id FROM web_cooperator wc
            INNER JOIN web_order_request wor ON wor.web_cooperator_id = wc.web_cooperator_id
            INNER JOIN order_request oreq ON oreq.web_order_request_id = wor.web_order_request_id 
            WHERE oreq.order_request_id IN (:orderrequestid)

    ) list ON list.web_cooperator_id = wc.web_cooperator_id
