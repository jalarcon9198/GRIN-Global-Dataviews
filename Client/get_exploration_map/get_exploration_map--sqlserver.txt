SELECT
  em.exploration_map_id,
  em.exploration_id,
  em.cooperator_id,
  em.created_date,
  em.created_by,
  em.modified_date,
  em.modified_by,
  em.owned_date,
  em.owned_by
FROM
    exploration_map em
WHERE 
    em.exploration_map_id in (:explorationmapid)

UNION
SELECT
  em.exploration_map_id,
  em.exploration_id,
  em.cooperator_id,
  em.created_date,
  em.created_by,
  em.modified_date,
  em.modified_by,
  em.owned_date,
  em.owned_by
FROM
    exploration_map em
WHERE 
    em.exploration_id in (:explorationid)

UNION
SELECT
  em.exploration_map_id,
  em.exploration_id,
  em.cooperator_id,
  em.created_date,
  em.created_by,
  em.modified_date,
  em.modified_by,
  em.owned_date,
  em.owned_by
FROM
    exploration_map em
WHERE 
    em.cooperator_id in (:cooperatorid)




