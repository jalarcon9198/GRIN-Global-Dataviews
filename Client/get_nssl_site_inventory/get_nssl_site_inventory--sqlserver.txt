SELECT
  nsi.nssl_site_inventory_id,
  nsi.inventory_id,
  nsi.next_test,
  nsi.hundred_weight,
  nsi.moisture_1,
  nsi.moisture_1_type_code,
  nsi.moisture_1_date,
  nsi.moisture_2,
  nsi.moisture_2_type_code,
  nsi.moisture_2_date,
  nsi.moisture_3,
  nsi.moisture_3_type_code,
  nsi.moisture_3_date,
  nsi.moisture_4,
  nsi.moisture_4_type_code,
  nsi.moisture_4_date,
  nsi.moisture_5,
  nsi.moisture_5_type_code,
  nsi.moisture_5_date,
  nsi.percent_cleanout,
  nsi.note,
  nsi.created_date,
  nsi.created_by,
  nsi.modified_date,
  nsi.modified_by,
  nsi.owned_date,
  nsi.owned_by
FROM
    nssl_site_inventory nsi
    INNER JOIN (
        SELECT nssl_site_inventory_id FROM nssl_site_inventory WHERE nssl_site_inventory_id IN (:nsslsiteinventoryid)

        UNION SELECT nssl_site_inventory_id FROM nssl_site_inventory WHERE inventory_id IN (:inventoryid)

        UNION SELECT nssl_site_inventory_id FROM nssl_site_inventory
           INNER JOIN inventory ON nssl_site_inventory.inventory_id = inventory.inventory_id
           WHERE accession_id IN (:accessionid)

    ) list ON list.nssl_site_inventory_id = nsi.nssl_site_inventory_id
