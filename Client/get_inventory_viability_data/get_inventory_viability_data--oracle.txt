SELECT
  ivd.inventory_viability_data_id,
  ivd.inventory_viability_id,
  iv.inventory_id,
  ivd.order_request_item_id,
  ivd.counter_cooperator_id,
  ivd.replication_number,
  ivd.count_number,
  ivd.count_date,
  ivd.normal_count,
  ivd.abnormal_count,
  ivd.dormant_count,
  ivd.dead_count,
  ivd.unknown_count,
  ivd.replication_count,
  ivd.empty_count,
  ivd.infested_count,
  dbms_lob.substr(ivd.note,32767,1) AS note,,
  ivd.created_date,
  ivd.created_by,
  ivd.modified_date,
  ivd.modified_by,
  ivd.owned_date,
  ivd.owned_by
FROM	inventory_viability_data ivd
	INNER JOIN inventory_viability iv
		ON  iv.inventory_viability_id = ivd.inventory_viability_id 
	INNER JOIN (
		SELECT inventory_viability_data_id
			FROM inventory_viability_data WHERE inventory_viability_data_id IN (:inventoryviabilitydataid)

		UNION SELECT inventory_viability_data_id
			FROM inventory_viability_data WHERE inventory_viability_id IN (:inventoryviabilityid)

		UNION SELECT inventory_viability_data_id
			FROM inventory_viability_data WHERE order_request_item_id IN (:orderrequestitemid)

		UNION SELECT inventory_viability_data_id
			FROM inventory_viability_data WHERE counter_cooperator_id IN (:cooperatorid)

		UNION SELECT inventory_viability_data_id
			FROM inventory_viability_data ivd
			INNER JOIN inventory_viability iv ON iv.inventory_viability_id = ivd.inventory_viability_id
			WHERE iv.inventory_id IN (:inventoryid)

		UNION SELECT inventory_viability_data_id
			FROM inventory_viability_data ivd
			INNER JOIN inventory_viability iv ON iv.inventory_viability_id = ivd.inventory_viability_id
			WHERE iv.inventory_viability_rule_id IN (:inventoryviabilityruleid)

		UNION SELECT inventory_viability_data_id
			FROM inventory_viability_data ivd
			INNER JOIN inventory_viability iv ON iv.inventory_viability_id = ivd.inventory_viability_id
			INNER JOIN inventory i ON i.inventory_id = iv.inventory_id
			WHERE i.accession_id IN (:accessionid)

		UNION SELECT inventory_viability_data_id
			FROM inventory_viability_data ivd
			INNER JOIN inventory_viability iv ON iv.inventory_viability_id = ivd.inventory_viability_id
			INNER JOIN inventory i ON i.inventory_id = iv.inventory_id
			INNER JOIN accession a ON i.accession_id = a.accession_id
			INNER JOIN taxonomy_species ts ON ts.taxonomy_species_id = a.taxonomy_species_id
			WHERE  ts.taxonomy_genus_id IN (:taxonomygenusid)

		UNION SELECT inventory_viability_data_id
			FROM inventory_viability_data ivd
			INNER JOIN inventory_viability iv ON iv.inventory_viability_id = ivd.inventory_viability_id
			INNER JOIN order_request_item ori ON ori.inventory_id = iv.inventory_id
			WHERE ori.order_request_id IN (:orderrequestid)
	) list ON ivd.inventory_viability_data_id = list.inventory_viability_data_id