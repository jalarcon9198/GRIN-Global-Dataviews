SELECT
      ag.accession_inv_group_id
     ,ag.group_name
     ,ag.is_web_visible
     ,ag.note
     ,ag.created_date
     ,ag.created_by
     ,ag.modified_date
     ,ag.modified_by
     ,ag.owned_date
     ,ag.owned_by
FROM 
    accession_inv_group ag
    INNER JOIN (
	SELECT ag.accession_inv_group_id FROM accession_inv_group ag
            WHERE ag.accession_inv_group_id in (:accessioninvgroupid)

	UNION SELECT ag.accession_inv_group_id FROM accession_inv_group ag
	    INNER JOIN accession_inv_group_map agm ON ag.accession_inv_group_id = agm.accession_inv_group_id
            WHERE agm.accession_inv_group_map_id in (:accessioninvgroupmapid)

	UNION SELECT ag.accession_inv_group_id FROM accession_inv_group ag
	    INNER JOIN accession_inv_group_map agm ON ag.accession_inv_group_id = agm.accession_inv_group_id
            WHERE agm.inventory_id in (:inventoryid)

	UNION SELECT ag.accession_inv_group_id FROM accession_inv_group ag
	    INNER JOIN accession_inv_group_map agm ON ag.accession_inv_group_id = agm.accession_inv_group_id
	    INNER JOIN inventory i ON agm.inventory_id = i.inventory_id
            WHERE i.accession_id in (:accessionid)

	UNION SELECT ag.accession_inv_group_id FROM accession_inv_group ag
	    INNER JOIN accession_inv_group_map agm ON ag.accession_inv_group_id = agm.accession_inv_group_id
	    INNER JOIN order_request_item ori ON agm.inventory_id = ori.inventory_id
            WHERE ori.order_request_id in (:orderrequestid)

    ) list ON ag.accession_inv_group_id = list.accession_inv_group_id