SELECT
  distinct g.geography_id,
  g.current_geography_id,
  g.country_code,
  g.adm1,
  g.adm1_type_code,
  g.adm2,
  g.adm2_type_code,
  g.adm3,
  g.adm3_type_code,
  g.adm4,
  g.adm4_type_code,
  g.changed_date,
  g.note,
  g.created_date,
  g.created_by,
  g.modified_date,
  g.modified_by,
  g.owned_date,
  g.owned_by
FROM
    geography AS g
WHERE 
     g.geography_id in (:geographyid)
 UNION 
SELECT
  distinct g.geography_id,
  g.current_geography_id,
  g.country_code,
  g.adm1,
  g.adm1_type_code,
  g.adm2,
  g.adm2_type_code,
  g.adm3,
  g.adm3_type_code,
  g.adm4,
  g.adm4_type_code,
  g.changed_date,
  g.note,
  g.created_date,
  g.created_by,
  g.modified_date,
  g.modified_by,
  g.owned_date,
  g.owned_by
FROM
    geography AS g
    left join geography_region_map grm on g.geography_id = grm.geography_id
WHERE 
     grm.region_id in (:regionid)
 UNION
SELECT
  distinct g.geography_id,
  g.current_geography_id,
  g.country_code,
  g.adm1,
  g.adm1_type_code,
  g.adm2,
  g.adm2_type_code,
  g.adm3,
  g.adm3_type_code,
  g.adm4,
  g.adm4_type_code,
  g.changed_date,
  g.note,
  g.created_date,
  g.created_by,
  g.modified_date,
  g.modified_by,
  g.owned_date,
  g.owned_by
FROM
    geography AS g,
    accession AS a,
    accession_source AS s
WHERE 
     g.geography_id = s.geography_id
     and s.accession_id = a.accession_id
--     and s.is_origin = 'Y'
     and a.accession_id in (:accessionid)
 UNION 
SELECT
  distinct g.geography_id,
  g.current_geography_id,
  g.country_code,
  g.adm1,
  g.adm1_type_code,
  g.adm2,
  g.adm2_type_code,
  g.adm3,
  g.adm3_type_code,
  g.adm4,
  g.adm4_type_code,
  g.changed_date,
  g.note,
  g.created_date,
  g.created_by,
  g.modified_date,
  g.modified_by,
  g.owned_date,
  g.owned_by
FROM
    geography AS g,
    cooperator AS c
WHERE 
     g.geography_id = c.geography_id
     and c.cooperator_id in (:cooperatorid)
 UNION 
SELECT
  distinct g.geography_id,
  g.current_geography_id,
  g.country_code,
  g.adm1,
  g.adm1_type_code,
  g.adm2,
  g.adm2_type_code,
  g.adm3,
  g.adm3_type_code,
  g.adm4,
  g.adm4_type_code,
  g.changed_date,
  g.note,
  g.created_date,
  g.created_by,
  g.modified_date,
  g.modified_by,
  g.owned_date,
  g.owned_by
FROM
    geography AS g,
    cooperator AS c,
    order_request AS o
WHERE 
     g.geography_id = c.geography_id
     and o.final_recipient_cooperator_id = c.cooperator_id
     and o.order_request_id in (:orderrequestid)
