SELECT
  m.method_id,
  m.name,
  m.geography_id,
  m.elevation_meters,
  m.latitude,
  m.longitude,
  m.uncertainty,
  m.formatted_locality,
  m.georeference_datum,
  m.georeference_protocol_code,
  m.georeference_annotation,
  m.materials_and_methods,
  m.study_reason_code,
  m.created_date,
  m.created_by,
  m.modified_date,
  m.modified_by,
  m.owned_date,
  m.owned_by
FROM
  method AS m
WHERE 
  m.method_id in (:methodid)

UNION
SELECT
  m.method_id,
  m.name,
  m.geography_id,
  m.elevation_meters,
  m.latitude,
  m.longitude,
  m.uncertainty,
  m.formatted_locality,
  m.georeference_datum,
  m.georeference_protocol_code,
  m.georeference_annotation,
  m.materials_and_methods,
  m.study_reason_code,
  m.created_date,
  m.created_by,
  m.modified_date,
  m.modified_by,
  m.owned_date,
  m.owned_by
FROM
  method AS m
WHERE 
  m.geography_id in (:geographyid)

UNION

SELECT
  m.method_id,
  m.name,
  m.geography_id,
  m.elevation_meters,
  m.latitude,
  m.longitude,
  m.uncertainty,
  m.formatted_locality,
  m.georeference_datum,
  m.georeference_protocol_code,
  m.georeference_annotation,
  m.materials_and_methods,
  m.study_reason_code,
  m.created_date,
  m.created_by,
  m.modified_date,
  m.modified_by,
  m.owned_date,
  m.owned_by
FROM
  method AS m
  LEFT JOIN crop_trait_observation cto ON cto.method_id = m.method_id
  LEFT JOIN crop_trait ct ON ct.crop_trait_id = cto.crop_trait_id
  LEFT JOIN crop c ON c.crop_id = ct.crop_id
WHERE
  ct.crop_id in (:cropid)

UNION
  
SELECT
  m.method_id,
  m.name,
  m.geography_id,
  m.elevation_meters,
  m.latitude,
  m.longitude,
  m.uncertainty,
  m.formatted_locality,
  m.georeference_datum,
  m.georeference_protocol_code,
  m.georeference_annotation,
  m.materials_and_methods,
  m.study_reason_code,
  m.created_date,
  m.created_by,
  m.modified_date,
  m.modified_by,
  m.owned_date,
  m.owned_by
FROM
  method AS m
  LEFT JOIN crop_trait_observation cto ON cto.method_id = m.method_id
  LEFT JOIN inventory i ON i.inventory_id = cto.inventory_id
WHERE
  i.inventory_id in (:inventoryid)

UNION
  
SELECT
  m.method_id,
  m.name,
  m.geography_id,
  m.elevation_meters,
  m.latitude,
  m.longitude,
  m.uncertainty,
  m.formatted_locality,
  m.georeference_datum,
  m.georeference_protocol_code,
  m.georeference_annotation,
  m.materials_and_methods,
  m.study_reason_code,
  m.created_date,
  m.created_by,
  m.modified_date,
  m.modified_by,
  m.owned_date,
  m.owned_by
FROM
  method AS m
  LEFT JOIN crop_trait_observation cto ON cto.method_id = m.method_id
  LEFT JOIN inventory i ON i.inventory_id = cto.inventory_id
  LEFT JOIN accession a ON a.accession_id = i.accession_id
WHERE
  a.accession_id in (:accessionid)