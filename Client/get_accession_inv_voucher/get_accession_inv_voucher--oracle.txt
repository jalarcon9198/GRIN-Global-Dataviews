SELECT
  v.accession_inv_voucher_id,
  i.accession_id,
  v.inventory_id,
  v.collector_voucher_number,
  v.voucher_location,
  v.vouchered_date_code,
  v.vouchered_date,
  v.collector_cooperator_id,
  dbms_lob.substr(v.note,32767,1),
  (SELECT taxonomy_species_id FROM accession acc WHERE (i.accession_id = acc.accession_id)) AS taxonomy_species_id,
  v.created_date,
  v.created_by,
  v.modified_date,
  v.modified_by,
  v.owned_date,
  v.owned_by
FROM
    accession_inv_voucher v
    INNER JOIN inventory i ON v.inventory_id = i.inventory_id
    INNER JOIN (
	SELECT accession_inv_voucher_id FROM accession_inv_voucher
            WHERE accession_inv_voucher_id in (:accessioninvvoucherid)

	UNION SELECT accession_inv_voucher_id FROM accession_inv_voucher
            WHERE inventory_id in (:inventoryid)

	UNION SELECT accession_inv_voucher_id FROM accession_inv_voucher
            WHERE collector_cooperator_id in (:cooperatorid)

	UNION SELECT accession_inv_voucher_id FROM accession_inv_voucher
            INNER JOIN inventory ON accession_inv_voucher.inventory_id = inventory.inventory_id
            WHERE inventory.accession_id in (:accessionid)

	UNION SELECT accession_inv_voucher_id FROM accession_inv_voucher
            INNER JOIN order_request_item ON accession_inv_voucher.inventory_id = order_request_item.inventory_id
            WHERE order_request_item.order_request_id in (:orderrequestid)

    ) list ON v.accession_inv_voucher_id = list.accession_inv_voucher_id
