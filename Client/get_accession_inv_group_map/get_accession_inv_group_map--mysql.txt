SELECT
  agm.accession_inv_group_map_id AS accession_inv_group_map_id,
  agm.inventory_id,
  agm.accession_inv_group_id,
  agm.note,
  agm.created_date,
  agm.created_by,
  agm.modified_date,
  agm.modified_by,
  agm.owned_date,
  agm.owned_by
FROM
    accession_inv_group_map agm,
    inventory i,
    accession a
WHERE
    agm.inventory_id = i.inventory_id and
    i.accession_id = a.accession_id and
    agm.accession_inv_group_map_id in (:accessioninvgroupmapid)
 UNION
SELECT
  agm.accession_inv_group_map_id AS accession_inv_group_map_id,
  agm.inventory_id,
  agm.accession_inv_group_id,
  agm.note,
  agm.created_date,
  agm.created_by,
  agm.modified_date,
  agm.modified_by,
  agm.owned_date,
  agm.owned_by
FROM
    accession_inv_group_map agm,
    inventory i,
    accession a
WHERE
    agm.inventory_id = i.inventory_id and
    i.accession_id = a.accession_id and
    a.accession_id in (:accessionid)
 UNION 
SELECT
  agm.accession_inv_group_map_id,
  agm.inventory_id,
  agm.accession_inv_group_id,
  agm.note,
  agm.created_date,
  agm.created_by,
  agm.modified_date,
  agm.modified_by,
  agm.owned_date,
  agm.owned_by
FROM
    accession_inv_group_map agm

WHERE
    agm.inventory_id in (:inventoryid)
 UNION 
SELECT
  agm.accession_inv_group_map_id,
  agm.inventory_id,
  agm.accession_inv_group_id,
  agm.note,
  agm.created_date,
  agm.created_by,
  agm.modified_date,
  agm.modified_by,
  agm.owned_date,
  agm.owned_by
FROM
    accession_inv_group_map agm,
    order_request_item ori
WHERE
    agm.inventory_id = ori.inventory_id and
    ori.order_request_id in (:orderrequestid)
