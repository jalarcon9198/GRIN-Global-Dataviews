SELECT
  ivr.inventory_viability_rule_id,
  ivr.taxonomy_species_id,
  ivr.name,
  ivr.substrata,
  ivr.temperature_range,
  ivr.requirements,
  ivr.category_code,
  ivr.count_regime_days,
  ivr.moisture,
  ivr.prechill,
  ivr.lighting,
  ivr.note,
  ivr.created_date,
  ivr.created_by,
  ivr.modified_date,
  ivr.modified_by,
  ivr.owned_date,
  ivr.owned_by
FROM
    inventory_viability_rule AS ivr

	INNER JOIN (
		SELECT inventory_viability_rule_id
			FROM inventory_viability_rule WHERE inventory_viability_rule_id IN (:inventoryviabilityruleid)

		UNION SELECT inventory_viability_rule_id
			FROM inventory_viability_rule WHERE taxonomy_species_id IN (:taxonomyspeciesid)

		UNION SELECT inventory_viability_rule_id
			FROM inventory_viability_rule ivr
			INNER JOIN taxonomy_species ts ON ts.taxonomy_species_id = ivr.taxonomy_species_id
			WHERE  ts.taxonomy_genus_id IN (:taxonomygenusid)

		UNION SELECT ivr.inventory_viability_rule_id
			FROM inventory_viability_rule ivr
			INNER JOIN inventory_viability iv ON iv.inventory_viability_rule_id = ivr.inventory_viability_rule_id
			WHERE iv.inventory_id IN (:inventoryid)

		UNION SELECT ivr.inventory_viability_rule_id
			FROM inventory_viability_rule ivr
			INNER JOIN inventory_viability iv ON iv.inventory_viability_rule_id = ivr.inventory_viability_rule_id
			INNER JOIN inventory i ON i.inventory_id = iv.inventory_id
			WHERE i.accession_id IN (:accessionid)

		UNION SELECT ivr.inventory_viability_rule_id
			FROM inventory_viability_rule ivr
			INNER JOIN inventory_viability iv ON iv.inventory_viability_rule_id = ivr.inventory_viability_rule_id
			INNER JOIN order_request_item ori ON ori.inventory_id = iv.inventory_id
			WHERE ori.order_request_id IN (:orderrequestid)
	) list ON ivr.inventory_viability_rule_id = list.inventory_viability_rule_id