SELECT
  tf.taxonomy_family_id,
  tf.current_taxonomy_family_id,
  tf.type_taxonomy_genus_id,
  tf.suprafamily_rank_code, 
  tf.suprafamily_rank_name,
  tf.family_name,
  tf.family_authority,
  tf.alternate_name,
  tf.subfamily_name,
  tf.tribe_name,
  tf.subtribe_name,
  tf.note,
  tf.created_date,
  tf.created_by,
  tf.modified_date,
  tf.modified_by,
  tf.owned_date,
  tf.owned_by
FROM
    taxonomy_family tf
    INNER JOIN (
        SELECT taxonomy_family_id FROM taxonomy_family WHERE taxonomy_family_id IN (:taxonomyfamilyid)

        UNION SELECT taxonomy_family_id FROM taxonomy_genus
            WHERE taxonomy_genus_id in (:taxonomygenusid)

        UNION SELECT taxonomy_family_id FROM taxonomy_genus
            INNER JOIN taxonomy_species ON taxonomy_genus.taxonomy_genus_id = taxonomy_species.taxonomy_genus_id
            INNER JOIN accession ON taxonomy_species.taxonomy_species_id = accession.taxonomy_species_id
            WHERE accession_id in (:accessionid)

        UNION SELECT taxonomy_family_id FROM taxonomy_genus
            INNER JOIN taxonomy_species ON taxonomy_genus.taxonomy_genus_id = taxonomy_species.taxonomy_genus_id
            INNER JOIN accession ON taxonomy_species.taxonomy_species_id = accession.taxonomy_species_id
            INNER JOIN inventory ON accession.accession_id = inventory.accession_id
            WHERE inventory_id in (:inventoryid)

    ) list ON list.taxonomy_family_id = tf.taxonomy_family_id
