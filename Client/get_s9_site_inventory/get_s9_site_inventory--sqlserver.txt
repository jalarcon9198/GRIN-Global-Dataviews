SELECT
  ssi.s9_site_inventory_id,
  ssi.inventory_id,
  ssi.increase_location_code,
  ssi.plot_number,
  ssi.hundred_weight,
  ssi.pollination_procedure_code,
  ssi.virus_status_code,
  ssi.note,
  ssi.created_date,
  ssi.created_by,
  ssi.modified_date,
  ssi.modified_by,
  ssi.owned_date,
  ssi.owned_by
FROM
    s9_site_inventory ssi
WHERE
    ssi.s9_site_inventory_id in (:s9siteinventoryid)

UNION
SELECT
  ssi.s9_site_inventory_id,
  ssi.inventory_id,
  ssi.increase_location_code,
  ssi.plot_number,
  ssi.hundred_weight,
  ssi.pollination_procedure_code,
  ssi.virus_status_code,
  ssi.note,
  ssi.created_date,
  ssi.created_by,
  ssi.modified_date,
  ssi.modified_by,
  ssi.owned_date,
  ssi.owned_by
FROM
    s9_site_inventory ssi
WHERE
    ssi.inventory_id in (:inventoryid)

UNION
SELECT
  ssi.s9_site_inventory_id,
  ssi.inventory_id,
  ssi.increase_location_code,
  ssi.plot_number,
  ssi.hundred_weight,
  ssi.pollination_procedure_code,
  ssi.virus_status_code,
  ssi.note,
  ssi.created_date,
  ssi.created_by,
  ssi.modified_date,
  ssi.modified_by,
  ssi.owned_date,
  ssi.owned_by
FROM
    s9_site_inventory ssi, accession a, inventory i
WHERE
    ssi.inventory_id = i.inventory_id and
    i.accession_id = a.accession_id and
    a.accession_id in (:accessionid)





