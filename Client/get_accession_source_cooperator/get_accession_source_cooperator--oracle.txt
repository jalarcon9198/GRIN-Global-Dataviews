SELECT
  asm.accession_source_map_id,
  asm.accession_source_id,
  asm.cooperator_id,
  asrc.accession_id,
  asrc.source_type_code,
  asrc.source_date_code,
  asrc.source_date,
  (SELECT taxonomy_species_id FROM accession WHERE accession_id = asrc.accession_id) AS taxonomy_species_id,
  asm.created_date,
  asm.created_by,
  asm.modified_date,
  asm.modified_by,
  asm.owned_date,
  asm.owned_by
FROM
    accession_source_map asm
    LEFT JOIN accession_source asrc ON asm.accession_source_id = asrc.accession_source_id 
    INNER JOIN (
	SELECT accession_source_map_id FROM accession_source_map
            WHERE accession_source_map_id in (:accessionsourcemapid)

	UNION SELECT accession_source_map_id FROM accession_source_map
            WHERE accession_source_id in (:accessionsourceid)

	UNION SELECT accession_source_map_id FROM accession_source_map
            WHERE cooperator_id in (:cooperatorid)

	UNION SELECT accession_source_map_id FROM accession_source_map
            INNER JOIN accession_source ON accession_source_map.accession_source_id = accession_source.accession_source_id
            WHERE accession_id in (:accessionid)

	UNION SELECT accession_source_map_id FROM accession_source_map
            INNER JOIN accession_source ON accession_source_map.accession_source_id = accession_source.accession_source_id
            INNER JOIN inventory ON accession_source.accession_id = inventory.accession_id
            WHERE inventory.inventory_id in (:inventoryid)

	UNION SELECT accession_source_map_id FROM accession_source_map
            INNER JOIN accession_source ON accession_source_map.accession_source_id = accession_source.accession_source_id
            INNER JOIN inventory ON accession_source.accession_id = inventory.accession_id
            INNER JOIN order_request_item ON inventory.inventory_id = order_request_item.inventory_id
            WHERE order_request_item.order_request_id in (:orderrequestid)

    ) list ON asm.accession_source_map_id = list.accession_source_map_id
