SELECT
  c.citation_id,
  c.taxonomy_genus_id,
  c.type_code,
  c.author_name,
  c.citation_year,
  c.title,
  c.citation_title,
  l.abbreviation,
  c.reference,
  c.literature_id,
  c.doi_reference,
  c.url,
  c.description,
  dbms_lob.substr(c.note,32767,1) as note,
  c.created_date,
  c.created_by,
  c.modified_date,
  c.modified_by,
  c.owned_date,
  c.owned_by
FROM
    citation c
    LEFT JOIN literature l ON c.literature_id = l.literature_id
    INNER JOIN (
        SELECT citation_id FROM citation WHERE citation_id IN (:citationid)
        UNION SELECT citation_id FROM citation WHERE taxonomy_genus_id IN (:taxonomygenusid)
        UNION SELECT citation_id FROM citation WHERE literature_id IN (:literatureid) AND taxonomy_genus_id IS NOT NULL
        UNION SELECT citation_id FROM citation
            INNER JOIN taxonomy_genus ON citation.taxonomy_genus_id = taxonomy_genus.taxonomy_genus_id
            WHERE taxonomy_genus.taxonomy_family_id IN (:taxonomyfamilyid)
     ) list on list.citation_id = c.citation_id AND taxonomy_genus_id IS NOT NULL
