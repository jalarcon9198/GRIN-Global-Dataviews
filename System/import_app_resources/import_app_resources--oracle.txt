SELECT
  ar.app_name,
  ar.form_name,
  ar.app_resource_name,
  ar.description,
  ar.display_member,
  ar.value_member,
  ar.sort_order,
  ar.properties
FROM
    app_resource ar

WHERE 
     ar.sys_lang_id = :langid
    and 1 = :viewallrows
ORDER BY
   ar.app_name, ar.form_name, ar.app_resource_name
