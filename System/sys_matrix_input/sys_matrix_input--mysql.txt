/* sys_matrix_input  version 2.06
   Generates a list of joins. The first section should form a loopless spanning tree
*/
SELECT 
	'map' command,
	'taxonomy_genus'	child_table,
	'taxonomy_family'	parent_table,
	'' 			dest_table,	/* defaults to parent */
	'taxonomy_family_id'	child_field,	/* defaults to parent_table_id */
	''			parent_field,	/* defaults to parent_table_id */
	''			extra_join_code	/* rarely used */

UNION ALL SELECT 'map', 'taxonomy_species', 'taxonomy_genus', '', 'taxonomy_genus_id', '', ''
UNION ALL SELECT 'map', 'accession', 'taxonomy_species', '', 'taxonomy_species_id', '', ''
UNION ALL SELECT 'map', 'inventory', 'accession', '', 'accession_id', '', ''
UNION ALL SELECT 'map', 'crop_trait_observation', 'inventory', '', 'inventory_id', '', ''
UNION ALL SELECT 'map', 'crop_trait_observation', 'crop_trait', '', 'crop_trait_id', '', ''
UNION ALL SELECT 'map', 'crop_trait', 'crop', '', 'crop_id', '', ''

UNION ALL SELECT 'map', 'accession_source', 'accession', '', 'accession_id', '', ''
UNION ALL SELECT 'map', 'accession_source', 'geography', '', 'geography_id', '', ''
UNION ALL SELECT 'map', 'geography_region_map', 'geography', '', 'geography_id', '', ''
UNION ALL SELECT 'map', 'geography_region_map', 'region', '', 'region_id', '', ''
UNION ALL SELECT 'map', 'geography', 'code_value', '', 'country_code', 'value', 'AND code_value.group_name = ''GEOGRAPHY_COUNTRY_CODE'''
UNION ALL SELECT 'map', 'code_value_lang', 'code_value', '', 'code_value_id', '', ''

UNION ALL SELECT 'map', 'accession_source_map', 'accession_source', '', 'accession_source_id', '', ''
UNION ALL SELECT 'map', 'accession_source_map', 'cooperator', '', 'cooperator_id', '', ''
UNION ALL SELECT 'map', 'cooperator_map', 'cooperator_group', '', 'cooperator_group_id', '', ''
UNION ALL SELECT 'map', 'cooperator_map', 'cooperator', '', 'cooperator_id', '', ''

UNION ALL SELECT 'map', 'accession_action',	'accession', '', 'accession_id', '', ''
UNION ALL SELECT 'map', 'accession_ipr',	'accession', '', 'accession_id', '', ''
UNION ALL SELECT 'map', 'accession_pedigree',	'accession', '', 'accession_id', '', ''
UNION ALL SELECT 'map', 'accession_quarantine',	'accession', '', 'accession_id', '', ''
UNION ALL SELECT 'map', 'accession',		'cooperator a_owned_by_c', '', 'owned_by', 'cooperator_id', ''
UNION ALL SELECT 'map', 'cooperator a_owned_by_c', 'site', '', 'site_id', '', ''

UNION ALL SELECT 'map', 'citation', 'accession', '', 'accession_id', '', ''
UNION ALL SELECT 'map', 'citation', 'literature', '', 'literature_id', '', ''

UNION ALL SELECT 'map', 'inventory', 'inventory accession_inv', '', 'accession_id', 'accession_id', 'AND inventory.inventory_id = 
    CASE accession_inv.form_type_code
        WHEN ''**'' THEN inventory.inventory_id
        ELSE accession_inv.inventory_id
    END'
UNION ALL SELECT 'map', 'accession_inv_annotation',	'inventory accession_inv', '', 'inventory_id', 'inventory_id', ''
UNION ALL SELECT 'map', 'accession_inv_attach',		'inventory accession_inv', '', 'inventory_id', 'inventory_id', ''
UNION ALL SELECT 'map', 'accession_inv_group_map',	'accession_inv_group', '', 'accession_inv_group_id', '', ''
UNION ALL SELECT 'map', 'accession_inv_group_map',	'inventory accession_inv', '', 'inventory_id', 'inventory_id', ''
UNION ALL SELECT 'map', 'accession_inv_name',		'inventory accession_inv', '', 'inventory_id', 'inventory_id', ''
UNION ALL SELECT 'map', 'accession_inv_name',		'name_group', '', 'name_group_id', '', ''
UNION ALL SELECT 'map', 'accession_inv_voucher',	'inventory accession_inv', '', 'inventory_id', 'inventory_id', ''

UNION ALL SELECT 'map', 'crop_attach', 'crop', '', 'crop_id', '', ''
UNION ALL SELECT 'map', 'crop_trait_attach', 'crop_trait', '', 'crop_trait_id', '', ''
UNION ALL SELECT 'map', 'crop_trait_observation', 'crop_trait_code', '', 'crop_trait_code_id', '', ''
UNION ALL SELECT 'map', 'crop_trait_code_attach', 'crop_trait_code', '', 'crop_trait_code_id', '', ''
UNION ALL SELECT 'map', 'crop_trait_code_lang', 'crop_trait_code', '', 'crop_trait_code_id', '', ''
UNION ALL SELECT 'map', 'crop_trait_lang', 'crop_trait', '', 'crop_trait_id', '', ''
UNION ALL SELECT 'map', 'crop_trait_observation', 'method', '', 'method_id', '', ''
UNION ALL SELECT 'map', 'method_map', 'method', '', 'method_id', '', ''
UNION ALL SELECT 'map', 'crop_trait_observation_data', 'inventory', '', 'inventory_id', '', ''

UNION ALL SELECT 'map', 'exploration_map', 'cooperator', '', 'cooperator_id', '', ''
UNION ALL SELECT 'map', 'exploration_map', 'exploration', '', 'exploration_id', '', ''


UNION ALL SELECT 'map', 'genetic_observation_data', 'genetic_observation', '', 'genetic_observation_id', '', ''
UNION ALL SELECT 'map', 'genetic_observation_data', 'inventory', '', 'inventory_id', '', ''
UNION ALL SELECT 'map', 'genetic_observation_data', 'genetic_annotation', '', 'genetic_annotation_id', '', ''
UNION ALL SELECT 'map', 'genetic_annotation', 'genetic_marker', '', 'genetic_marker_id', '', ''

UNION ALL SELECT 'map', 'inventory', 'inventory_maint_policy', '', 'inventory_maint_policy_id', '', ''
UNION ALL SELECT 'map', 'inventory_action', 'inventory', '', 'inventory_id', '', ''
UNION ALL SELECT 'map', 'inventory_quality_status', 'inventory', '', 'inventory_id', '', ''
UNION ALL SELECT 'map', 'inventory_viability', 'inventory', '', 'inventory_id', '', ''
UNION ALL SELECT 'map', 'inventory_viability', 'inventory_viability_rule', '', 'inventory_viability_rule_id', '', ''
UNION ALL SELECT 'map', 'inventory_viability_data', 'inventory_viability', '', 'inventory_viability_id', '', ''

UNION ALL SELECT 'map', 'geneva_site_inventory', 'inventory', '', 'inventory_id', '', ''
UNION ALL SELECT 'map', 'gspi_site_inventory', 'inventory', '', 'inventory_id', '', ''
UNION ALL SELECT 'map', 'nc7_site_inventory', 'inventory', '', 'inventory_id', '', ''
UNION ALL SELECT 'map', 'ne9_site_inventory', 'inventory', '', 'inventory_id', '', ''
UNION ALL SELECT 'map', 'nssl_site_inventory', 'inventory', '', 'inventory_id', '', ''
UNION ALL SELECT 'map', 'opgc_site_inventory', 'inventory', '', 'inventory_id', '', ''
UNION ALL SELECT 'map', 'parl_site_inventory', 'inventory', '', 'inventory_id', '', ''
UNION ALL SELECT 'map', 's9_site_inventory', 'inventory', '', 'inventory_id', '', ''
UNION ALL SELECT 'map', 'w6_site_inventory', 'inventory', '', 'inventory_id', '', ''

UNION ALL SELECT 'map', 'order_request_item', 'inventory', '', 'inventory_id', '', ''
UNION ALL SELECT 'map', 'order_request_item', 'order_request', '', 'order_request_id', '', ''
UNION ALL SELECT 'map', 'order_request_action', 'order_request', '', 'order_request_id', '', ''
UNION ALL SELECT 'map', 'order_request_attach', 'order_request', '', 'order_request_id', '', ''
UNION ALL SELECT 'map', 'order_request_phyto_log', 'order_request', '', 'order_request_id', '', ''

UNION ALL SELECT 'map', 'source_desc_observation', 'accession_source', '', 'accession_source_id', '', ''
UNION ALL SELECT 'map', 'source_desc_observation', 'source_descriptor_code', '', 'source_descriptor_code_id', '', ''
UNION ALL SELECT 'map', 'source_desc_observation', 'source_descriptor', '', 'source_descriptor_id', '', ''
UNION ALL SELECT 'map', 'source_descriptor_code_lang', 'source_descriptor_code', '', 'source_descriptor_code_id', '', ''
UNION ALL SELECT 'map', 'source_descriptor_lang', 'source_descriptor', '', 'source_descriptor_id', '', ''

UNION ALL SELECT 'map', 'taxonomy_alt_family_map', 'taxonomy_genus', '', 'taxonomy_genus_id', '', ''
UNION ALL SELECT 'map', 'taxonomy_attach', 'taxonomy_family', '', 'taxonomy_family_id', '', ''
UNION ALL SELECT 'map', 'taxonomy_common_name', 'taxonomy_species', '', 'taxonomy_species_id', '', ''
UNION ALL SELECT 'map', 'taxonomy_crop_map', 'taxonomy_species', '', 'taxonomy_species_id', '', ''
UNION ALL SELECT 'map', 'taxonomy_geography_map', 'taxonomy_species', '', 'taxonomy_species_id', '', ''
UNION ALL SELECT 'map', 'taxonomy_noxious', 'taxonomy_species', '', 'taxonomy_species_id', '', ''
UNION ALL SELECT 'map', 'taxonomy_use', 'taxonomy_species', '', 'taxonomy_species_id', '', ''

UNION ALL SELECT 'map', 'web_order_request_item', 'accession', '', '', '', ''
UNION ALL SELECT 'map', 'web_order_request_item', 'web_order_request', '', '', '', ''
UNION ALL SELECT 'map', 'web_order_request', 'web_cooperator', '', '', '', ''
UNION ALL SELECT 'map', 'web_user_cart_item', 'accession', '', '', '', ''
UNION ALL SELECT 'map', 'web_user_cart_item', 'web_user_cart', '', '', '', ''

UNION ALL SELECT 'map', 'sys_table_lang', 'sys_table', '', 'sys_table_id', '', ''
UNION ALL SELECT 'map', 'sys_table_field', 'sys_table', '', 'sys_table_id', '', ''
UNION ALL SELECT 'map', 'sys_table_field_lang', 'sys_table_field', '', 'sys_table_field_id', '', ''
UNION ALL SELECT 'map', 'sys_dataview_lang', 'sys_dataview', '', 'sys_dataview_id', '', ''
UNION ALL SELECT 'map', 'sys_dataview_field', 'sys_dataview', '', 'sys_dataview_id', '', ''
UNION ALL SELECT 'map', 'sys_dataview_field_lang', 'sys_dataview_field', '', 'sys_dataview_field_id', '', ''
UNION ALL SELECT 'map', 'sys_dataview_field', 'sys_table_field', '', 'sys_table_field_id', '', ''


/* Aliased tables for special cases */
UNION ALL SELECT 'map', 'inventory', 'cooperator i_owned_by_c', '', 'owned_by', 'cooperator_id', ''
UNION ALL SELECT 'map', 'order_request', 'cooperator ord_owned_by_c', '', 'owned_by', 'cooperator_id', ''
UNION ALL SELECT 'map', 'order_request', 'cooperator ord_final_recipient_coop', '', 'final_recipient_cooperator_id', 'cooperator_id', ''
UNION ALL SELECT 'map', 'inventory_viability recent_iv', 'inventory', '', '', '', 'AND recent_iv.inventory_viability_id = (SELECT TOP 1 inventory_viability_id FROM inventory_viability WHERE inventory_viability.inventory_id = inventory.inventory_id ORDER BY tested_date DESC, inventory_viability_id DESC)'

UNION ALL SELECT 'replace', 'inventory', 'cooperator i_owned_by_c', 'site', 'owned_by', 'cooperator_id', ''
UNION ALL SELECT 'replace', 'cooperator i_owned_by_c', 'site', 'site', 'site_id', 'site_id', ''
UNION ALL SELECT 'replace', 'order_request', 'cooperator ord_owned_by_c', 'site', 'owned_by', 'cooperator_id', ''
UNION ALL SELECT 'replace', 'order_request_item', 'order_request', 'site', '', '', ''
UNION ALL SELECT 'replace', 'cooperator ord_owned_by_c', 'site', 'site', 'site_id', 'site_id', ''

/* context sensitive method links */
UNION ALL SELECT 'replace', 'accession_action', 'method', '', '', '', ''
UNION ALL SELECT 'replace', 'inventory_action', 'method', '', '', '', ''
UNION ALL SELECT 'replace', 'inventory_quality_status', 'method', '', '', '', ''
UNION ALL SELECT 'replace', 'genetic_annotation', 'method', '', '', '', ''
UNION ALL SELECT 'replace', 'citation', 'method', '', '', '', ''
UNION ALL SELECT 'replace', 'crop_trait_observation_data', 'method', '', '', '', ''

/* context sensitive cooperator links */
UNION ALL SELECT 'replace', 'accession_action', 'cooperator', '', '', '', ''
UNION ALL SELECT 'replace', 'accession_ipr', 'cooperator', '', '', '', ''
UNION ALL SELECT 'replace', 'inventory_action', 'cooperator', '', '', '', ''
UNION ALL SELECT 'replace', 'method_map', 'cooperator', '', '', '', ''
UNION ALL SELECT 'replace', 'order_request_action', 'cooperator', '', '', '', ''
UNION ALL SELECT 'replace', 'exploration_map', 'cooperator', '', '', '', ''

/* context sensitive geography links */
UNION ALL SELECT 'replace', 'cooperator ord_final_recipient_coop', 'geography', '', '', '', ''
UNION ALL SELECT 'replace', 'order_request', 'cooperator ord_final_recipient_coop', 'geography', 'final_recipient_cooperator_id', 'cooperator_id', ''
UNION ALL SELECT 'replace', 'order_request_item', 'order_request', 'geography', '', '', ''

/* context sensitive citation and literature links */
UNION ALL SELECT 'replace', 'citation', 'method', '', '', '', ''
UNION ALL SELECT 'replace', 'method', 'citation', 'literature', 'method_id', 'method_id', ''
UNION ALL SELECT 'replace', 'citation', 'taxonomy_species', '', '', '', ''
UNION ALL SELECT 'replace', 'taxonomy_species', 'citation', 'literature', 'taxonomy_species_id', 'taxonomy_species_id', ''
UNION ALL SELECT 'replace', 'citation', 'taxonomy_genus', '', '', '', ''
UNION ALL SELECT 'replace', 'taxonomy_genus', 'citation', 'literature', 'taxonomy_genus_id', 'taxonomy_genus_id', ''
UNION ALL SELECT 'replace', 'citation', 'taxonomy_family', '', '', '', ''
UNION ALL SELECT 'replace', 'taxonomy_family', 'citation', 'literature', 'taxonomy_family_id', 'taxonomy_family_id', ''
UNION ALL SELECT 'replace', 'citation', 'accession_ipr', '', '', '', ''
UNION ALL SELECT 'replace', 'accession_ipr', 'citation', 'literature', 'accession_ipr_id', 'accession_ipr_id', ''
UNION ALL SELECT 'replace', 'citation', 'accession_pedigree', '', '', '', ''
UNION ALL SELECT 'replace', 'accession_pedigree', 'citation', 'literature', 'accession_pedigree_id', 'accession_pedigree_id', ''
UNION ALL SELECT 'replace', 'citation', 'genetic_marker', '', '', '', ''
UNION ALL SELECT 'replace', 'genetic_marker', 'citation', 'literature', 'genetic_marker_id', 'genetic_marker_id', ''
UNION ALL SELECT 'replace', 'citation', 'taxonomy_common_name', '', '', '', ''
UNION ALL SELECT 'replace', 'taxonomy_common_name', 'citation', 'literature', 'taxonomy_common_name_id', 'taxonomy_common_name_id', ''
UNION ALL SELECT 'replace', 'citation', 'taxonomy_use', '', '', '', ''
UNION ALL SELECT 'replace', 'taxonomy_use', 'citation', 'literature', 'taxonomy_use_id', 'taxonomy_use_id', ''
UNION ALL SELECT 'replace', 'citation', 'taxonomy_geography_map', '', '', '', ''
UNION ALL SELECT 'replace', 'taxonomy_geography_map', 'citation', 'literature', 'taxonomy_geography_map_id', 'taxonomy_geography_map_id', ''
UNION ALL SELECT 'replace', 'citation', 'taxonomy_crop_map', '', '', '', ''
UNION ALL SELECT 'replace', 'taxonomy_crop_map', 'citation', 'literature', 'taxonomy_crop_map_id', 'taxonomy_crop_map_id', ''

/* Get more joins from the relationship table in case we missed something */

UNION ALL SELECT 
'map' command,
st1.table_name child_table, 
st2.table_name parent_table,
'' dest_table, 
stf1.field_name child_field, 
stf2.field_name parent_field,
'' extra_join_code

FROM sys_table_relationship str
INNER JOIN sys_table_field stf1 ON str.sys_table_field_id = stf1.sys_table_field_id
INNER JOIN sys_table st1 ON stf1.sys_table_id = st1.sys_table_id
INNER JOIN sys_table_field stf2 ON str.other_table_field_id = stf2.sys_table_field_id
INNER JOIN sys_table st2 ON stf2.sys_table_id = st2.sys_table_id
WHERE relationship_type_tag IN ('OWNER_PARENT', 'PARENT')
  AND stf1.field_name = stf2.field_name and st2.table_name NOT IN ('cooperator', 'method', 'geography', 'site', 'sys_lang')
  AND EXISTS (SELECT * FROM sys_dataview WHERE dataview_name = 'get_' + st1.table_name AND category_code = 'Client')
  AND EXISTS (SELECT * FROM sys_dataview WHERE dataview_name = 'get_' + st2.table_name AND category_code = 'Client')
