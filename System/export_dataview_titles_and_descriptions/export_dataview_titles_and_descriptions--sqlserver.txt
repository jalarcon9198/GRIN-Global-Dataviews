SELECT
  sd.dataview_name,
  sdl.title AS dataview_title,
  sdl.description AS dataview_description,
  (select sdl2.title from sys_dataview_lang sdl2 where sdl2.sys_dataview_id = sd.sys_dataview_id and sdl2.sys_lang_id = 1) AS dv_english_title,
  (select sdl2.description from sys_dataview_lang sdl2 where sdl2.sys_dataview_id = sd.sys_dataview_id and sdl2.sys_lang_id = 1) AS dv_english_description

from sys_dataview sd
  left join sys_dataview_lang sdl 
    on sd.sys_dataview_id = sdl.sys_dataview_id 
    and sdl.sys_lang_id = :langid and 1 = :viewallrows
