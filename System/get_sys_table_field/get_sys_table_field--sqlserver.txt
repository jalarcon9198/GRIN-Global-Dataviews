SELECT 
       sys_table_field_id
      ,(select table_name from sys_table st where st.sys_table_id = stf.sys_table_id) as table_name
      ,field_name
      ,field_purpose
      ,field_type
      ,default_value
      ,is_primary_key
      ,is_foreign_key
      ,foreign_key_table_field_id
      ,foreign_key_dataview_name
      ,is_nullable
      ,gui_hint
      ,is_readonly
      ,min_length
      ,max_length
      ,numeric_precision
      ,numeric_scale
      ,is_autoincrement
      ,group_name
      ,created_date
      ,created_by
      ,modified_date
      ,modified_by
      ,owned_date
      ,owned_by
  FROM sys_table_field stf