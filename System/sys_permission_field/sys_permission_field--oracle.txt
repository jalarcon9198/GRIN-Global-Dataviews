SELECT sys_permission_field_id
      ,sys_permission_id
      ,sys_dataview_field_id
      ,sys_table_field_id
      ,field_type
      ,compare_operator
      ,dbms_lob.substr(compare_value,32767,1) as compare_value
      ,parent_table_field_id
      ,parent_field_type
      ,parent_compare_operator
      ,dbms_lob.substr(parent_compare_value,32767,1) as parent_compare_value
      ,compare_mode
      ,created_date
      ,created_by
      ,modified_date
      ,modified_by
      ,owned_date
      ,owned_by
  FROM sys_permission_field
