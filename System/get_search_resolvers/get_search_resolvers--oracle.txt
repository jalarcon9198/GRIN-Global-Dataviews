SELECT sd.dataview_name, ssr.resolved_pkey_name, st.table_name, sds.sql_statement, sd.database_area_code FROM sys_search_resolver ssr
LEFT JOIN sys_dataview sd ON ssr.sys_dataview_id = sd.sys_dataview_id
LEFT JOIN sys_dataview_sql sds ON sd.sys_dataview_id = sds.sys_dataview_id
LEFT JOIN sys_table st ON ssr.sys_table_id = st.sys_table_id
WHERE ssr.resolved_pkey_name = :resolvertype
AND sds.database_engine_tag = :databasetype
