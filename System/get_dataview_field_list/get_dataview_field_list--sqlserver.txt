SELECT 
      sdf.sys_dataview_field_id AS sys_dataview_field_id
      ,sdf.field_name AS field_name
      ,coalesce(sdfl.title, stfl.title) AS title
      ,coalesce(sdfl.description, stfl.description) AS description
FROM sys_dataview_field sdf
      LEFT JOIN sys_dataview sd ON sdf.sys_dataview_id = sd.sys_dataview_id
      LEFT JOIN sys_dataview_field_lang sdfl ON sdfl.sys_dataview_field_id = sdf.sys_dataview_field_id AND sdfl.sys_lang_id = __LANGUAGEID__
	LEFT JOIN sys_table_field_lang stfl on stfl.sys_table_field_id = sdf.sys_table_field_id and stfl.sys_lang_id = __LANGUAGEID__
WHERE
      sd.dataview_name = :dataviewname