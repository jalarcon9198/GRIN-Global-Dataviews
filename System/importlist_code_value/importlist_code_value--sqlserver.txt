SELECT
  cv.value as value_member,
  cv.value as display_member2,
  coalesce(cvl.title, cv.value) as display_member
FROM
  code_value AS cv 
  LEFT JOIN code_value_lang AS cvl
    ON cvl.code_value_id = cv.code_value_id
	and cvl.sys_lang_id = :langid
where
	cv.group_name = :groupname
order by
	coalesce(cvl.title, cv.value)

