SELECT
  sd.dataview_name,
  sdl.title AS dataview_title,
  sdl.description AS dataview_description,
  sdf.field_name,
  sdfl.title as field_title,
  sdfl.description as field_description
FROM
  sys_dataview AS sd 
  LEFT JOIN sys_dataview_field AS sdf
    ON sdf.sys_dataview_id = sd.sys_dataview_id
  LEFT JOIN sys_dataview_field_lang AS sdfl
    ON sdfl.sys_dataview_field_id = sdf.sys_dataview_field_id
	and sdfl.sys_lang_id = :langid

  LEFT JOIN sys_dataview_lang AS sdl
    ON sdl.sys_dataview_id = sd.sys_dataview_id
	and sdfl.sys_lang_id = :langid
WHERE 1 = :viewallrows
