select 
  ':tablename' as table_name,
  ':pkfieldname' as pk_field_name,
  min(:pkfieldname) as min_pk,
  max(:pkfieldname) as max_pk,
  count(:pkfieldname) as row_count
from
  :tablename
WHERE 
  :tablename.:pkfieldname is not null
 
/*
the WHERE clause simply makes sure the given
tablename / pkfieldname are just names and not complex SQL
to help minimize the chance of SQL injection.
If they are complex SQL, the final invalid SQL will cause an error to be generated.
*/