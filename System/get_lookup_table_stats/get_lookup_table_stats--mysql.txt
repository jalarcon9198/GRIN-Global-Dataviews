select 
  ':tablename' as table_name,
  ':pkfieldname' as pk_field_name,
  min(:pkfieldname) as min_pk,
  max(:pkfieldname) as max_pk,
  count(:pkfieldname) as row_count
from
  :tablename
WHERE 
  :tablename.:pkfieldname is not null
 

