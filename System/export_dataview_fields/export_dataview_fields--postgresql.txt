SELECT
  sd.dataview_name,
  sdf.field_name AS dataview_field_name,
  sdfl.title AS dataview_field_title,
  sdfl.description AS dataview_field_description,
  (select sdfl2.title from sys_dataview_field_lang sdfl2 where sdfl2.sys_dataview_field_id = sdf.sys_dataview_field_id and sdfl2.sys_lang_id = 1) AS dv_english_title,
  (select sdfl2.description from sys_dataview_field_lang sdfl2 where sdfl2.sys_dataview_field_id = sdf.sys_dataview_field_id and sdfl2.sys_lang_id = 1) AS dv_english_description
FROM
    sys_dataview_field sdf,
    sys_dataview sd,
    sys_dataview_field_lang sdfl
WHERE sd.sys_dataview_id = sdf.sys_dataview_id and
   sdf.sys_dataview_field_id = sdfl.sys_dataview_field_id
   and sdfl.sys_lang_id = :langid and 1 = :viewallrows
