SELECT stf.sys_table_field_id
      ,stf.sys_table_id AS child_table_id
      ,(SELECT table_name FROM sys_table st WHERE st.sys_table_id = stf.sys_table_id) AS child_table_name
      ,stf.sys_table_field_id AS child_table_field_id
      ,stf.field_name AS child_field_name
      ,stf.field_purpose AS child_field_purpose
      ,strl.relationship_type_tag
      ,(SELECT stf.sys_table_id FROM sys_table_field stf WHERE stf.sys_table_field_id = strl.other_table_field_id) AS parent_table_id
      ,(SELECT table_name FROM sys_table st, sys_table_field stf WHERE st.sys_table_id = stf.sys_table_id AND stf.sys_table_field_id = strl.other_table_field_id) AS parent_table_name
      ,strl.other_table_field_id AS parent_table_field_id
      ,(SELECT field_name FROM sys_table_field stf WHERE strl.sys_table_field_id = stf.sys_table_field_id) AS parent_field_name
      ,stf.created_by
      ,stf.created_date
      ,stf.modified_by
      ,stf.modified_date
      ,stf.owned_by
      ,stf.owned_date
FROM sys_table_field stf
LEFT JOIN sys_table_relationship strl ON stf.sys_table_field_id = strl.sys_table_field_id
