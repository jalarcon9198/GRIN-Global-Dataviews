SELECT
  spf.sys_permission_field_id,
  spf.sys_permission_id,
  spf.sys_dataview_field_id,
  spf.sys_table_field_id,
  spf.field_type,
  spf.compare_operator,
  dbms_lob.substr(spf.compare_value,32767,1) as compare_value,
  spf.parent_table_field_id,
  spf.parent_field_type,
  spf.parent_compare_operator,
  dbms_lob.substr(spf.parent_compare_value,32767,1) as parent_compare_value,
  spf.compare_mode
FROM sys_permission_field spf
  LEFT JOIN sys_group_permission_map sgpm ON sgpm.sys_permission_id = spf.sys_permission_id
  LEFT JOIN sys_group sg ON sg.sys_group_id = sgpm.sys_group_id
WHERE sg.owned_by = :cooperatorid
