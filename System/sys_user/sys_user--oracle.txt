SELECT sys_user.sys_user_id
      ,sys_user.user_name
      ,sys_user.password
      ,sys_user.is_enabled
      ,sys_user.cooperator_id
      ,LTRIM(RTRIM(NVL(co.last_name, '') || ', ' || NVL(co.first_name, '') || ', ' || NVL(co.organization, ''))) as cooperator_name
      ,sys_user.created_date
      ,sys_user.created_by
      ,sys_user.modified_date
      ,sys_user.modified_by
      ,sys_user.owned_date
      ,sys_user.owned_by
  FROM sys_user, cooperator co
  WHERE sys_user.cooperator_id = co.cooperator_id