SELECT st.table_name, stf.field_name, st.database_area_code, stf.field_type, stf.group_name FROM sys_search_autofield ssa
LEFT JOIN sys_table_field stf ON ssa.sys_table_field_id = stf.sys_table_field_id
LEFT JOIN sys_table st ON stf.sys_table_id = st.sys_table_id
