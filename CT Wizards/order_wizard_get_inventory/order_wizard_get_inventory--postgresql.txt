SELECT
  i.inventory_number_part1
  ,i.inventory_number_part2
  ,i.inventory_number_part3
  ,i.form_type_code
  ,(select c.site_id from cooperator c where c.cooperator_id = i.owned_by) AS site_id
  ,i.inventory_maint_policy_id
  ,i.is_distributable
  ,i.is_available
  ,i.availability_status_code
  ,i.availability_status_note
  ,i.quantity_on_hand
  ,i.quantity_on_hand_unit_code
  ,i.distribution_default_quantity
  ,i.distribution_unit_code
  ,i.pathogen_status_code
  ,(SELECT TOP 1 percent_viable FROM inventory_viability iv WHERE iv.inventory_id = i.inventory_id ORDER BY iv.tested_date DESC, inventory_viability_id DESC) AS percent_viable
  ,(SELECT TOP 1 tested_date FROM inventory_viability iv WHERE iv.inventory_id = i.inventory_id ORDER BY iv.tested_date DESC) AS tested_date
  ,i.storage_location_part1
  ,i.storage_location_part2
  ,i.storage_location_part3
  ,i.storage_location_part4
  ,(SELECT TOP 1 plant_name FROM accession_inv_name ain INNER JOIN inventory i2 ON i2.inventory_id = ain.inventory_id WHERE i.accession_id = i2.accession_id ORDER BY ain.plant_name_rank + CASE WHEN i2.inventory_id = i.inventory_id THEN 0 ELSE 10000 END) AS plant_name
  ,(select taxonomy_species_id from accession a where i.accession_id = a.accession_id) AS taxonomy_species_id
  ,(select s.geography_id from accession_source s where i.accession_id = s.accession_id and accession_source_id = ( select MIN(accession_source_id) from accession_source s2 where i.accession_id = s2.accession_id and is_origin = 'Y')) AS geography_id
  ,i.pollination_method_code
  ,i.pollination_vector_code
  ,i.note AS inventory_note
  ,(SELECT SUBSTRING((SELECT ', ' + ipr1.type_code AS [text()] FROM accession_ipr ipr1 WHERE ipr1.accession_id = i.accession_id AND ipr1.expired_date IS NULL ORDER BY ipr1.accession_id FOR XML PATH ('')), 2, 1000)) AS ipr_restriction
  ,(SELECT 'xPVP' FROM accession_ipr ipr2 WHERE ipr2.accession_id = i.accession_id AND ipr2.type_code = 'PVP' AND ipr2.expired_date IS NOT NULL) AS xpvp_warning
  ,(SELECT SUBSTRING((SELECT ', ' + iq.quarantine_type_code + ' = ' + iq.progress_status_code AS [text()] FROM accession_quarantine iq WHERE iq.accession_id = i.accession_id AND iq.released_date IS NULL ORDER BY iq.accession_id FOR XML PATH ('')), 2, 1000)) AS quarantine_restriction
  ,(SELECT SUBSTRING((SELECT ', ' + tn.noxious_type_code + ' in ' + adm1 + ', ' + cvl.title AS [text()] FROM taxonomy_noxious tn LEFT JOIN accession a ON tn.taxonomy_species_id = a.taxonomy_species_id LEFT JOIN inventory ii ON a.accession_id = ii.accession_id LEFT JOIN geography g ON tn.geography_id = g.geography_id LEFT JOIN code_value cv ON g.country_code = cv.value AND cv.group_name = 'GEOGRAPHY_COUNTRY_CODE' LEFT JOIN code_value_lang cvl ON cv.code_value_id = cvl.code_value_id and cvl.sys_lang_id = 1 WHERE ii.inventory_id = i.inventory_id ORDER BY tn.taxonomy_noxious_id FOR XML PATH ('')), 2, 1000)) AS noxious_restriction
  ,i.inventory_id
  ,i.accession_id
FROM
    inventory i
WHERE 
  i.accession_id IN (:accessionid)

UNION

SELECT
  i.inventory_number_part1
  ,i.inventory_number_part2
  ,i.inventory_number_part3
  ,i.form_type_code
  ,(select c.site_id from cooperator c where c.cooperator_id = i.owned_by) AS site_id
  ,i.inventory_maint_policy_id
  ,i.is_distributable
  ,i.is_available
  ,i.availability_status_code
  ,i.availability_status_note
  ,i.quantity_on_hand
  ,i.quantity_on_hand_unit_code
  ,i.distribution_default_quantity
  ,i.distribution_unit_code
  ,i.pathogen_status_code
  ,(SELECT TOP 1 percent_viable FROM inventory_viability iv WHERE iv.inventory_id = i.inventory_id ORDER BY iv.tested_date DESC, inventory_viability_id DESC) AS percent_viable
  ,(SELECT TOP 1 tested_date FROM inventory_viability iv WHERE iv.inventory_id = i.inventory_id ORDER BY iv.tested_date DESC) AS tested_date
  ,i.storage_location_part1
  ,i.storage_location_part2
  ,i.storage_location_part3
  ,i.storage_location_part4
  ,(SELECT TOP 1 plant_name FROM accession_inv_name ain INNER JOIN inventory i2 ON i2.inventory_id = ain.inventory_id WHERE i.accession_id = i2.accession_id ORDER BY ain.plant_name_rank + CASE WHEN i2.inventory_id = i.inventory_id THEN 0 ELSE 10000 END) AS plant_name
  ,(select taxonomy_species_id from accession a where i.accession_id = a.accession_id) AS taxonomy_species_id
  ,(select s.geography_id from accession_source s where i.accession_id = s.accession_id and accession_source_id = ( select MIN(accession_source_id) from accession_source s2 where i.accession_id = s2.accession_id and is_origin = 'Y')) AS geography_id
  ,i.pollination_method_code
  ,i.pollination_vector_code
  ,i.note AS inventory_note
  ,(SELECT SUBSTRING((SELECT ', ' + ipr1.type_code AS [text()] FROM accession_ipr ipr1 WHERE ipr1.accession_id = i.accession_id AND ipr1.expired_date IS NULL ORDER BY ipr1.accession_id FOR XML PATH ('')), 2, 1000)) AS ipr_restriction
  ,(SELECT 'xPVP' FROM accession_ipr ipr2 WHERE ipr2.accession_id = i.accession_id AND ipr2.type_code = 'PVP' AND ipr2.expired_date IS NOT NULL) AS xpvp_warning
  ,(SELECT SUBSTRING((SELECT ', ' + iq.quarantine_type_code + ' = ' + iq.progress_status_code AS [text()] FROM accession_quarantine iq WHERE iq.accession_id = i.accession_id AND iq.released_date IS NULL ORDER BY iq.accession_id FOR XML PATH ('')), 2, 1000)) AS quarantine_restriction
  ,(SELECT SUBSTRING((SELECT ', ' + tn.noxious_type_code + ' in ' + adm1 + ', ' + cvl.title AS [text()] FROM taxonomy_noxious tn LEFT JOIN accession a ON tn.taxonomy_species_id = a.taxonomy_species_id LEFT JOIN inventory ii ON a.accession_id = ii.accession_id LEFT JOIN geography g ON tn.geography_id = g.geography_id LEFT JOIN code_value cv ON g.country_code = cv.value AND cv.group_name = 'GEOGRAPHY_COUNTRY_CODE' LEFT JOIN code_value_lang cvl ON cv.code_value_id = cvl.code_value_id and cvl.sys_lang_id = 1 WHERE ii.inventory_id = i.inventory_id ORDER BY tn.taxonomy_noxious_id FOR XML PATH ('')), 2, 1000)) AS noxious_restriction
  ,i.inventory_id
  ,i.accession_id
FROM
    inventory i
WHERE 
  i.inventory_id IN (:inventoryid)