SELECT 
  i.inventory_id
  ,'/' + tg.genus_name + '/' + CAST(i.accession_id AS VARCHAR) + '/' AS attach_filepath
FROM 
  taxonomy_genus tg
  LEFT JOIN taxonomy_species ts ON tg.taxonomy_genus_id = ts.taxonomy_genus_id
  LEFT JOIN accession a ON ts.taxonomy_species_id = a.taxonomy_species_id
  LEFT JOIN inventory i ON a.accession_id = i.accession_id
WHERE
  i.inventory_id IN (:inventoryid)
