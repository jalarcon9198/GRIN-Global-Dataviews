SELECT
  i.inventory_id,
  i.inventory_number_part1,
  i.inventory_number_part2,
  i.inventory_number_part3,
  i.form_type_code,
  i.accession_id,
  i.inventory_maint_policy_id,
  (select s.site_id from site s where s.site_id in (select c.site_id from cooperator c where c.cooperator_id = i.owned_by)) AS site_id,
  i.is_distributable,
  i.is_auto_deducted,
  i.is_available,
  i.availability_status_code,
  i.availability_status_note,
  i.availability_start_date,
  i.availability_end_date,
  i.web_availability_note,
  i.quantity_on_hand,
  i.quantity_on_hand_unit_code,
  i.distribution_default_form_code,
  i.distribution_default_quantity,
  i.distribution_unit_code,
  i.distribution_critical_quantity,
  i.regeneration_critical_quantity,
  i.pathogen_status_code,
  i.storage_location_part1,
  i.storage_location_part2,
  i.storage_location_part3,
  i.storage_location_part4,
  i.latitude,
  i.longitude,
  i.rootstock,
  i.parent_inventory_id,
  i.backup_inventory_id,
  i.hundred_seed_weight,
  i.pollination_method_code,
  i.pollination_vector_code,
  i.preservation_method_id,
  i.regeneration_method_id,
  i.plant_sex_code,
  i.propagation_date_code,
  i.propagation_date,
  i.note,
  (select plant_name from accession_name an where i.accession_id = an.accession_id and plant_name_rank = (select MIN(plant_name_rank) from accession_name an2 where i.accession_id = an2.accession_id) LIMIT 1) AS plant_name,
  (select taxonomy_species_id from accession a where i.accession_id = a.accession_id) AS taxonomy_species_id,
  (select s.geography_id from accession_source s where i.accession_id = s.accession_id and accession_source_id = ( select MIN(accession_source_id) from accession_source s2 where i.accession_id = s2.accession_id and is_origin = 'Y')) AS geography_id,
  (select percent_viable from inventory_viability v1 where v1.inventory_viability_id = (select max(v2.inventory_viability_id) from inventory_viability v2 where i.inventory_id = v2.inventory_id and v2.tested_date = (select max(v3.tested_date) from inventory_viability v3 where i.inventory_id = v3.inventory_id))) AS percent_viable,
  (select tested_date from inventory_viability v1 where i.inventory_id = v1.inventory_id and v1.inventory_viability_id = (select max(v2.inventory_viability_id) from inventory_viability v2 where i.inventory_id = v2.inventory_id and v2.tested_date = (select max(v3.tested_date) from inventory_viability v3 where i.inventory_id = v3.inventory_id))) AS tested_date,
  (SELECT inventory_id FROM inventory WHERE inventory_id = i.inventory_id) AS inventory_id_string,
  i.created_date,
  i.created_by,
  i.modified_date,
  i.modified_by,
  i.owned_date,
  i.owned_by
FROM
    inventory i
    INNER JOIN (
        SELECT inventory_id FROM inventory WHERE inventory_id IN (:inventoryid)

        UNION SELECT inventory_id FROM inventory
            WHERE accession_id IN (:accessionid)

        UNION SELECT inventory_id FROM inventory
            WHERE inventory_maint_policy_id IN (:inventorymaintpolicyid)

        UNION SELECT DISTINCT inventory_id FROM order_request_item
            WHERE order_request_id in (:orderrequestid)

        UNION SELECT DISTINCT CASE i1.form_type_code
                         WHEN '**' THEN i2.inventory_id
                         ELSE i1.inventory_id
                     END AS inventory_id
            FROM accession_inv_group_map aigm
            LEFT JOIN inventory i1 ON i1.inventory_id = aigm.inventory_id
            LEFT JOIN inventory i2 ON i2.accession_id = i1.accession_id
            WHERE aigm.accession_inv_group_id IN (:accessioninvgroupid)
    ) list ON list.inventory_id = i.inventory_id