SELECT 	ora.order_request_attach_id,
	ora.order_request_id,
	ora.virtual_path,
	ora.thumbnail_virtual_path,
	ora.sort_order,
	ora.title,
	ora.description,
	ora.content_type,
	ora.category_code,
	ora.is_web_visible,
	ora.copyright_information,
	ora.attach_cooperator_id,
        ora.attach_date,
        ora.attach_date_code,
	ora.note,
	ora.created_date,
	ora.created_by,
	ora.modified_date,
	ora.modified_by,
	ora.owned_date,
	ora.owned_by
FROM
    order_request_attach ora
    INNER JOIN (
        SELECT ora.order_request_attach_id FROM order_request_attach ora
            WHERE ora.order_request_attach_id IN (:orderrequestattachid)

        UNION SELECT ora.order_request_attach_id FROM order_request_attach ora
            WHERE ora.order_request_id IN (:orderrequestid)

        UNION SELECT ora.order_request_attach_id FROM order_request_attach ora
            INNER JOIN order_request_item ori ON ori.order_request_id = ora.order_request_id
            WHERE ori.inventory_id in (:inventoryid)

        UNION SELECT ora.order_request_attach_id FROM order_request_attach ora
            INNER JOIN order_request_item ori ON ori.order_request_id = ora.order_request_id
            INNER JOIN inventory i ON i.inventory_id = ori.inventory_id
            WHERE i.accession_id in (:accessionid)

    ) list ON list.order_request_attach_id = ora.order_request_attach_id
