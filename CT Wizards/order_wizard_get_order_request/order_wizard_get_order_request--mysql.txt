SELECT
     DISTINCT(o.order_request_id)
    ,o.ordered_date
    ,o.web_order_request_id 
    ,o.original_order_request_id
    ,o.local_number
    ,c.site_id AS owner_site_id    
    ,o.order_type_code
    ,o.intended_use_code
    ,o.intended_use_note
    ,o.completed_date
    ,(SELECT COUNT(*) FROM order_request_item ori WHERE ori.order_request_id = o.order_request_id) AS items
    ,o.final_recipient_cooperator_id
    ,o.requestor_cooperator_id
    ,o.ship_to_cooperator_id
    ,o.order_obtained_via
    ,o.special_instruction
    ,o.note
    ,wor.web_cooperator_id
    ,wc.email
    ,wc.primary_phone
    ,o.created_date
    ,o.created_by
    ,o.modified_date
    ,o.modified_by
    ,o.owned_date
    ,o.owned_by
FROM
    order_request o
    LEFT JOIN cooperator c ON o.owned_by = c.cooperator_id
    LEFT JOIN web_order_request wor ON o.web_order_request_id = wor.web_order_request_id
    LEFT JOIN web_cooperator wc ON wor.web_cooperator_id = wc.web_cooperator_id
WHERE
    o.order_request_id IN (:orderrequestid)
