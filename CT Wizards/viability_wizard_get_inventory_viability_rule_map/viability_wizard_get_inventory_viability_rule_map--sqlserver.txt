SELECT
  ivrm.inventory_viability_rule_map_id,
  ivrm.inventory_viability_rule_id,
  ivrm.taxonomy_species_id,
  ivrm.note,
  ivrm.created_date,
  ivrm.created_by,
  ivrm.modified_date,
  ivrm.modified_by,
  ivrm.owned_date,
  ivrm.owned_by 
FROM
  inventory_viability_rule_map AS ivrm
WHERE
  ivrm.inventory_viability_rule_map_id in (:inventoryviabilityrulemapid)

UNION

SELECT
  ivrm.inventory_viability_rule_map_id,
  ivrm.inventory_viability_rule_id,
  ivrm.taxonomy_species_id,
  ivrm.note,
  ivrm.created_date,
  ivrm.created_by,
  ivrm.modified_date,
  ivrm.modified_by,
  ivrm.owned_date,
  ivrm.owned_by
FROM
  inventory_viability_rule_map AS ivrm
WHERE
  ivrm.inventory_viability_rule_id in (:inventoryviabilityruleid)

UNION

SELECT
  ivrm.inventory_viability_rule_map_id,
  ivrm.inventory_viability_rule_id,
  ivrm.taxonomy_species_id,
  ivrm.note,
  ivrm.created_date,
  ivrm.created_by,
  ivrm.modified_date,
  ivrm.modified_by,
  ivrm.owned_date,
  ivrm.owned_by
FROM
  inventory_viability_rule_map AS ivrm
WHERE
  ivrm.taxonomy_species_id in (:taxonomyspeciesid)

UNION

SELECT
  ivrm.inventory_viability_rule_map_id,
  ivrm.inventory_viability_rule_id,
  ivrm.taxonomy_species_id,
  ivrm.note,
  ivrm.created_date,
  ivrm.created_by,
  ivrm.modified_date,
  ivrm.modified_by,
  ivrm.owned_date,
  ivrm.owned_by
FROM
  inventory_viability_rule_map AS ivrm
WHERE
  ':inventoryviabilityrulemapid' = '-1'
  AND
  ':inventoryviabilityruleid' = '-1'
  AND
  ':taxonomyspeciesid' = '-1'