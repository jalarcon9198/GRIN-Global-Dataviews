SELECT  
     ora.order_request_action_id
    ,ora.order_request_id
    ,ora.action_name_code
    ,ora.started_date
    ,ora.started_date_code
    ,ora.action_information
    ,ora.completed_date
    ,ora.completed_date_code
    ,ora.action_cost
    ,ora.cooperator_id
    ,ora.note
    ,ora.created_date
    ,ora.created_by
    ,ora.modified_date
    ,ora.modified_by
    ,ora.owned_date
    ,ora.owned_by
FROM
    order_request_action ora
WHERE
    ora.order_request_id in (:orderrequestid)
