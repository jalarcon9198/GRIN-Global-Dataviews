SELECT
  oi.order_request_item_id
  ,oi.order_request_id
  ,oi.sequence_number
  ,i.accession_id
  ,oi.inventory_id
  ,(select c.site_id from cooperator c where c.cooperator_id = i.owned_by) AS site_id
  ,oi.name
  ,oi.external_taxonomy
  ,(select s.geography_id from accession_source s where i.accession_id = s.accession_id and accession_source_id = ( select MIN(accession_source_id) from accession_source s2 where i.accession_id = s2.accession_id and is_origin = 'Y')) AS geography_id
  ,i.quantity_on_hand
  ,i.quantity_on_hand_unit_code
  ,i.availability_status_code
  ,oi.quantity_shipped
  ,oi.quantity_shipped_unit_code
  ,oi.distribution_form_code
  ,oi.status_code
  ,oi.status_date
  ,oi.note
  ,oi.source_cooperator_id
  ,(SELECT SUBSTRING((SELECT ', ' + ipr1.type_code AS [text()] FROM accession_ipr ipr1 WHERE ipr1.accession_id = i.accession_id AND ipr1.expired_date IS NULL ORDER BY ipr1.accession_id FOR XML PATH ('')), 2, 1000)) AS ipr_restriction
  ,(SELECT 'xPVP' FROM accession_ipr ipr2 WHERE ipr2.accession_id = i.accession_id AND ipr2.type_code = 'PVP' AND ipr2.expired_date IS NOT NULL) AS xpvp_warning
  ,(SELECT SUBSTRING((SELECT ', ' + iq.quarantine_type_code + ' = ' + iq.progress_status_code AS [text()] FROM accession_quarantine iq WHERE iq.accession_id = i.accession_id AND iq.released_date IS NULL ORDER BY iq.accession_id FOR XML PATH ('')), 2, 1000)) AS quarantine_restriction
  ,(SELECT SUBSTRING((SELECT ', ' + tn.noxious_type_code + ' in ' + adm1 + ', ' + cvl.title AS [text()] FROM taxonomy_noxious tn LEFT JOIN accession a ON tn.taxonomy_species_id = a.taxonomy_species_id LEFT JOIN inventory ii ON a.accession_id = ii.accession_id LEFT JOIN geography g ON tn.geography_id = g.geography_id LEFT JOIN code_value cv ON g.country_code = cv.value AND cv.group_name = 'GEOGRAPHY_COUNTRY_CODE' LEFT JOIN code_value_lang cvl ON cv.code_value_id = cvl.code_value_id and cvl.sys_lang_id = 1 WHERE ii.inventory_id = i.inventory_id ORDER BY tn.taxonomy_noxious_id FOR XML PATH ('')), 2, 1000)) AS noxious_restriction
  ,oi.web_order_request_item_id
FROM
    order_request_item oi
    INNER JOIN inventory i ON  oi.inventory_id = i.inventory_id 
WHERE order_request_id IN (:orderrequestid)