SELECT DISTINCT(inventory.inventory_id) FROM inventory
LEFT JOIN accession ON inventory.accession_id = accession.accession_id
LEFT JOIN taxonomy_geography_map ON accession.taxonomy_species_id = taxonomy_geography_map.taxonomy_species_id
WHERE __searchcriteria__