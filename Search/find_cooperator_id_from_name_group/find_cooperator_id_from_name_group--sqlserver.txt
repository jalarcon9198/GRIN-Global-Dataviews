SELECT DISTINCT(accession_source_map.cooperator_id)
FROM 
  accession_source_map, accession_source, inventory, accession_inv_name, name_group
WHERE
  accession_source_map.accession_source_id = accession_source.accession_source_id AND
  inventory.inventory_id = accession_inv_name.inventory_id AND
  inventory.accession_id = accession_source.accession_id AND
  accession_inv_name.name_group_id = name_group.name_group_id AND
  __searchcriteria__
