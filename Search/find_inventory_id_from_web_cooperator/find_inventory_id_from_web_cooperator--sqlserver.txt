SELECT DISTINCT(inventory.inventory_id) FROM inventory
INNER JOIN web_order_request_item
        ON web_order_request_item.accession_id = inventory.accession_id
INNER JOIN web_cooperator
        ON web_cooperator.web_cooperator_id = web_order_request_item.web_cooperator_id 
WHERE __searchcriteria__