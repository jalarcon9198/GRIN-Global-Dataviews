SELECT DISTINCT(inventory.accession_id) FROM inventory
LEFT JOIN genetic_observation ON inventory.inventory_id = genetic_observation.inventory_id
LEFT JOIN genetic_annotation ON genetic_observation.genetic_annotation_id = genetic_annotation.genetic_annotation_id
LEFT JOIN genetic_marker ON genetic_annotation.genetic_marker_id = genetic_marker.genetic_marker_id
WHERE __searchcriteria__