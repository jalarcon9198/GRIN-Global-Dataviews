SELECT DISTINCT(cooperator.cooperator_id) FROM cooperator
INNER JOIN web_cooperator
        ON web_cooperator.web_cooperator_id = cooperator.web_cooperator_id 
INNER JOIN web_order_request
        ON web_order_request.web_cooperator_id = web_cooperator.web_cooperator_id
WHERE __searchcriteria__