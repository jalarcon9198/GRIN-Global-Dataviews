SELECT DISTINCT(order_request_item.order_request_id) FROM order_request_item
LEFT JOIN inventory ON order_request_item.inventory_id = inventory.inventory_id
LEFT JOIN accession ON inventory.accession_id = accession.accession_id
LEFT JOIN inventory_viability_rule ON accession.taxonomy_species_id = inventory_viability_rule.taxonomy_species_id
WHERE __searchcriteria__