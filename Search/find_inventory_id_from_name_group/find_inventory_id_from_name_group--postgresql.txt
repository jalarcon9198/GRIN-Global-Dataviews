SELECT DISTINCT 
    CASE i.form_type_code
        WHEN '**' THEN i2.inventory_id
        ELSE i.inventory_id
    END AS inventory_id
FROM name_group
    INNER JOIN accession_inv_name ain ON ain.name_group_id = name_group.name_group_id
    LEFT JOIN inventory i ON ain.inventory_id = i.inventory_id
    LEFT JOIN inventory i2 ON i.accession_id = i2.accession_id
WHERE __searchcriteria__