SELECT DISTINCT(inventory.inventory_id) FROM inventory
LEFT JOIN accession ON inventory.accession_id = accession.accession_id
LEFT JOIN taxonomy_noxious ON accession.taxonomy_species_id = taxonomy_noxious.taxonomy_species_id
WHERE __searchcriteria__