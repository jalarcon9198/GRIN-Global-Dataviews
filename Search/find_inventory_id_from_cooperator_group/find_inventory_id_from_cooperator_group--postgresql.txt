SELECT DISTINCT(inventory.inventory_id) FROM inventory
LEFT JOIN accession_source ON inventory.accession_id = accession_source.accession_id
LEFT JOIN accession_source_map ON accession_source.accession_source_id = accession_source_map.accession_source_id
LEFT JOIN cooperator_map ON accession_source_map.cooperator_id = cooperator_map.cooperator_id
LEFT JOIN cooperator_group ON cooperator_map.cooperator_group_id = cooperator_group.cooperator_group_id
WHERE __searchcriteria__