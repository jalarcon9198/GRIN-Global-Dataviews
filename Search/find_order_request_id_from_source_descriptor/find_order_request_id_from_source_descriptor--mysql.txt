SELECT DISTINCT(order_request_item.order_request_id) FROM order_request_item
LEFT JOIN inventory
       inventory.inventory_id = ON order_request_item.inventory_id
LEFT JOIN accession_source ON
       ON accession_source.accession_id = inventory.accession_id
INNER JOIN source_desc_observation
        ON source_desc_observation.accession_source_id = accession_source.accession_source_id 
INNER JOIN source_descriptor
        ON source_descriptor.source_descriptor_id = source_desc_observation.source_descriptor_id 
WHERE __searchcriteria__