SELECT DISTINCT(order_request.order_request_id) FROM order_request
LEFT JOIN cooperator_map ON order_request.final_recipient_cooperator_id = cooperator_map.cooperator_id
LEFT JOIN cooperator_group ON cooperator_map.cooperator_group_id = cooperator_group.cooperator_group_id
WHERE __searchcriteria__