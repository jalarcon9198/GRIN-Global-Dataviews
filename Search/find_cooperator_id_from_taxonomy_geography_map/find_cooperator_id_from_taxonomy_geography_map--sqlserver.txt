SELECT DISTINCT(accession_source_map.cooperator_id) FROM accession_source_map
LEFT JOIN accession_source ON accession_source_map.accession_source_id = accession_source.accession_source_id
LEFT JOIN accession ON accession_source.accession_id = accession.accession_id
LEFT JOIN taxonomy_geography_map ON accession.taxonomy_species_id = taxonomy_geography_map.taxonomy_species_id
WHERE __searchcriteria__