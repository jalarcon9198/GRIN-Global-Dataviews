SELECT DISTINCT(inventory.inventory_id) FROM inventory
INNER JOIN accession_source 
        ON accession_source.accession_id = inventory.accession_id
INNER JOIN source_desc_observation
        ON source_desc_observation.accession_source_id = accession_source.accession_source_id 
INNER JOIN source_descriptor_lang
        ON source_descriptor_lang.source_descriptor_id = source_desc_observation.source_descriptor_id 
WHERE __searchcriteria__