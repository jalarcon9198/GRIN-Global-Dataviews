SELECT DISTINCT(cooperator.cooperator_id) FROM cooperator
LEFT JOIN geography ON cooperator.geography_id = geography.geography_id
LEFT JOIN code_value ON geography.country_code = code_value.value
LEFT JOIN code_value_lang ON code_value.code_value_id = code_value_lang.code_value_id 
WHERE __searchcriteria__
