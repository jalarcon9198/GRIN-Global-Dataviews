SELECT DISTINCT(inventory.inventory_id) FROM inventory
LEFT JOIN cooperator ON inventory.owned_by = cooperator.cooperator_id
LEFT JOIN site ON cooperator.site_id = site.site_id
WHERE __searchcriteria__