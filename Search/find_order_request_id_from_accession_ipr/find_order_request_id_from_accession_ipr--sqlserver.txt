SELECT DISTINCT(order_request_item.order_request_id) FROM order_request_item
LEFT JOIN inventory ON order_request_item.inventory_id = inventory.inventory_id
LEFT JOIN accession_ipr ON inventory.accession_id = accession_ipr.accession_id
WHERE __searchcriteria__