SELECT DISTINCT(inventory.inventory_id)
FROM inventory
    INNER JOIN citation ON inventory.accession_id = citation.accession_id
WHERE __searchcriteria__