SELECT DISTINCT(accession_source.accession_id) FROM accession_source
LEFT JOIN geography_region_map ON accession_source.geography_id = geography_region_map.geography_id
LEFT JOIN region ON geography_region_map.region_id = region.region_id
WHERE __searchcriteria__