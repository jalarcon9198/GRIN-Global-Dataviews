SELECT DISTINCT(order_request.order_request_id) FROM order_request
LEFT JOIN cooperator ON order_request.owned_by = cooperator.cooperator_id
LEFT JOIN site ON cooperator.site_id = site.site_id
WHERE __searchcriteria__