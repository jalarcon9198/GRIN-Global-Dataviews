SELECT DISTINCT(order_request_item.order_request_id) FROM order_request_item
LEFT JOIN inventory ON order_request_item.inventory_id = inventory.inventory_id
LEFT JOIN accession ON inventory.accession_id = accession.accession_id
LEFT JOIN taxonomy_species ON accession.taxonomy_species_id = taxonomy_species.taxonomy_species_id
LEFT JOIN taxonomy_genus ON taxonomy_species.taxonomy_genus_id = taxonomy_genus.taxonomy_genus_id
LEFT JOIN taxonomy_family ON taxonomy_genus.taxonomy_family_id = taxonomy_family.taxonomy_family_id
WHERE __searchcriteria__