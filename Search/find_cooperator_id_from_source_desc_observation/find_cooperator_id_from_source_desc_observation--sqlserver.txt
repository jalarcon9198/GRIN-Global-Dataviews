SELECT DISTINCT(accession_source_map.cooperator_id) FROM accession_source_map
INNER JOIN accession_source
        ON accession_source.accession_source_id = accession_source_map.accession_source_id
INNER JOIN source_desc_observation
        ON source_desc_observation.accession_source_id = accession_source.accession_source_id 
WHERE __searchcriteria__