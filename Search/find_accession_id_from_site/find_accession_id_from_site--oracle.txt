SELECT DISTINCT(accession.accession_id) FROM accession
LEFT JOIN cooperator ON accession.owned_by = cooperator.cooperator_id
LEFT JOIN site ON cooperator.site_id = site.site_id
WHERE __searchcriteria__