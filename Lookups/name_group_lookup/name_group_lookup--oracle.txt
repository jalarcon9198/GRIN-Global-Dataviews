SELECT
  ng.name_group_id value_member,
  ng.group_name display_member
FROM
  name_group ng
WHERE
  ((to_char(ng.created_date,'YYYY-MM-DD') > nvl(to_char(:createddate,'YYYY-MM-DD'), '1753-01-01'))
   OR (to_char(ng.modified_date,'YYYY-MM-DD') > nvl(to_char(:modifieddate,'YYYY-MM-DD'), '1753-01-01'))
   OR (ng.name_group_id IN (:valuemember))
   OR (ng.name_group_id BETWEEN :startpkey AND :stoppkey)
  )