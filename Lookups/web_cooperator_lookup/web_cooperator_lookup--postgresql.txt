SELECT
  DISTINCT wc.web_cooperator_id AS value_member,
  LTRIM(RTRIM(COALESCE(wc.last_name, '') || ', ' || COALESCE(wc.first_name, '') || ', ' || COALESCE(wc.organization, ''))) AS display_member,
  COALESCE(wc.is_active, 'N') AS is_active
FROM
    web_cooperator wc
WHERE
  ((wc.created_date > COALESCE(:createddate, '1753-01-01'))
   OR (wc.modified_date > COALESCE(:modifieddate, '1753-01-01'))
   OR (wc.web_cooperator_id IN (:valuemember))
   OR (wc.web_cooperator_id BETWEEN :startpkey AND :stoppkey))

