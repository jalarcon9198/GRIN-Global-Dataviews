SELECT
  imp.inventory_maint_policy_id value_member,
  imp.maintenance_name display_member
FROM
  inventory_maint_policy imp
WHERE
  ((to_char(imp.created_date,'YYYY-MM-DD') > nvl(to_char(:createddate,'YYYY-MM-DD'), '1753-01-01'))
   OR (to_char(imp.modified_date,'YYYY-MM-DD') > nvl(to_char(:modifieddate,'YYYY-MM-DD'), '1753-01-01'))
   OR (imp.inventory_maint_policy_id IN (:valuemember))
   OR (imp.inventory_maint_policy_id BETWEEN :startpkey AND :stoppkey)
  )