SELECT
  r.region_id AS value_member,
  CONCAT(r.continent,
    CASE WHEN r.subcontinent IS NULL THEN '' ELSE CONCAT(', ', r.subcontinent) END
    ) AS display_member
FROM
  region r
WHERE
  ((r.created_date > COALESCE(:createddate, '1753-01-01'))
   OR (r.modified_date > COALESCE(:modifieddate, '1753-01-01'))
   OR (r.region_id IN (:valuemember))
   OR (r.region_id BETWEEN :startpkey AND :stoppkey))
ORDER BY 2
