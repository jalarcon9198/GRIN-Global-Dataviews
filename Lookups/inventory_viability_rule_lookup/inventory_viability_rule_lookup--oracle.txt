/* Inventory Viability Rule Lookup */
SELECT
  ivr.inventory_viability_rule_id as value_member,
  COALESCE(ivr.name,'') || ' - ' || COALESCE(ts.name, '') as display_member
FROM
  inventory_viability_rule  ivr 
  LEFT JOIN taxonomy_species ts	ON ivr.taxonomy_species_id = ts.taxonomy_species_id
WHERE
  ((ivr.created_date > COALESCE(:createddate, to_date('1753-01-01','YYYY-MM-DD')))
    OR (ivr.modified_date > COALESCE(:modifieddate, to_date('1753-01-01','YYYY-MM-DD')))
    OR (ivr.inventory_viability_rule_id IN (:valuemember))
    OR (ivr.inventory_viability_rule_id BETWEEN :startpkey AND :stoppkey)
    OR (ts.created_date > COALESCE(:createddate, to_date('1753-01-01','YYYY-MM-DD')))
    OR (ts.modified_date > COALESCE(:modifieddate, to_date('1753-01-01','YYYY-MM-DD')))
  )