SELECT
  ctc.crop_trait_code_id AS value_member
  ,ctc.crop_trait_id AS crop_trait_id
  ,COALESCE(ctcl.title, ctc.code) AS display_member
FROM
  crop_trait_code ctc 
  LEFT JOIN crop_trait_code_lang ctcl ON ctc.crop_trait_code_id = ctcl.crop_trait_code_id AND ctcl.sys_lang_id = __LANGUAGEID__
WHERE
  ((ctc.created_date > COALESCE(:createddate, '1753-01-01'))
    OR (ctc.modified_date > COALESCE(:modifieddate, '1753-01-01'))
    OR (ctc.crop_trait_code_id IN (:valuemember))
    OR (ctc.crop_trait_code_id BETWEEN :startpkey AND :stoppkey)
    OR (ctcl.created_date > COALESCE(:createddate, '1753-01-01'))
    OR (ctcl.modified_date > COALESCE(:modifieddate, '1753-01-01'))
  )
