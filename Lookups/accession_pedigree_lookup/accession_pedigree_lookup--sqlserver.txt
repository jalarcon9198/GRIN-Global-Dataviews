SELECT
  ped.accession_pedigree_id AS value_member
  ,LTRIM(RTRIM(COALESCE(CONVERT(NVARCHAR, LTRIM(RTRIM(COALESCE(a.accession_number_part1, '') + ' ' + 
               COALESCE(CONVERT(NVARCHAR, a.accession_number_part2), '') + ' ' + 
               COALESCE(a.accession_number_part3, '')))), '') + ' ' + 
               COALESCE(CONVERT(NVARCHAR, ped.released_date, 101), ''))) AS display_member
FROM
  accession_pedigree ped
  LEFT JOIN accession a ON ped.accession_id = a.accession_id
WHERE
  ((ped.created_date > COALESCE(:createddate, '1753-01-01'))
    OR (ped.modified_date > COALESCE(:modifieddate, '1753-01-01'))
    OR (ped.accession_pedigree_id IN (:valuemember))
    OR (ped.accession_pedigree_id BETWEEN :startpkey AND :stoppkey)
    OR (a.created_date > COALESCE(:createddate, '1753-01-01'))
    OR (a.modified_date > COALESCE(:modifieddate, '1753-01-01'))
  )
