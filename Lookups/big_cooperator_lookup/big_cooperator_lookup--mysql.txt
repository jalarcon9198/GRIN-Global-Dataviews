SELECT
  DISTINCT co.cooperator_id AS value_member,
  LTRIM(RTRIM(COALESCE(co.last_name, '') || ', ' || 
              COALESCE(co.first_name, '') || ', ' || 
              COALESCE(co.organization, '') || ', ' || 
              COALESCE(co.address_line1, '') || ', ' ||
              COALESCE(co.city, '') || ', ' ||  
              COALESCE(CONVERT(g.adm1,char),'') || ', ' || 
              COALESCE(cvl.title,g.country_code))) AS display_member,
  s.site_short_name AS site,
  COALESCE(su.is_enabled, 'N') AS account_is_enabled,
  CASE WHEN co.cooperator_id = co.current_cooperator_id THEN 'Y' ELSE 'N' END AS is_current_address
FROM
  cooperator co 
  LEFT JOIN sys_user su ON co.cooperator_id = su.cooperator_id
  LEFT JOIN site s ON co.site_id = s.site_id
  LEFT JOIN geography g ON co.geography_id = g.geography_id
  LEFT JOIN code_value cv ON g.country_code = cv.value AND cv.group_name = 'GEOGRAPHY_COUNTRY_CODE'
  LEFT JOIN code_value_lang cvl ON cv.code_value_id = cvl.code_value_id AND cvl.sys_lang_id = __LANGUAGEID__
WHERE
  ((co.created_date > COALESCE(:createddate, '1753-01-01'))
    OR (co.modified_date > COALESCE(:modifieddate, '1753-01-01'))
    OR (co.cooperator_id IN (:valuemember))
    OR (co.cooperator_id BETWEEN :startpkey AND :stoppkey)
    OR (su.created_date > COALESCE(:createddate, '1753-01-01'))
    OR (su.modified_date > COALESCE(:modifieddate, '1753-01-01'))
    OR (s.created_date > COALESCE(:createddate, '1753-01-01'))
    OR (s.modified_date > COALESCE(:modifieddate, '1753-01-01'))
    OR (g.created_date > COALESCE(:createddate, '1753-01-01'))
    OR (g.modified_date > COALESCE(:modifieddate, '1753-01-01'))
    OR (cv.created_date > COALESCE(:createddate, '1753-01-01'))
    OR (cv.modified_date > COALESCE(:modifieddate, '1753-01-01'))
    OR (cvl.created_date > COALESCE(:createddate, '1753-01-01'))
    OR (cvl.modified_date > COALESCE(:modifieddate, '1753-01-01'))
  )

