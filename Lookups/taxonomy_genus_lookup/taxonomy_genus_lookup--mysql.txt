SELECT
  tg.taxonomy_genus_id AS value_member
,SUBSTR(concat(LTRIM(RTRIM(COALESCE(tg.genus_name, '')))
 , CASE COALESCE(convert(tg.subgenus_name,char), '') WHEN '' THEN '' ELSE concat('' , ' subg. ' , 
         LTRIM(RTRIM(tg.subgenus_name))) END
 , CASE COALESCE(convert(tg.section_name,char), '') WHEN '' THEN '' ELSE concat('' , ' sect. ' , 
         LTRIM(RTRIM(tg.section_name))) END
 , CASE COALESCE(convert(tg.subsection_name,char), '') WHEN '' THEN '' ELSE concat('' , ' subsect. ' , 
         LTRIM(RTRIM(tg.subsection_name))) END
 , CASE COALESCE(convert(tg.series_name,char), '') WHEN '' THEN '' ELSE concat('' , ' ser. ' , 
         LTRIM(RTRIM(tg.series_name))) END
 , CASE COALESCE(convert(tg.subseries_name,char), '') WHEN '' THEN '' ELSE concat('' , ' subser. ' , 
         LTRIM(RTRIM(tg.subseries_name))) END
       ),1,100)
AS display_member
  ,CASE 
     WHEN taxonomy_genus_id = current_taxonomy_genus_id THEN 'Y'
     ELSE 'N'
   END AS is_accepted_genus
FROM
  taxonomy_genus tg
WHERE
  ((tg.created_date > COALESCE(:createddate, '1753-01-01'))
   OR (tg.modified_date > COALESCE(:modifieddate, '1753-01-01'))
   OR (tg.taxonomy_genus_id IN (:valuemember))
   OR (tg.taxonomy_genus_id BETWEEN :startpkey AND :stoppkey)
  )
