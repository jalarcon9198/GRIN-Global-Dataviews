SELECT
  sl.sys_lang_id value_member,
  sl.title display_member
FROM
  sys_lang sl
WHERE
  ((to_char(sl.created_date,'YYYY-MM-DD') > nvl(to_char(:createddate,'YYYY-MM-DD'), '1753-01-01'))
   OR (to_char(sl.modified_date,'YYYY-MM-DD') > nvl(to_char(:modifieddate,'YYYY-MM-DD'), '1753-01-01'))
   OR (sl.sys_lang_id IN (:valuemember))
   OR (sl.sys_lang_id BETWEEN :startpkey AND :stoppkey)
  )