SELECT
  ts.taxonomy_species_id AS value_member,
  CASE
     WHEN EXISTS (SELECT * FROM taxonomy_species ts2 WHERE ts.name = ts2.name AND ts.taxonomy_species_id != ts2.taxonomy_species_id)
     THEN CONCAT(ts.name, CONCAT(' ', COALESCE(ts.name_authority, '')))
     ELSE ts.name
   END AS display_member,
  CASE
     WHEN taxonomy_species_id = current_taxonomy_species_id THEN 'Y'
     ELSE 'N'
   END AS is_accepted_name
FROM
  taxonomy_species ts
WHERE
  ((ts.created_date > COALESCE(:createddate, '1753-01-01'))
   OR (ts.modified_date > COALESCE(:modifieddate, '1753-01-01'))
   OR (ts.taxonomy_species_id IN (:valuemember))
   OR (ts.taxonomy_species_id BETWEEN :startpkey AND :stoppkey)
  )