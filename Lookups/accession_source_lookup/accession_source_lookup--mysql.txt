SELECT
  asrc.accession_source_id AS value_member
  ,LTRIM(RTRIM(concat(COALESCE(convert(LTRIM(RTRIM(concat(COALESCE(a.accession_number_part1, '') , ' ' ,
   COALESCE(convert(a.accession_number_part2,char), '') , ' ' ,COALESCE(a.accession_number_part3, '')))),char), '') ,' ' , COALESCE(convert(asrc.source_type_code,char), '') ,' ' , COALESCE(convert(asrc.source_date, date), '')))) AS display_member
FROM
  accession_source asrc
  LEFT JOIN accession a ON asrc.accession_id = a.accession_id
WHERE
  ((asrc.created_date > COALESCE(:createddate, '1753-01-01'))
   OR (asrc.modified_date > COALESCE(:modifieddate, '1753-01-01'))
   OR (asrc.accession_source_id IN (:valuemember))
   OR (asrc.accession_source_id BETWEEN :startpkey AND :stoppkey)
   OR (a.created_date > COALESCE(:createddate, '1753-01-01'))
   OR (a.modified_date > COALESCE(:modifieddate, '1753-01-01'))
  )
