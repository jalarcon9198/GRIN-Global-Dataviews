SELECT
  tu.taxonomy_use_id AS value_member
  ,(SELECT ts.name FROM taxonomy_species ts WHERE ts.taxonomy_species_id = tu.taxonomy_species_id)
    + ' - ' + tu.economic_usage_code
    + CASE WHEN tu.usage_type IS NOT NULL THEN ' - ' + tu.usage_type ELSE '' END
     AS display_member
FROM
  taxonomy_use tu
WHERE
  ((tu.created_date > COALESCE(:createddate, '1753-01-01'))
   OR (tu.modified_date > COALESCE(:modifieddate, '1753-01-01'))
   OR (tu.taxonomy_use_id IN (:valuemember))
   OR (tu.taxonomy_use_id BETWEEN :startpkey AND :stoppkey)
 )
