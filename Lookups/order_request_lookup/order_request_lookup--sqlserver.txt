SELECT
  oreq.order_request_id AS value_member,
  COALESCE(CONVERT(NVARCHAR, oreq.order_request_id),'') + ' - ' + COALESCE(c.last_name,'') + ', ' + COALESCE(c.first_name,'') + ', ' + COALESCE(c.organization,'') as display_member
FROM
    order_request AS oreq
	LEFT JOIN cooperator c ON oreq.final_recipient_cooperator_id = c.cooperator_id
where
  ((oreq.created_date > COALESCE(:createddate, '1753-01-01'))
    OR (oreq.modified_date > COALESCE(:modifieddate, '1753-01-01'))
    OR (oreq.order_request_id IN (:valuemember))
    OR (oreq.order_request_id BETWEEN :startpkey AND :stoppkey)
    OR (c.created_date > COALESCE(:createddate, '1753-01-01'))
    OR (c.modified_date > COALESCE(:modifieddate, '1753-01-01'))
  )




