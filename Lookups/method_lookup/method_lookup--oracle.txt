SELECT
  m.method_id value_member,
  m.name display_member
FROM
  method m
WHERE
  ((to_char(m.created_date,'YYYY-MM-DD') > nvl(to_char(:createddate,'YYYY-MM-DD'), '1753-01-01'))
   OR (to_char(m.modified_date,'YYYY-MM-DD') > nvl(to_char(:modifieddate,'YYYY-MM-DD'), '1753-01-01'))
   OR (m.method_id IN (:valuemember))
   OR (m.method_id BETWEEN :startpkey AND :stoppkey)
  )