SELECT
  i.inventory_id AS value_member,
  LTRIM(RTRIM(LTRIM(COALESCE(i.inventory_number_part1, '') || ' ') || LTRIM(to_char(COALESCE( to_char(i.inventory_number_part2), '')) || ' ') || LTRIM(COALESCE(i.inventory_number_part3, '') || ' ') || COALESCE(cvl.title, i.form_type_code))) AS display_member,
  i.accession_id
FROM
  inventory i
  LEFT JOIN code_value cv ON i.form_type_code = cv.value AND cv.group_name = 'GERMPLASM_FORM'
  LEFT JOIN code_value_lang cvl ON cv.code_value_id = cvl.code_value_id AND cvl.sys_lang_id = __LANGUAGEID__
WHERE
  (    (i.created_date > COALESCE(:createddate, to_date('1753-01-01','YYYY-MM-DD')))
    OR (i.modified_date > COALESCE(:modifieddate, to_date('1753-01-01','YYYY-MM-DD')))
    OR (i.inventory_id IN (:valuemember))
    OR (i.inventory_id BETWEEN :startpkey AND :stoppkey)
    OR (cv.created_date > COALESCE(:createddate, to_date('1753-01-01','YYYY-MM-DD')))
    OR (cv.modified_date > COALESCE(:modifieddate, to_date('1753-01-01','YYYY-MM-DD')))
    OR (cvl.created_date > COALESCE(:createddate, to_date('1753-01-01','YYYY-MM-DD')))
    OR (cvl.modified_date > COALESCE(:modifieddate, to_date('1753-01-01','YYYY-MM-DD')))
  )
