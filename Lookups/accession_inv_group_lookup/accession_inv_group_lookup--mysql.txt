select 
  ag.accession_inv_group_id as value_member,
  ag.group_name as display_member
from
  accession_inv_group ag
WHERE
  ((ag.created_date > COALESCE(:createddate, '1753-01-01'))
   OR (ag.modified_date > COALESCE(:modifieddate, '1753-01-01'))
   OR (ag.accession_inv_group_id IN (:valuemember))
   OR (ag.accession_inv_group_id BETWEEN :startpkey AND :stoppkey))