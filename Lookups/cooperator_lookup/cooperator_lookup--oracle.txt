/* Cooperator Lookup */
SELECT
  DISTINCT co.cooperator_id AS value_member,
  LTRIM(RTRIM(COALESCE(co.last_name, '') || ', ' || COALESCE(co.first_name, '') || ', ' || COALESCE(co.organization, ''))) AS display_member,
  s.site_short_name AS site, 
  COALESCE(su.is_enabled, 'N') AS account_is_enabled,
  CASE WHEN co.cooperator_id = co.current_cooperator_id THEN 'Y' ELSE 'N' END AS is_current_address
FROM
  cooperator  co 
  LEFT JOIN sys_user  su ON co.cooperator_id = su.cooperator_id
  LEFT JOIN site s ON co.site_id = s.site_id
WHERE
  (    (co.created_date > COALESCE(:createddate, to_date('1753-01-01','YYYY-MM-DD')))
    OR (co.modified_date > COALESCE(:modifieddate, to_date('1753-01-01','YYYY-MM-DD')))
    OR (co.cooperator_id IN (:valuemember))
    OR (co.cooperator_id BETWEEN :startpkey AND :stoppkey)
    OR (su.created_date > COALESCE(:createddate, to_date('1753-01-01','YYYY-MM-DD')))
    OR (su.modified_date > COALESCE(:modifieddate, to_date('1753-01-01','YYYY-MM-DD')))
    OR (s.created_date > COALESCE(:createddate, to_date('1753-01-01','YYYY-MM-DD')))
    OR (s.modified_date > COALESCE(:modifieddate, to_date('1753-01-01','YYYY-MM-DD')))
  )