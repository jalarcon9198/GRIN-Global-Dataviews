SELECT
  cit.citation_id AS value_member,
  COALESCE('. ' + l.abbreviation, '')
    + COALESCE( '. ' + COALESCE(cit.citation_title, cit.title), '')
    + COALESCE(cit.author_name, 'Unrecorded Author')
    + COALESCE( '. ' + CONVERT(NVARCHAR, cit.citation_year), '')
    + COALESCE( '. ' + cit.reference, '')
    AS display_member,
  cit.taxonomy_species_id,
  ts.current_taxonomy_species_id
FROM
  citation cit
  LEFT JOIN literature l ON cit.literature_id = l.literature_id
  INNER JOIN taxonomy_species ts ON cit.taxonomy_species_id = ts.taxonomy_species_id
WHERE
  ((cit.created_date > COALESCE(:createddate, '1753-01-01'))
    OR (cit.modified_date > COALESCE(:modifieddate, '1753-01-01'))
    OR (cit.citation_id IN (:valuemember))
    OR (cit.citation_id BETWEEN :startpkey AND :stoppkey)
    OR (l.created_date > COALESCE(:createddate, '1753-01-01'))
    OR (l.modified_date > COALESCE(:modifieddate, '1753-01-01'))
  )
