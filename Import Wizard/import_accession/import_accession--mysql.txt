SELECT
  ts.name AS taxonomy_taxon,
  ts.name_authority AS taxonomy_name_authority,
  a.accession_number_part1, 
  a.accession_number_part2, 
  a.accession_number_part3,
  a.is_core,
  a.status_code,
  a.life_form_code,
  a.improvement_status_code,
  a.reproductive_uniformity_code,
  a.initial_received_form_code,
  a.initial_received_date,
  a.initial_received_date_code,
  a.note AS accession_note,
  a.is_backed_up,
  bs1.site_short_name AS back_site1_short_name,
  bs1.organization_abbrev AS back_site1_organization_abbrev,
  bs1.site_long_name AS back_site1_long_name,
  bs1.fao_institute_number AS back_site1_faoinstitute_number,
  bs2.site_short_name AS back_site2_short_name,
  bs2.organization_abbrev AS back_site2_organization_abbrev,
  bs2.site_long_name AS back_site2_long_name,
  bs2.fao_institute_number AS back_site2_faoinstitute_number
FROM
    accession a
    LEFT JOIN taxonomy_species ts
      ON  ts.taxonomy_species_id = a.taxonomy_species_id 
    LEFT JOIN site bs1
      ON  a.backup_location1_site_id = bs1.site_id 
    LEFT JOIN site bs2
      ON  a.backup_location2_site_id = bs2.site_id 

WHERE
  1 = :viewallrows