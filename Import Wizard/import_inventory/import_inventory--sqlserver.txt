SELECT
  a.accession_number_part1,
  a.accession_number_part2,
  a.accession_number_part3,
  i.inventory_number_part1,
  i.inventory_number_part2,
  i.inventory_number_part3,
  i.form_type_code,
  i.is_distributable,
  i.is_auto_deducted,
  i.is_available,
  i.availability_status_code,
  i.availability_status_note,
  i.availability_start_date,
  i.availability_end_date,
  i.web_availability_note,
  i.quantity_on_hand,
  i.quantity_on_hand_unit_code,
  i.distribution_default_form_code,
  i.distribution_default_quantity,
  i.distribution_unit_code,
  i.distribution_critical_quantity,
  i.regeneration_critical_quantity,
  i.pathogen_status_code,
  i.storage_location_part1,
  i.storage_location_part2,
  i.storage_location_part3,
  i.storage_location_part4,
  i.latitude,
  i.longitude,
  i.rootstock,
  i.hundred_seed_weight,
  i.pollination_method_code,
  i.pollination_vector_code,
  i.note,
  imp.maintenance_name,
  ip.inventory_number_part1 AS ip_inventory_number_part1,
  ip.inventory_number_part2 AS ip_inventory_number_part2,
  ip.inventory_number_part3 AS ip_inventory_number_part3,
  ip.form_type_code AS ip_form_type_code,
  ip.availability_status_code AS ip_availability_status_code,
  impp.maintenance_name AS ip_maintenance_name,
  ib.inventory_number_part1 AS ib_inventory_number_part1,
  ib.inventory_number_part2 AS ib_inventory_number_part2,
  ib.inventory_number_part3 AS ib_inventory_number_part3,
  ib.form_type_code AS ib_form_type_code,
  ib.availability_status_code AS ib_availability_status_code,
  impb.maintenance_name AS ib_maintenance_name
FROM
    inventory i
    INNER JOIN accession a
      ON  a.accession_id = i.accession_id 
    LEFT JOIN inventory_maint_policy imp
      ON  imp.inventory_maint_policy_id = i.inventory_maint_policy_id 
    LEFT JOIN inventory ip
      ON  i.parent_inventory_id = ip.inventory_id 
    LEFT JOIN inventory_maint_policy impp
      ON  impp.inventory_maint_policy_id = ip.inventory_maint_policy_id 
    LEFT JOIN inventory ib
      ON  i.backup_inventory_id = ib.inventory_id 
    LEFT JOIN inventory_maint_policy impb
      ON  impb.inventory_maint_policy_id = ib.inventory_maint_policy_id 

WHERE 1 = :viewallrows
