SELECT
  imp.maintenance_name,
  imp.form_type_code,
  imp.quantity_on_hand_unit_code,
  imp.web_availability_note,
  imp.is_auto_deducted,
  imp.distribution_default_form_code,
  imp.distribution_default_quantity,
  imp.distribution_unit_code,
  imp.distribution_critical_quantity,
  imp.regeneration_critical_quantity,
  imp.regeneration_method_code,
  imp.note,
  g.country_code AS curator_country,
  g.adm1 AS curator_adm1,
  g.adm2 AS curator_adm2,
  g.adm3 AS curator_adm3,
  g.adm4 AS curator_adm4,
  c.last_name AS curator_last_name,
  c.first_name AS curator_first_name,
  c.organization AS curator_organization,
  c.address_line1 AS curator_address_line1,
  sl.ietf_tag AS ietf_language_tag
FROM
    inventory_maint_policy imp
    LEFT JOIN cooperator c
      ON  imp.curator_cooperator_id = c.cooperator_id 
    LEFT JOIN geography g
      ON  c.geography_id = g.geography_id 
    LEFT JOIN sys_lang sl
      ON  sl.sys_lang_id = c.sys_lang_id 

WHERE 
1 = :viewallrows
