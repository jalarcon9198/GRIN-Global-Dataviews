SELECT
  a.accession_number_part1,
  a.accession_number_part2,
  a.accession_number_part3,
  g.country_code,
  g.adm1,
  g.adm2,
  g.adm3,
  g.adm4,
  c.last_name,
  c.first_name,
  c.organization,
  c.address_line1,
  ng.group_name,
  an.plant_name,
  an.category_code,
  an.plant_name_rank,
  an.note,
  a.taxonomy_species_id,
  c.sys_lang_id,
  i.inventory_number_part1,
  i.inventory_number_part2,
  i.inventory_number_part3,
  i.form_type_code

FROM
    accession a
    INNER JOIN inventory i
      ON i.accession_id = a.accession_id
    INNER JOIN accession_inv_name an
      ON  an.inventory_id = i.inventory_id 
    LEFT JOIN cooperator c
      ON  an.name_source_cooperator_id = c.cooperator_id 
    LEFT JOIN geography g
      ON  c.geography_id = g.geography_id 
    LEFT JOIN name_group ng
      ON  ng.name_group_id = an.name_group_id 

WHERE 
1 = :viewallrows
