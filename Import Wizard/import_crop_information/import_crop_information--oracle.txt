SELECT
  crop.name,
  crop.note AS crop_note,
  ct.category_code,
  ct.coded_name,
  ctl.title AS ct_title,
  ctl.description AS ct_description,
  ct.is_peer_reviewed,
  ct.data_type_code,
  ct.is_coded,
  ct.max_length,
  ct.numeric_format,
  ct.numeric_maximum,
  ct.numeric_minimum,
  ct.original_value_type_code,
  ct.original_value_format,
  ct.is_archived,
  ct.ontology_url,
  dbms_lob.substr(ct.note,32767,1) AS trait_note,
  ctc.code,
  ctcl.title AS code_title,
  ctcl.description AS code_description
FROM
    crop crop
    LEFT JOIN crop_trait ct
      ON  ct.crop_id = crop.crop_id 
    LEFT JOIN crop_trait_code ctc
      ON  ctc.crop_trait_id = ct.crop_trait_id 
    LEFT JOIN crop_trait_code_lang ctcl
      ON  ctcl.crop_trait_code_id = ctc.crop_trait_code_id 
          and  ctcl.sys_lang_id = :langid 
    LEFT JOIN crop_trait_lang ctl
      ON  ctl.crop_trait_id = ct.crop_trait_id 
          and  ctl.sys_lang_id = :langid 

WHERE 1 = :viewallrows
