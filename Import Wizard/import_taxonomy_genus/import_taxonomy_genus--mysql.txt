SELECT
  tg.qualifying_code AS qualifying_code,
  tg.is_hybrid AS is_hybrid,
  tg.genus_name AS genus_name,
  tg.genus_authority AS genus_authority,
  tg.subgenus_name AS subgenus_name,
  tg.section_name AS section_name,
  tg.subsection_name AS subsection_name,  
  tg.series_name AS series_name,
  tg.subseries_name AS subseries_name,
  tg.note AS note,
  tf.family_name AS family_family_name,
  tf.family_authority AS family_authority,
  tf.subfamily_name AS family_subfamily_name,
  tf.tribe_name AS family_tribe_name, 
  tf.subtribe_name AS family_subtribe_name,
  tg1.genus_name AS current_genus_name,
  tg1.genus_authority AS current_genus_authority,
  tg1.subgenus_name AS current_subgenus_name,
  tg1.section_name AS current_section_name,
  tg1.subsection_name AS current_subsection_name,
  tg1.series_name AS current_series_name,
  tg1.subseries_name AS current_subseries_name,
  tf1.family_name AS family1_family_name,
  tf1.family_authority AS family1_authority,
  tf1.subfamily_name AS family1_subfamily_name,
  tf1.tribe_name AS family1_tribe_name, 
  tf1.subtribe_name AS family1_subtribe_name
FROM 
taxonomy_genus tg
    LEFT JOIN taxonomy_family tf
      ON tf.taxonomy_family_id = tg.taxonomy_family_id
    LEFT JOIN taxonomy_genus tg1
      ON  tg.current_taxonomy_genus_id = tg1.taxonomy_genus_id 
    LEFT JOIN taxonomy_family tf1
      ON tf1.taxonomy_family_id = tg1.taxonomy_family_id

where 1 = :viewallrows