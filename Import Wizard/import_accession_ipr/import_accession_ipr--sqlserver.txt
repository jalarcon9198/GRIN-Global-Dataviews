SELECT
  a.accession_number_part1,
  a.accession_number_part2,
  a.accession_number_part3,
  g.country_code,
  g.adm1,
  g.adm2,
  g.adm3,
  g.adm4,
  c.last_name,
  c.first_name,
  c.organization,
  c.address_line1,
  ipr.type_code,
  ipr.ipr_number,
  ipr.ipr_crop_name,
  ipr.ipr_full_name,
  ipr.issued_date,
  ipr.expired_date,
  ipr.accepted_date,
  ipr.expected_date,
  ipr.note,
  c.sys_lang_id
FROM
    accession a
    INNER JOIN accession_ipr ipr
      ON  a.accession_id = ipr.accession_id 
      LEFT JOIN cooperator c
      ON ipr.cooperator_id = c.cooperator_id 
          LEFT JOIN geography g
          ON g.geography_id = c.geography_id

WHERE 
1 = :viewallrows
