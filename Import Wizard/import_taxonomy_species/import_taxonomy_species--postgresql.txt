SELECT
  tf.family_name,
  tf.family_authority,
  tf.subfamily_name AS family_subfamily_name,
  tf.tribe_name AS family_tribe_name,
  tf.subtribe_name AS family_subtribe_name,
  tg.genus_name,
  tg.genus_authority,
  tg.subgenus_name AS genus_subgenus_name,
  tg.section_name AS genus_section_name,
  tg.subsection_name AS genus_subsection_name,
  tg.series_name AS genus_series_name,
  tg.subseries_name AS genus_subseries_name,
  ts.nomen_number,
  ts.is_specific_hybrid,
  ts.species_name,
  ts.species_authority,
  ts.is_subspecific_hybrid,
  ts.subspecies_name,
  ts.subspecies_authority,
  ts.is_varietal_hybrid,
  ts.variety_name,
  ts.variety_authority,
  ts.is_subvarietal_hybrid,
  ts.subvariety_name,
  ts.subvariety_authority,
  ts.is_forma_hybrid,
  ts.forma_rank_type,
  ts.forma_name,
  ts.forma_authority,
  ts.restriction_code,
  ts.life_form_code,
  ts.common_fertilization_code,
  ts.is_name_pending,
  ts.synonym_code,
  ts.name_verified_date,
  ts.name,
  ts.name_authority,
  ts.protologue,
  ts.alternate_name,
  ts.note,
  ts.site_note,
  s1.site_short_name AS priority1_site,
  s1.site_long_name AS priority1_site_long_name,
  s1.organization_abbrev AS priority1_organization_abbrev,
  s1.fao_institute_number AS priority1_fao_institute_number,
  s2.site_short_name AS priority2_site,
  s2.site_long_name AS priority2_site_long_name,
  s2.organization_abbrev AS priority2_organization_abbrev,
  s2.fao_institute_number AS priority2_fao_institute_number,
  g1.country_code AS curator1_country_code,
  g1.adm1 AS curator1_adm1,
  g1.adm2 AS curator1_adm2,
  g1.adm3 AS curator1_adm3,
  g1.adm4 AS curator1_adm4,
  c1.last_name AS curator1_last_name,
  c1.first_name AS curator1_first_name,
  c1.organization AS curator1_organization,
  c1.address_line1 AS curator1_address_line1,
  lang1.ietf_tag AS curator1_language,
  g2.country_code AS curator2_country_code,
  g2.adm1 AS curator2_adm1,
  g2.adm2 AS curator2_adm2,
  g2.adm3 AS curator2_adm3,
  g2.adm4 AS curator2_adm4,
  c2.last_name AS curator2_last_name,
  c2.first_name AS curator2_first_name,
  c2.organization AS curator2_organization,
  c2.address_line1 AS curator2_address_line1,
  lang2.ietf_tag AS curator2_language,
  g3.country_code AS verifier_country_code,
  g3.adm1 AS verifier_adm1,
  g3.adm2 AS verifier_adm2,
  g3.adm3 AS verifier_adm3,
  g3.adm4 AS verifier_adm4,
  c3.last_name AS verifier_last_name,
  c3.first_name AS verifier_first_name,
  c3.organization AS verifier_organization,
  c3.address_line1 AS verifier_address_line1,
  lang3.ietf_tag AS verifier_language,
  tf1.family_name AS current_family_name,
  tf1.family_authority AS current_family_authority,
  tf1.subfamily_name AS current_subfamily_name,
  tf1.tribe_name AS current_tribe_name,
  tf1.subtribe_name AS current_subtribe_name,
  tg1.genus_name AS current_genus_name,
  tg1.genus_authority AS current_genus_authority,
  tg1.subgenus_name AS current_subgenus_name,
  tg1.section_name AS current_section_name,
  tg1.subsection_name AS current_subsection_name,
  tg1.series_name AS current_series_name,
  tg1.subseries_name AS current_subseries_name,
  ts1.species_name AS current_species_name,
  ts1.name AS current_name,
  ts1.name_authority AS current_name_authority
FROM 
    taxonomy_species ts 
    LEFT JOIN taxonomy_genus tg
      ON  ts.taxonomy_genus_id = tg.taxonomy_genus_id
    LEFT JOIN taxonomy_family tf
      ON  tg.taxonomy_family_id = tf.taxonomy_family_id
    LEFT JOIN site s1
      ON  ts.priority1_site_id = s1.site_id
    LEFT JOIN site s2
      ON  ts.priority2_site_id = s2.site_id
    LEFT JOIN cooperator c1
      ON ts.curator1_cooperator_id = c1.cooperator_id
    LEFT JOIN geography g1
      ON c1.geography_id = g1.geography_id
    LEFT JOIN sys_lang lang1
      ON lang1.sys_lang_id = c1.sys_lang_id
    LEFT JOIN cooperator c2
      ON ts.curator2_cooperator_id = c2.cooperator_id
    LEFT JOIN geography g2
      ON c2.geography_id = g2.geography_id
    LEFT JOIN sys_lang lang2
      ON lang2.sys_lang_id = c2.sys_lang_id
    LEFT JOIN cooperator c3
      ON ts.verifier_cooperator_id = c3.cooperator_id
    LEFT JOIN geography g3
      ON c3.geography_id = g3.geography_id
    LEFT JOIN sys_lang lang3
      ON lang3.sys_lang_id = c3.sys_lang_id
    LEFT JOIN taxonomy_species ts1
      ON ts.current_taxonomy_species_id = ts1.taxonomy_species_id
    LEFT JOIN taxonomy_genus tg1
      ON  ts1.taxonomy_genus_id = tg1.taxonomy_genus_id
    LEFT JOIN taxonomy_family tf1
      ON  tg1.taxonomy_family_id = tf1.taxonomy_family_id

where 1 = :viewallrows