SELECT
  sl.ietf_tag as ietf_language_tag,
  c.last_name,
  c.title,
  c.first_name,
  c.job,
  c.organization,
  c.organization_abbrev,
  c.address_line1,
  c.address_line2,
  c.address_line3,
  c.city,
  c.postal_index,
  c.secondary_organization,
  c.secondary_organization_abbrev,
  c.secondary_address_line1,
  c.secondary_address_line2,
  c.secondary_address_line3,
  c.secondary_city,
  c.secondary_postal_index,
  c.primary_phone,
  c.secondary_phone,
  c.fax,
  c.email,
  c.secondary_email,
  c.status_code,
  c.category_code,
  c.organization_region_code,
  c.discipline_code,
  c.note,
  g.country_code,
  g.adm1,
  g.adm2,
  g.adm3,
  g.adm4,
  g.changed_date,
  g.note AS g_note,
  s.site_short_name, 
  s.site_long_name,
  s.organization_abbrev AS s_organization_abbrev,
  s.is_internal,
  s.is_distribution_site,
  s.type_code,
  s.fao_institute_number,
  s.note AS s_note
FROM
    cooperator c
    LEFT JOIN sys_lang sl
      ON  sl.sys_lang_id = c.sys_lang_id 
    LEFT JOIN geography g
      ON  c.geography_id = g.geography_id 
    LEFT JOIN site s
      ON  c.site_id = s.site_id 

WHERE 
	1 = :viewallrows
