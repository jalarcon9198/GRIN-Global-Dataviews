select 
	cv.value, 
	cvl.title,
	0 as seq
from 
	code_value cv
     	join code_value_lang cvl 
	on  cv.code_value_id = cvl.code_value_id 
where 
	lower(replace(cv.group_name, '_', ' ')) = :groupname 
	and cvl.sys_lang_id = :langid

union all

select 'Null' as value, '(not defined)' as title, 1 as seq

order by seq, title
