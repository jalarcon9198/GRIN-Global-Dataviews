select distinct 
	usage_type as code,
	usage_type as text 
from 
	taxonomy_use 
where 
	economic_usage_code = :usagecode 
	and usage_type not like 'potential%'
	and :usagecode != 'CTIES'

union select  'CITESI' as code, 'CITES Appendix I' as text  where :usagecode = 'CITES'
union select  'CITESII' as code, 'CITES Appendix II' as text  where :usagecode = 'CITES'
union select  'CITESIII' as code, 'CITES Appendix III' as text  where :usagecode = 'CITES'
	
order by 2
