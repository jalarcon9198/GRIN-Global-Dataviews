select
    0 as quantity,
    a.accession_id,
    concat(coalesce(a.accession_number_part1,''), ' ', coalesce(convert(a.accession_number_part2, char),''), ' ', coalesce(a.accession_number_part3,'')) as pi_number,
    s.site_short_name as site,
    s.site_id,
    (select cvl.description from code_value cv left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id where cv.group_name = 'GERMPLASM_FORM' and cvl.sys_lang_id = __LANGUAGEID__ and cv.value = a.initial_received_form_code) as distributed_type,
    (select plant_name from accession_inv_name an join inventory i on an.inventory_id = i.inventory_id where a.accession_id = i.accession_id and plant_name_rank = (select MIN(plant_name_rank) from accession_inv_name an2 join inventory i2 on an2.inventory_id = i2.inventory_id where a.accession_id = i2.accession_id) limit 1) as top_name,
    (select i.distribution_default_quantity from inventory i where i.accession_id = a.accession_id and i.is_distributable = 'Y' and is_available = 'Y' limit 1) as standard_distribution_quantity,
    (select coalesce(cvl.title, cvl.description) from code_value cv left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id and cv.group_name = 'UNIT_OF_QUANTITY' and cvl.sys_lang_id = __LANGUAGEID__ 
    left join inventory i on cv.value = i.distribution_unit_code and i.is_distributable = 'Y' and is_available = 'Y' and i.accession_id  in (:idlist) limit 1) as standard_distribution_unit,
    t.taxonomy_species_id,
    t.name as taxonomy_name,
    (select coalesce(aipr.type_code, '') from accession_ipr aipr where aipr.accession_id = a.accession_id and type_code like 'MTA%') as type_code
from
    accession a left join taxonomy_species t 
		on a.taxonomy_species_id = t.taxonomy_species_id
	left join cooperator c on a.owned_by = c.cooperator_id
	left join site s on c.site_id = s.site_id
where
    a.accession_id in (:idlist)
