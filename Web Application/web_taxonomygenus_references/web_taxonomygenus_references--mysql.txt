select
	cm.citation_id,
	c.literature_id,
	COALESCE (c.author_name, l.editor_author_name) as author,
	c.citation_year,
	COALESCE (c.citation_title, l.reference_title) as title,
	case when (COALESCE (l.standard_abbreviation, l.abbreviation)) = l.abbreviation then concat('(', l.abbreviation, ')')
		else l.standard_abbreviation
		end
	 as abbrev,
	COALESCE (c.reference, '') as refernece,
	c.note as cit_note,
	l.note
from taxonomy_genus tg 
	join citation c 
		on tg.taxonomy_genus_id = c.taxonomy_genus_id
	left join literature l
		on c.literature_id = l.literature_id
where tg.taxonomy_genus_id = :taxonomygenusid
order by author asc