select distinct
	a.accession_id,
	a.accession_number_part1 as accession_prefix,
	a.accession_number_part2 as accession_number,
	case 
		when ct.is_coded = 'Y' then ctc.code
    		when cto.numeric_value is not null then convert(varchar, cast(cto.numeric_value as float))    	
    	else 
        	cto.string_value 
    	end as observation_value,
	ct.coded_name as descriptor_name,
	m.name as method_name,
	a.accession_number_part3 as accession_suffix,
   	(select TOP 1 plant_name from accession_inv_name an join inventory i on an.inventory_id = i.inventory_id where a.accession_id = i.accession_id and plant_name_rank = (select MIN(plant_name_rank) from accession_inv_name an2 join inventory i2 on an2.inventory_id = i2.inventory_id where a.accession_id = i2.accession_id)) as plant_name,
	t.name as taxon,
	(select cvl.title
   		from code_value cv left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id 
   		where cv.group_name = 'GEOGRAPHY_COUNTRY_CODE' and cvl.sys_lang_id = __LANGUAGEID__ 
   		and cv.value = (select top 1 g.country_code from geography g join accession_source asr on g.geography_id = asr.geography_id
   		and asr.is_origin = 'Y' and asr.accession_id = a.accession_id)) as origin,
	cto.original_value,	
	cto.frequency,    	
	cto.minimum_value as low,
    	cto.maximum_value as hign,
    	cto.mean_value as mean,
    	cto.standard_deviation as sdev,
    	cto.sample_size as ssize,
	i.inventory_number_part1 as inventory_prefix,
	i.inventory_number_part2 as inventory_number,
	i.inventory_number_part3 as inventory_suffix,
	a.note as accession_comment
from 
	accession a 
	inner join inventory i 
        	on a.accession_id = i.accession_id 
    	join crop_trait_observation cto 
        	on cto.inventory_id = i.inventory_id 
    	join crop_trait ct 
        	on cto.crop_trait_id = ct.crop_trait_id 
    	left join crop_trait_code ctc 
        	on cto.crop_trait_code_id = ctc.crop_trait_code_id 
    	left join method m 
		on cto.method_id = m.method_id 
	join taxonomy_species t
		on a.taxonomy_species_id = t.taxonomy_species_id
where 
	a.accession_id in (select distinct i.accession_id from inventory i 
		join crop_trait_observation cto on i.inventory_id = cto.inventory_id 
	where :where )
order by a.accession_id, observation_value