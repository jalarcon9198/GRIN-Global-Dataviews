SELECT
  l.literature_id,
  l.abbreviation,
  l.standard_abbreviation,
  l.reference_title,
  l.editor_author_name,
  l.literature_type_code,
  l.publication_year,
  l.publisher_name,
  l.publisher_location,
  l.note,
  l.created_date,
  l.created_by,
  l.modified_date,
  l.modified_by,
  l.owned_date,
  l.owned_by
FROM
    literature l
where
   literature_id = :literature_id

