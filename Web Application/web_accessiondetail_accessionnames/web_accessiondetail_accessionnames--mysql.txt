select
	(select cvl.title from code_value cv left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id 
	where cv.group_name = 'ACCESSION_NAME_TYPE'  and cvl.sys_lang_id =  __LANGUAGEID__ and cv.value = an.category_code) as category,
	a.accession_id,
	an.plant_name as top_name,
	ng.group_name,
	coalesce(an.note, ng.note) as note,
	c.cooperator_id,
	LTRIM(RTRIM(concat(COALESCE(c.last_name, ''), ', ', COALESCE(c.first_name, ''), ' ', COALESCE(c.organization, '')))) AS full_name,
	an.plant_name_rank
from
	accession a
	left join accession_inv_name an
		on a.accession_id = an.accession_id
	left join name_group ng
		on an.name_group_id = ng.name_group_id
	left join cooperator c
		on an.name_source_cooperator_id = c.cooperator_id
where 
    a.accession_id = :accessionid
    and an.is_web_visible = 'Y'
order by 8 asc