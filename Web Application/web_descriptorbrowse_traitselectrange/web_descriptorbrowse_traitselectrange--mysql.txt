select
   case when cto.crop_trait_code_id is not null then
	concat(coalesce(ctc.code, ctcl.title), '=', coalesce(ctcl.description, ctc.code))
	else
		coalesce(string_value, convert(numeric_value, char))
	end as crop_trait_code_text,
    case when cto.crop_trait_code_id is not null then
        concat('crop_trait_code_id|', convert(cto.crop_trait_code_id, char))
    when cto.string_value is not null then
        concat('string_value|', cto.string_value)
    else
        concat('numeric_value|', convert(cto.numeric_value, char))
    end as field_name_value
from
    crop_trait_observation cto
    inner join crop_trait ct
        on cto.crop_trait_id = ct.crop_trait_id
    left join crop_trait_lang ctl
        on ct.crop_trait_id = ctl.crop_trait_id
        and ctl.sys_lang_id = :langid1
    left join crop_trait_code ctc
        on cto.crop_trait_code_id = ctc.crop_trait_code_id
    left join crop_trait_code_lang ctcl
        on ctc.crop_trait_code_id = ctcl.crop_trait_code_id
        and ctcl.sys_lang_id = :langid2
    left join inventory i 
        on cto.inventory_id = i.inventory_id
    left join accession a
        on i.accession_id = a.accession_id
where
    cto.numeric_value is null and
    cto.crop_trait_id = :traitid
    and cto.is_archived = 'N'
group by
	case when cto.crop_trait_code_id is not null then
		concat(coalesce(ctc.code, ctcl.title), '=', coalesce(ctcl.description, ctc.code))
	else
		coalesce(string_value, convert(numeric_value, char))
	end,
    case when cto.crop_trait_code_id is not null then
        concat('crop_trait_code_id|', convert(cto.crop_trait_code_id, char))
    when cto.string_value is not null then
        concat('string_value|', cto.string_value)
    else
        concat('numeric_value|', convert(cto.numeric_value, char))
    end
order by
	1, 2