select
   case when cto.crop_trait_code_id is not null then
	concat(coalesce(ctc.code, ctcl.title), '=', coalesce(ctcl.description, ctc.code))
	else
		coalesce(string_value, convert(nvarchar, numeric_value))
	end as crop_trait_code_text,
    case when cto.crop_trait_code_id is not null then
        concat('crop_trait_code_id|', convert(nvarchar, cto.crop_trait_code_id))
    when cto.string_value is not null then
        concat('string_value|', cto.string_value)
    else
        concat('numeric_value|', convert(nvarchar, cto.numeric_value))
    end as field_name_value
from
    crop_trait_observation cto
    inner join crop_trait ct
        on cto.crop_trait_id = ct.crop_trait_id
    left join crop_trait_lang ctl
        on ct.crop_trait_id = ctl.crop_trait_id
        and ctl.sys_lang_id = :langid1
    left join crop_trait_code ctc
        on cto.crop_trait_code_id = ctc.crop_trait_code_id
    left join crop_trait_code_lang ctcl
        on ctc.crop_trait_code_id = ctcl.crop_trait_code_id
        and ctcl.sys_lang_id = :langid2
    left join inventory i 
        on cto.inventory_id = i.inventory_id
    left join accession a
        on i.accession_id = a.accession_id
where
    cto.numeric_value is null and
    cto.crop_trait_id = :traitid
    and cto.is_archived = 'N'
group by
	case when cto.crop_trait_code_id is not null then
		concat(coalesce(ctc.code, ctcl.title), '=', coalesce(ctcl.description, ctc.code))
	else
		coalesce(string_value, convert(nvarchar, numeric_value))
	end,
    case when cto.crop_trait_code_id is not null then
        concat('crop_trait_code_id|', convert(nvarchar, cto.crop_trait_code_id))
    when cto.string_value is not null then
        concat('string_value|', cto.string_value)
    else
        concat('numeric_value|', convert(nvarchar, cto.numeric_value))
    end
order by
	1, 2