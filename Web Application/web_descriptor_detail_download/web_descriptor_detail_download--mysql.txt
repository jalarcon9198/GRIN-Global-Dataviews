select distinct
	a.accession_id,
	a.accession_number_part1 as accession_prefix,
	a.accession_number_part2 as accession_number,
	a.accession_number_part3 as accession_surfix,
	case 
		when ct.is_coded = 'Y' then ctc.code
    		when cto.numeric_value is not null then convert(cto.numeric_value, char)    	
    	else 
        	cto.string_value 
    	end as observation_value,
	ct.coded_name,
	m.name as method_name,
   	(select TOP 1 plant_name from accession_inv_name an join inventory i on an.inventory_id = i.inventory_id where a.accession_id = i.accession_id and plant_name_rank = (select MIN(plant_name_rank) from accession_inv_name an2 join inventory i2 on an2.inventory_id = i2.inventory_id where a.accession_id = i2.accession_id)) as plant_name,
	t.name as taxon,
	cvl.title as origin,
	acsr.source_type_code,
	acsr.latitude,
	acsr.longitude,
	cto.data_quality_code,	
	cto.original_value,	
	cto.frequency,    	
	cto.minimum_value as low,
    	cto.maximum_value as hign,
    	cto.mean_value as mean,
    	cto.standard_deviation as sdev,
    	cto.sample_size as ssize,
	i.inventory_number_part1 as inventory_prefix,
	i.inventory_number_part2 as inventory_number,
	i.inventory_number_part3 as inventory_suffix
from 
	accession a 
	inner join inventory i 
        	on a.accession_id = i.accession_id 
    	left join crop_trait_observation cto 
        	on cto.inventory_id = i.inventory_id 
    	left join crop_trait ct 
        	on cto.crop_trait_id = ct.crop_trait_id 
    	left join crop_trait_code ctc 
        	on cto.crop_trait_code_id = ctc.crop_trait_code_id 
    	left join method m 
		on cto.method_id = m.method_id 
	join taxonomy_species t
		on a.taxonomy_species_id = t.taxonomy_species_id
	join accession_source acsr
		on a.accession_id = acsr.accession_id
	join geography g
		on acsr.geography_id = g.geography_id
	join code_value cv
		on g.country_code = cv.value
	join code_value_lang cvl
		on cv.code_value_id = cvl.code_value_id
where 
	cv.group_name = 'GEOGRAPHY_COUNTRY_CODE'
	and cvl.sys_lang_id = __LANGUAGEID__
	and acsr.is_origin = 'Y'
	and ct.crop_trait_id = :traitid	
	and cto.method_id =
	case when :methodid = 0 then cto.method_id else :methodid end
order by observation_value, a.accession_id