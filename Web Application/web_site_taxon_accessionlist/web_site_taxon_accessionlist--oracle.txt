select
 	a.accession_id,
 	concat('<nobr><a onclick="javascript:return true;" href="~/accessiondetail.aspx?id=', cast(a.accession_id as varchar2(50)), '">', trim(CONCAT (COALESCE (a.accession_number_part1, ''), ' ', COALESCE (cast(a.accession_number_part2 as varchar2(50)), ''), ' ', COALESCE (a.accession_number_part3, ''))), '</a></nobr>') as pi_number,
 	(select plant_name from accession_inv_name an join inventory i on an.inventory_id = i.inventory_id where a.accession_id = i.accession_id and plant_name_rank = (select MIN(plant_name_rank) from accession_inv_name an2 join inventory i2 on an2.inventory_id = i2.inventory_id where a.accession_id = i2.accession_id) and rownum = 1) as top_name,
 	'<a href="~/taxonomydetail.aspx?id=' || cast(t.taxonomy_species_id as varchar2(50)) || '">' || t.name || '</a>' as taxonomy_name,
	(select cvl.title from 
	 code_value cv left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id
   	 where cv.group_name = 'GEOGRAPHY_COUNTRY_CODE'  and cvl.sys_lang_id = __LANGUAGEID__ 
   	and cv.value = (select g.country_code from geography g join accession_source asr on g.geography_id = asr.geography_id
        and asr.is_origin = 'Y' where asr.accession_id = a.accession_id and rownum = 1)) as origin,
 	(select coalesce(cvl.description, to_clob(cv.value)) from code_value cv left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id where cv.group_name = 'GERMPLASM_FORM' and cv.value = i.form_type_code and rownum = 1) as material_type,
  	concat('<a href="site.aspx?id=',cast(s.site_id as varchar2(50)), '">', s.site_short_name, '</a>') as maintained_by,
   	case when exists (select inventory_id from inventory where is_distributable = 'Y' AND  is_available = 'Y' and accession_id = a.accession_id) then
		case when sum(case when i.is_distributable = 'Y' AND i.is_available = 'Y' then 1 else 0 end) = 0 then
  			''
 		else 		
 			concat('<nobr><a href="~/cartview.aspx?action=add&id=', cast(a.accession_id as varchar2(50)), '">Add to Cart</a></nobr>')
		end 	
	else
 		'Not Available'
 	end as availability
from
 	accession a
 	left join taxonomy_species t 
  		on a.taxonomy_species_id = t.taxonomy_species_id
 	left join inventory i
  		on i.accession_id = a.accession_id
     		and i.form_type_code != '**'
	left join cooperator c on a.owned_by = c.cooperator_id
	left join site s on c.site_id = s.site_id
where 
	t.taxonomy_species_id = :taxonomyid
	and s.site_id = :siteid
	and a.is_web_visible = 'Y'
group by
 	a.accession_id,
 	concat('<nobr><a onclick="javascript:return true;" href="~/accessiondetail.aspx?id=', cast(a.accession_id as varchar2(50)), '">', trim(CONCAT (COALESCE (a.accession_number_part1, ''), ' ', COALESCE (cast(a.accession_number_part2 as varchar2(50)), ''), ' ', COALESCE (a.accession_number_part3, ''))), '</a></nobr>'),
 	t.taxonomy_species_id,
 	t.name,
 	i.form_type_code,
 	concat('<a href="site.aspx?id=',cast(s.site_id as varchar2(50)), '">', s.site_short_name, '</a>')
order by
	1