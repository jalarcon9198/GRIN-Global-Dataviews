select distinct 
 	concat(cast(g.geography_id as varchar2(50)), ':', g.country_code) as countrycode, 
	cvl.title as countryname 
from 
	geography g 
	join code_value cv 
		on g.country_code = cv.value
	join code_value_lang cvl 
		on cv.code_value_id = cvl.code_value_id 
	join accession_source acs
		on g.geography_id = acs.geography_id
where 
	cv.group_name = 'GEOGRAPHY_COUNTRY_CODE'
	and cvl.sys_lang_id = :languageid 
	and g.adm1 is null and g.adm2 is null and g.adm3 is null and g.adm4 is null
 	and country_code not like '%[0-9]%'
order by countryname