select 
		cvl.title as title,   
		aipr.accession_ipr_id,
		aipr.type_code,
		aipr.ipr_number,   
		aipr.ipr_crop_name,   
		aipr.issued_date,
		aipr.expired_date,
		aipr.note as comment1, 
 		c.author_name,   
		c.citation_title,   
		c.citation_year,   
		c.reference,   
		c.note  
	from accession a   
	join accession_ipr aipr    
		on a.accession_id = aipr.accession_id   
	left join code_value cv    
	    on aipr.type_code = cv.value   
	left join code_value_lang cvl    
	    on cv.code_value_id = cvl.code_value_id                 
	    and cvl.sys_lang_id = __LANGUAGEID__   
	left join citation c   
	    on aipr.accession_ipr_id = c.accession_ipr_id   
	where a.accession_id = :accessionid   
	    and cv.group_name = 'ACCESSION_RESTRICTION_TYPE'