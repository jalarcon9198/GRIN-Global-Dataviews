SELECT
  concat(fr.title, ' - ',  fff.title) as full_field_title,
  concat(convert(fr.feedback_report_id, char), '|', convert(fff.feedback_form_field_id, char)) as full_field_key,
  fr.feedback_report_id,
  fr.title as report_tile,
  ff.feedback_form_id as feedback_form_id,
  ff.title AS ff_title,
  fff.feedback_form_field_id as feedback_form_field_id,
  fff.title AS fff_title
FROM
    feedback_report fr 
    LEFT JOIN feedback_form ff
      ON  fr.feedback_form_id = ff.feedback_form_id 
    LEFT JOIN feedback_form_field fff
      ON  fff.feedback_form_id = ff.feedback_form_id 
where
	fr.feedback_report_id != :feedbackreportid
	and fr.feedback_id = :feedbackid
order by 
1