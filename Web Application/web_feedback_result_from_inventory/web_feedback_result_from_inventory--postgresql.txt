select
	fr.feedback_result_id
from
	feedback_result fr
where
	fr.feedback_result_group_id = coalesce(:feedbackresultgroupid, fr.feedback_result_group_id)
	and fr.inventory_id = coalesce(:inventoryid, fr.inventory_id)
