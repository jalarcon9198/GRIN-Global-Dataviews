select 
    tg.taxonomy_genus_id as value,
    tg.genus_name,
    concat(tg.genus_name, ' ', coalesce(tg.subgenus_name, ''), ' ', coalesce(tg.section_name, ''), ' ', coalesce(tg.subsection_name, ''),  ' ', coalesce(tg.series_name, ''), ' ', coalesce(tg.subseries_name,''), ' (', replace(convert(convert(count(distinct a.accession_id), decimal), char), '.00', ''), ')') as display_text,
    COUNT(distinct a.accession_id) as accession_count
from 
	taxonomy_genus tg :jointype join taxonomy_species t
		on tg.taxonomy_genus_id = t.taxonomy_genus_id
	:jointype join accession a
		on t.taxonomy_species_id = a.taxonomy_species_id
where
    tg.taxonomy_family_id = coalesce(:id, tg.taxonomy_family_id)
    and tg.genus_name like :genus and (t.species_name like :species or t.name like :species)
group by 
    tg.taxonomy_genus_id,
    tg.genus_name,
    tg.subgenus_name,
    tg.section_name,
    tg.subsection_name,
    tg.series_name,
    tg.subseries_name
order by
    :orderby
    concat(tg.genus_name, ' ', coalesce(tg.subgenus_name, ''), ' ', coalesce(tg.section_name, ''), ' ', coalesce(tg.subsection_name, ''), ' ', coalesce(tg.series_name, ''), ' ', coalesce(tg.subseries_name,''))