select 
	gm.name,
	c.name as Crop_Name,
	concat('<a href="site.aspx?id=',cast(s.site_id as varchar(50)), '">', s.site_long_name, '</a>') as Site, 
	gm.synonyms as Synonyms, 
	gm.repeat_motif as Repeat_Motif, 
	gm.primers as Primers, 
	gm.assay_conditions as Assay_Conditions, 
	gm.range_products as Range_Products, 
	gm.genbank_number as Genbank_Number, 
	gm.known_standards as Known_Standards, 
    	gm.map_location as Map_location, 
	gm.position as Position, 
	gm.poly_type_code as Polymorphic_Type,
	gm.note as Comment
from 
	crop c 
	join genetic_marker gm on c.crop_id = gm.crop_id
    	join cooperator co on gm.owned_by = co.cooperator_id
    	join site s on co.site_id = s.site_id
where 
	gm.genetic_marker_id = :markerid