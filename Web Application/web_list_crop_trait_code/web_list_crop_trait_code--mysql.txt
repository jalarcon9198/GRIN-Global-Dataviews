SELECT
  ctc.crop_trait_id,
  ctc.code,
  ctcl.sys_lang_id,
  ctcl.title,
  ctcl.description
FROM
    crop_trait_code ctc
    LEFT JOIN crop_trait_code_lang ctcl
      ON  ctcl.crop_trait_code_id = ctc.crop_trait_code_id 
          and  ctcl.sys_lang_id = __LANGUAGEID__ 
where 
	ctc.crop_trait_id = :croptraitid  
	and ctc.crop_trait_code_id = coalesce(:croptraitcodeid, ctc.crop_trait_code_id)


