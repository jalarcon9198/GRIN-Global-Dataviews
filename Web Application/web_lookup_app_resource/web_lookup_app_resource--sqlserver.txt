select
    sys_lang_id,
    app_name,
    form_name,
    app_resource_name,
    description,
    display_member,
    value_member,
    sort_order
from
    app_resource
where
    coalesce(app_name,'\b') = coalesce(:appname, app_name, '\b')
    and coalesce(form_name,'\b') = coalesce(:formname, form_name, '\b')
    and sys_lang_id = coalesce(:langid, sys_lang_id)
order by
    app_name,
    form_name,
    app_resource_name,
    sort_order
