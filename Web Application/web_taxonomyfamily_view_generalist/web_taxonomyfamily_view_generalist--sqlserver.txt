select  ('<a href="taxonomygenus.aspx?id=' + convert(nvarchar, taxonomy_genus_id) +
	(case when tg.series_name IS NOT NULL then '&type=subgenus'   
	      when tg.subsection_name IS NOT NULL then '&type=subsection'  
              when tg.section_name IS NOT NULL then '&type=section'  
	      when tg.subgenus_name IS NOT NULL then '&type=subgenus'
	 else ' '  end) 
    	 + '">' + 
	COALESCE (tg.genus_name, '') + '  '  + COALESCE (tg.genus_authority, '') +
	(case when tg.subgenus_name IS NOT NULL then ' subg. <i>' + tg.subgenus_name + '</i> ' else '' end) +
	(case when tg.section_name IS NOT NULL then ' sect. <i>' + tg.section_name + '</i> ' else '' end) +
	(case when tg.subsection_name IS NOT NULL then ' subs. <i>' + tg.subsection_name + '</i> ' else '' end) +
	(case when tg.series_name IS NOT NULL then ' ser. <i>' + tg.series_name + '</i> ' else '' end) +  '</a>'
	) as name
from 
	taxonomy_genus tg  
	join taxonomy_family tf on tg.taxonomy_family_id = tf.taxonomy_family_id
	and tg.taxonomy_family_id in (
		select taxonomy_family_id from taxonomy_family where :columntype =  
		(select :columntype from taxonomy_family where taxonomy_family_id = :taxonomyfamilyid)) 
order by 
	tg.genus_name