select		
	wora.action_code, 
	cvl.title as action_title,
	wora.acted_date,		
	wora.note
from	
	web_order_request_action  wora
	join code_value cv on wora.action_code = cv.value 
	join code_value_lang cvl on cv.code_value_id = cvl.code_value_id 
where	
	cv.group_name = 'ORDER_REQUEST_ACTION'
	and cvl.sys_lang_id = __LANGUAGEID__
	and wora.web_order_request_id = :orderrequestid

union

select	
	ora.action_name_code as action_code,
	cvl.title as action_title,
	ora.started_date as acted_date,
	ora.note
from	
	web_order_request wor
	join order_request ore on wor.web_order_request_id = ore.web_order_request_id
	join order_request_action ora on ore.order_request_id = ora.order_request_id 
	join code_value cv on ora.action_name_code = cv.value 
	join code_value_lang cvl on cv.code_value_id = cvl.code_value_id 
where
	cv.group_name = 'ORDER_REQUEST_ACTION'
	and cvl.sys_lang_id = __LANGUAGEID__	
	and wor.web_order_request_id = :orderrequestid
 
order by acted_date desc
