SELECT
 distinct
  frg.order_request_id,
  (select (CONVERT(ordered_date, char)) from order_request where order_request_id = frg.order_request_id) as ordered_date,
  (select COUNT(ori2.order_request_item_id) from order_request_item ori2 left join order_request oreq2 on oreq2.order_request_id = ori2.order_request_id where oreq2.requestor_cooperator_id = :cooperatorid AND ori2.order_request_id = frg.order_request_id ) AS item_count,
  CONVERT(((select MIN(frg2.due_date) from feedback_result_group frg2 
								  left join order_request oreq3 on oreq3.order_request_id = frg2.order_request_id 
								where oreq3.requestor_cooperator_id = :cooperatorid AND frg2.order_request_id = frg.order_request_id AND frg2.due_date > NOW())), char(10)) as due_date,
 case 
   when frg.submitted_date IS NOT NULL AND frg.accepted_date IS NULL then 'Submitted: Pending Review'
   when frg.submitted_date IS NOT NULL AND frg.accepted_date IS NOT NULL then 'Accepted'
   when frg.started_date IS NOT NULL AND frg.accepted_date IS NULL then 'Started'
 else ''
 end as status,
concat('<nobr><a href="~/feedback/participantorder.aspx?id=', convert(frg.order_request_id, char), '">Details</a></nobr>') AS link
FROM feedback_result_group frg
    left join order_request_item ori on ori.order_request_id = frg.order_request_id
    left join feedback_inventory fbi on fbi.inventory_id = ori.inventory_id
    left join feedback f on f.feedback_id = fbi.feedback_id
    left join feedback_report fbr on fbr.feedback_id = f.feedback_id
    left join order_request oreq on oreq.feedback_id = fbr.feedback_id
    
 WHERE
 frg.participant_cooperator_id = :cooperatorid AND
 (select MIN(frg2.due_date) from feedback_result_group frg2 
	left join order_request oreq3 on oreq3.order_request_id = frg2.order_request_id 
	where oreq3.requestor_cooperator_id = :cooperatorid AND frg2.order_request_id = frg.order_request_id AND frg2.due_date > NOW()) = due_date