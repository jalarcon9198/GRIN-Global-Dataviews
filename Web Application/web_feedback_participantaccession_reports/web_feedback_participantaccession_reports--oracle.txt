select
frg.feedback_result_group_id,
fb.title AS program_title,
fbr.title AS report_name,
to_char(frg.due_date, 'MM-DD-YYYY') as due_date,
case 
   when frg.submitted_date IS NOT NULL AND frg.accepted_date IS NULL then 'Submitted: Pending Review'
   when frg.submitted_date IS NOT NULL AND frg.accepted_date IS NOT NULL then 'Accepted'
   when frg.started_date IS NOT NULL AND frg.accepted_date IS NULL then 'Started'
 else ''
 end as status

from feedback_result_group frg
left join feedback_result frslt on frslt.feedback_result_group_id = frg.feedback_result_group_id
left join inventory i on i.inventory_id = frslt.inventory_id
left join accession a on a.accession_id = i.accession_id
left join feedback_report fbr on fbr.feedback_report_id = frg.feedback_report_id
left join feedback fb on fb.feedback_id = fbr.feedback_id


where a.accession_id = :accessionid and frg.participant_cooperator_id = :cooperatorid
order by due_date
