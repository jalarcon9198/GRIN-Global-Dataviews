select concat(coalesce(a.accession_number_part1,''), ' ', coalesce(convert(a.accession_number_part2, char), ''), coalesce(a.accession_number_part3,'')) as pi_number,
        coalesce(convert(asrc.latitude, char), '') as latitude,
        coalesce(convert(asrc.longitude, char), '') as longitude,
        coalesce(convert(asrc.collector_verbatim_locality, char), '') as location,
        1 as count, a.accession_id as accession_id 
from accession a 
	join accession_source asrc on a.accession_id = asrc.accession_id
	join 
	(select ascr.latitude, ascr.longitude 
  	from accession_source ascr 
 	where accession_id  in (select a.accession_id from accession a where a.taxonomy_species_id = :taxonomy_species_id)
	and latitude is not null and longitude is not null and is_web_visible= 'Y' 
	group by ascr.latitude, ascr.longitude 
	having COUNT(ascr.accession_source_id) = 1) ascr2 
	on asrc.latitude = ascr2.latitude and asrc.longitude = ascr2.longitude 
	and asrc.accession_id in (select a.accession_id from accession a where a.taxonomy_species_id = :taxonomy_species_id)

union

 select distinct '' as pi_number,
        coalesce(convert(ascr.latitude, char), '') as latitude,
        coalesce(convert(ascr.longitude, char), '') as longitude,
        coalesce(convert(ascr.collector_verbatim_locality, char), '') as location,
        COUNT(ascr.accession_source_id) as count, 0 as accession_id 
 from accession_source ascr 
 where accession_id  in (select a.accession_id from accession a where a.taxonomy_species_id = :taxonomy_species_id)
 	and latitude is not null and longitude is not null and is_web_visible= 'Y' 
 group  by ascr.latitude, ascr.longitude, ascr.collector_verbatim_locality 
 having COUNT(ascr.accession_source_id) > 1	