SELECT
  fff.feedback_form_field_id as fff_id,
  fff.feedback_form_id,
  fff.title,
  fff.description,
  fff.field_type_code,
  fff.gui_hint,
  fff.foreign_key_dataview_name,
  fff.group_name,
  fff.sort_order,
  fff.is_readonly,
  fff.is_required,
  fff.default_value,
  fff.created_date,
  fff.created_by,
  fff.modified_date,
  fff.modified_by,
  fff.owned_date,
  fff.owned_by,
  ff.title as form_title,
  fr.title AS report_title,
  f.title as program_title,
  frg.participant_cooperator_id,
  frg.order_request_id,
  fr1.inventory_id,
  frg.started_date,
  frg.submitted_date,
  frg.accepted_date,
  frg.modified_date AS frg_modified_date,
  frf.feedback_result_id,
  frf.feedback_form_field_id,
  fr1.feedback_result_group_id,
  coalesce(frf.feedback_result_field_id, (fff.feedback_form_field_id  + fr1.inventory_id) * -1) as feedback_result_field_id,
  (select top 1 coalesce(frf2.string_value, fff2.default_value) 
     from feedback_result_field frf2 inner join feedback_result fr2 on frf2.feedback_result_id = fr2.feedback_result_id
	inner join feedback_result_group frg2 on fr2.feedback_result_group_id = frg2.feedback_result_group_id 
       inner join feedback_form_field fff2 on fff2.feedback_form_field_id = frf2.feedback_form_field_id
     where convert(nvarchar, frg2.feedback_report_id) + '|' + convert(nvarchar, fff2.feedback_form_field_id) = fff.references_tag) as other_result_value,
  frf.admin_value,
  frf.string_value,
  coalesce(a.accession_number_part1,'') + ' ' + coalesce(convert(nvarchar, a.accession_number_part2),'') + ' ' + coalesce(a.accession_number_part3,'') as accession_number

FROM 
    feedback_form_field fff
    INNER JOIN feedback_form ff
	on fff.feedback_form_id = ff.feedback_form_id
    INNER JOIN feedback_report fr
	on ff.feedback_form_id = fr.feedback_form_id
    INNER JOIN feedback f
      ON  fr.feedback_id = f.feedback_id
    LEFT JOIN feedback_result_group frg
      ON  frg.feedback_report_id = fr.feedback_report_id
    LEFT JOIN feedback_result fr1
      ON  fr1.feedback_result_group_id = frg.feedback_result_group_id 
    LEFT JOIN feedback_result_field frf
      ON  frf.feedback_result_id = fr1.feedback_result_id 
          and frf.feedback_form_field_id = fff.feedback_form_field_id
    LEFT JOIN inventory i 
      ON  i.inventory_id = fr1.inventory_id
    LEFT JOIN accession a
      ON  a.accession_id = i.accession_id

WHERE fr1.feedback_result_id = coalesce(:feedbackresultid, fr1.feedback_result_id)
and fr1.feedback_result_group_id = coalesce(:feedbackresultgroupid, fr1.feedback_result_group_id)
order by fff.sort_order 