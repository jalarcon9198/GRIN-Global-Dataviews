SELECT
  ts.taxonomy_species_id AS value_member
  ,ts.name AS display_member
FROM 
  taxonomy_species ts
WHERE
ts.taxonomy_species_id = :id
or ts.name like :txt
order by 
2