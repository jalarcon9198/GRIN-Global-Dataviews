SELECT *, :cropid AS crop_id FROM (
SELECT 
	min(citation_id) as min_cid,  
	count(distinct a.accession_id) as cnt,  
	author_name, 
	citation_title, 
	citation_year
FROM accession a
INNER JOIN citation c ON a.accession_id = c.accession_id
WHERE a.accession_id IN 
  (SELECT DISTINCT i.accession_id from
    crop_trait ct
	inner join crop_trait_observation cto ON cto.crop_trait_id = ct.crop_trait_id
	inner join inventory i ON i.inventory_id = cto.inventory_id
    WHERE ct.crop_id = :cropid)
group by author_name, citation_title, citation_year
UNION 
SELECT 
	min(citation_id) as min_cid,  
	count(distinct a.accession_id) as cnt,  
	author_name, 
	citation_title, 
	citation_year
FROM accession a
LEFT JOIN accession_pedigree ap ON a.accession_id = ap.accession_id
INNER JOIN citation c ON ap.accession_pedigree_id = c.accession_pedigree_id
WHERE a.accession_id IN 
  (SELECT DISTINCT i.accession_id from
    crop_trait ct
	inner join crop_trait_observation cto ON cto.crop_trait_id = ct.crop_trait_id
	inner join inventory i ON i.inventory_id = cto.inventory_id
    WHERE ct.crop_id = :cropid)
group by author_name, citation_title, citation_year
UNION
SELECT 
	min(citation_id) as min_cid,  
	count(distinct a.accession_id) as cnt,  
	author_name, 
	citation_title, 
	citation_year
FROM accession a
INNER JOIN accession_ipr ai ON a.accession_id = ai.accession_id
INNER JOIN citation c ON ai.accession_ipr_id = c.accession_ipr_id
WHERE a.accession_id IN 
  (SELECT DISTINCT i.accession_id from
    crop_trait ct
	inner join crop_trait_observation cto ON cto.crop_trait_id = ct.crop_trait_id
	inner join inventory i ON i.inventory_id = cto.inventory_id
    WHERE ct.crop_id = :cropid)
group by author_name, citation_title, citation_year
) AS A
ORDER BY 
	COALESCE(author_name, 'zzzz'), citation_title, citation_year