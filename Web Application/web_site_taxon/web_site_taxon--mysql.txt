select 
	ts.name, 
	a.taxonomy_species_id,
	c.site_id,
	count(distinct a.accession_id) as cnt
from 
	accession a 
	join taxonomy_species ts on a.taxonomy_species_id = ts.taxonomy_species_id 
	join cooperator c on a.owned_by = c.cooperator_id
group by 
	a.taxonomy_species_id, ts.name, c.site_id, a.is_web_visible
having 
	c.site_id = :siteid
	and a.is_web_visible = 'Y'
order by 
	ts.name