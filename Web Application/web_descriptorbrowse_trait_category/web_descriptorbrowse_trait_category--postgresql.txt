select 
    ct.category_code,
    coalesce(cvl_ct.title, ct.category_code) as category_title,
    concat(coalesce(cvl_ct.title, ct.category_code), ' (', ct.category_code, ')') as display_text
from 
    crop_trait ct
    left join code_value cv_ct
	on cv_ct.value = ct.category_code
         and cv_ct.group_name = 'DESCRIPTOR_CATEGORY'
    left join code_value_lang cvl_ct
        on cv_ct.code_value_id = cvl_ct.code_value_id
        and cvl_ct.sys_lang_id = :langid
    join crop_trait_observation cto
        on ct.crop_trait_id = cto.crop_trait_id
where 
    ct.crop_id = :cropid
    and cv_ct.code_value_id is not null
    and cto.is_archived = 'N'
group by
    ct.category_code,
    coalesce(cvl_ct.title, ct.category_code)
order by  
    ct.category_code,
    coalesce(cvl_ct.title, ct.category_code)
