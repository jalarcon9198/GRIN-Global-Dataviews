SELECT
  fft.feedback_form_trait_id,
  fft.feedback_form_id,
  fft.crop_trait_id,
  fft.sort_order,
  fft.references_tag,
  fft.title,
  fft.description,
  fft.created_date,
  fft.created_by,
  fft.modified_date,
  fft.modified_by,
  fft.owned_date,
  fft.owned_by
FROM
    feedback_form_trait fft


WHERE fft.feedback_form_id = coalesce(:feedbackformid, fft.feedback_form_id)

