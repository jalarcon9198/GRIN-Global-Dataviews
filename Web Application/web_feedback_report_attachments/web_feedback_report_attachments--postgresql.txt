SELECT
  fra.feedback_report_attach_id,
  fra.feedback_report_id,
  fra.virtual_path,
  fra.title,
  fra.content_type,
  fra.is_web_visible
FROM
    feedback_report_attach fra

WHERE fra.feedback_report_id = coalesce(:feedbackreportid, fra.feedback_report_id)
and fra.feedback_report_attach_id = coalesce(:feedbackreportattachid, fra.feedback_report_attach_id)
