select 
	web_order_request_attach_id as id, 
	coalesce(modified_date, created_date) as uploaddate, 
	virtual_path as filePath
from 
	web_order_request_attach
where 
	web_order_request_id = :orderid  