select
 	a.accession_id,
 	concat('<nobr><a onclick="javascript:return true;" href="~/accessiondetail.aspx?id=', convert(nvarchar, a.accession_id), '">', trim(CONCAT (COALESCE (a.accession_number_part1, ''), ' ', COALESCE (CONVERT(nvarchar, a.accession_number_part2), ''), ' ', COALESCE (a.accession_number_part3, ''))), '</a></nobr>') as pi_number,
 	(select TOP 1 plant_name from accession_inv_name an join inventory i on an.inventory_id = i.inventory_id where a.accession_id = i.accession_id and plant_name_rank = (select MIN(plant_name_rank) from accession_inv_name an2 join inventory i2 on an2.inventory_id = i2.inventory_id where a.accession_id = i2.accession_id)) as top_name,
 	'<a href="~/taxonomydetail.aspx?id=' + convert(nvarchar, t.taxonomy_species_id) + '">' + t.name + '</a>' as taxonomy_name,
	(select cvl.title
   	from code_value cv left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id
   	where cv.group_name = 'GEOGRAPHY_COUNTRY_CODE'  and cvl.sys_lang_id = __LANGUAGEID__ 
   	and cv.value = (select top 1 g.country_code from geography g join accession_source asr on g.geography_id = asr.geography_id
   	and asr.is_origin = 'Y' and asr.accession_id = a.accession_id)) as origin,
 	(select top 1 coalesce(cvl.description, cv.value) from code_value cv left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id where cv.group_name = 'GERMPLASM_FORM' and cv.value = i.form_type_code) as material_type,
  	concat('<a href="site.aspx?id=', convert(nvarchar, s.site_id), '">', s.site_short_name , '</a>') as maintained_by,
 	case when exists (select inventory_id from inventory where is_distributable = 'Y' AND  is_available = 'Y' and accession_id = a.accession_id) then
		case 
			when sum(case when i.is_distributable = 'Y' AND i.is_available = 'Y' then 1 else 0 end) = 0 then ''
			when t.restriction_code = 'NARCOTIC' then '<nobr><font color="red">Not Available</font></nobr>'
			when t.restriction_code in ('WEED', 'RARE') then '<nobr><font color="orange">Contact Site</font></nobr>'
 		else 		
			concat('<nobr><a href="~/cartview.aspx?action=add&id=', convert(nvarchar, a.accession_id), '">Add to Cart</a></nobr>')
		end 	
	else
 		'<nobr><font color="red">Not Available</font></nobr>'
 	end as availability
from
 	accession a
 	left join taxonomy_species t 
  		on a.taxonomy_species_id = t.taxonomy_species_id
 	left join inventory i
  		on i.accession_id = a.accession_id
     		and i.form_type_code != '**'
	left join cooperator c on a.owned_by = c.cooperator_id
	left join site s on c.site_id = s.site_id
where 
	t.taxonomy_species_id = :taxonomyid
	and a.is_web_visible = 'Y' 
        and a.status_code = 'ACTIVE'
group by
 	a.accession_id,
 	concat('<nobr><a onclick="javascript:return true;" href="~/accessiondetail.aspx?id=', convert(nvarchar, a.accession_id), '">', trim(CONCAT (COALESCE (a.accession_number_part1, ''), ' ', COALESCE (CONVERT(nvarchar, a.accession_number_part2), ''), ' ', COALESCE (a.accession_number_part3, ''))), '</a></nobr>'),
 	t.taxonomy_species_id,
 	t.name,
	t.restriction_code,
 	i.form_type_code,
 	concat('<a href="site.aspx?id=', convert(nvarchar, s.site_id), '">', s.site_short_name , '</a>')
order by
	1