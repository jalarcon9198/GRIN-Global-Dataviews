select
  r.subcontinent,
  tgm.taxonomy_species_id,
  tgm.geography_status_code 
FROM taxonomy_geography_map tgm
  left join geography g
    on tgm.geography_id = g.geography_id
  left join geography_region_map grm
    on g.geography_id = grm.geography_id
  left join region r
    on grm.region_id = r.region_id
where taxonomy_species_id = :taxonomyid
   and coalesce(r.continent,'') = coalesce(:continent,'')
   and geography_status_code = :geostatuscode
group by r.subcontinent, tgm.taxonomy_species_id, tgm.geography_status_code 