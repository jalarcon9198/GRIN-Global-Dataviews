SELECT
  fr.feedback_id,
  fr.feedback_report_id,
  fr.title as report_title,
  fr.due_interval,
  fr.sort_order,
  fr.interval_length_code,
  fr.interval_type_code,
  ff.title AS form_title,
  cv.code_value_id,
  cvl.title AS interval_length_title,
  cv1.code_value_id AS cv1_code_value_id,
  cvl1.title AS interval_type_title
FROM
    feedback_report fr
    LEFT JOIN feedback_form ff
      ON  fr.feedback_form_id = ff.feedback_form_id 
    LEFT JOIN code_value cv
      ON  cv.value = fr.interval_length_code 
    LEFT JOIN code_value_lang cvl
      ON  cvl.code_value_id = cv.code_value_id  
          and  cvl.sys_lang_id = __LANGUAGEID__ 
    LEFT JOIN code_value cv1
      ON  cv1.value = fr.interval_type_code 
    LEFT JOIN code_value_lang cvl1
      ON  cvl1.code_value_id = cv1.code_value_id 
          and  cvl1.sys_lang_id = __LANGUAGEID__ 

WHERE fr.feedback_id = :feedbackid
order by fr.sort_order