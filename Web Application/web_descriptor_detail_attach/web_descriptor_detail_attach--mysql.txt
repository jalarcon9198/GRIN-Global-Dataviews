select
  crop_trait_attach_id 
  ,COALESCE (virtual_path, '') as virtual_path
  ,COALESCE (thumbnail_virtual_path, virtual_path, '') as thumbnail_virtual_path
  ,concat(convert(cta.accession_inv_attach_id, char), ', ', COALESCE (cta.title, ''), ', ', COALESCE (co.last_name, ''), ', ', COALESCE (co.first_name, ''), ', ', COALESCE (co.organization, ''), 'cta') as title
  ,COALESCE (description, '') as description
  ,cta.note
from
   crop_trait_attach cta
	left join cooperator co
		on cta.attach_cooperator_id = co.cooperator_id
where
  crop_trait_id = :traitid
  and is_web_visible = 'Y'
  and cta.category_code = :categorycode
order by sort_order