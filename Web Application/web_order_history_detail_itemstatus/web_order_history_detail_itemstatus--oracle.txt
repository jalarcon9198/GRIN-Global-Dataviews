select
	wori.web_order_request_item_id, 
	coalesce(ori.status_code, wori.status_code) as status_code 
from 
	order_request_item ori 
		join web_order_request_item wori on wori.web_order_request_item_id = ori.web_order_request_item_id 
		join web_order_request wor on wori.web_order_request_id = wor.web_order_request_id
where 
	wor.web_order_request_id = :orderrequestid
	and ori.status_code != 'NEW'
