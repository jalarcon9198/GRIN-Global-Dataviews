select distinct
	c.literature_id,
	COALESCE (c.author_name, l.editor_author_name) as author,
	c.citation_year,
	COALESCE (c.citation_title, l.reference_title) as title,
	case when (COALESCE (l.standard_abbreviation, l.abbreviation)) = l.abbreviation then '(' + l.abbreviation + ')'
		else l.standard_abbreviation
		end
	as abbrev,
	COALESCE (c.reference, '') as reference,
	c.note as cit_note,
	l.note
from citation c
	left join literature l
		on c.literature_id = l.literature_id
	join taxonomy_species t 
		on c.taxonomy_species_id = c.taxonomy_species_id 
where    
	c.note like 'relative%'  
	and c.taxonomy_species_id = :taxonomyid
order by author asc
