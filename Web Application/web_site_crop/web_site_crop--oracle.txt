select distinct
	cr.name,
	concat('<nobr><a onclick="javascript:return true;" href="crop.aspx?id=', convert(nvarchar, cr.crop_id), '">', cr.name, '</a></nobr>') as crop_name
from 
	crop cr
	join crop_trait ct on cr.crop_id = ct.crop_id
	join cooperator c on cr.owned_by = c.cooperator_id
where
	c.site_id = :siteid

union

select distinct
	cr.name,
	concat('<nobr><a onclick="javascript:return true;" href="crop.aspx?id=', convert(nvarchar, cr.crop_id), '">', cr.name, '</a></nobr>') as crop_name
from 
	crop cr
	join genetic_marker gm on cr.crop_id = gm.crop_id
	join cooperator c on gm.owned_by = c.cooperator_id
where 
	c.site_id = :siteid

order by 
	name