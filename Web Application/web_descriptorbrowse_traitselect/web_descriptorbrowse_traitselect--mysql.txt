select
    concat('<nobr><a href="descriptordetail.aspx?id=', convert(ct.crop_trait_id, char), '" title="', coalesce(ctl.description, ctl.title, ct.coded_name), '">', coalesce(ct.coded_name, ctl.title), '</a></nobr>') as crop_trait_name,
    ct.crop_trait_id
    , count(distinct i.accession_id) as accession_count
from
    crop_trait ct
    left join crop_trait_lang ctl
        on ct.crop_trait_id = ctl.crop_trait_id
        and ctl.sys_lang_id = :langid
    left join crop_trait_observation cto
        on ct.crop_trait_id = cto.crop_trait_id
    left join inventory i
        on cto.inventory_id = i.inventory_id
where
    ct.crop_trait_id in (:traitids) and cto.is_archived = 'N'
group by
    ct.crop_trait_id,
    coalesce(ctl.title, ct.coded_name),
    ctl.title,
    ct.coded_name,
    ctl.description  
order by
    coalesce(ctl.title, ct.coded_name)