select 
   action_name_code, started_date, completed_date, note
from 
   accession_action 
where 
   action_name_code in (:actionName)
   and is_web_visible = 'Y'
   and accession_id = :accessionid