SELECT
  fft.feedback_form_trait_id,
  fft.feedback_form_id,
  fr.feedback_report_id,
  fft.crop_trait_id,
  c.crop_id as crop_id,
  c.name as crop_name,
  ct.coded_name,
  ct.max_length,
  ct.is_coded,
  ct.data_type_code,
  case when fft.crop_trait_id is null then
   (select concat(fr2.title, ' - ', fft2.title) from feedback_form_trait fft2 inner join feedback_report fr2 on fft2.feedback_form_id = fr2.feedback_form_id where concat(convert(fr2.feedback_report_id, char), '|', convert(fft2.feedback_form_trait_id, char)) = fft.references_tag limit 1) 
  else 
   ctl.title 
  end as trait_title,
  fft.title as custom_title,
  fft.description as custom_description,
  fft.references_tag,
  fft.sort_order,
  fft.created_date,
  fft.created_by,
  fft.modified_date,
  fft.modified_by,
  fft.owned_date,
  fft.owned_by 
FROM
    feedback_form_trait fft
	left join feedback_report fr 
	on fft.feedback_form_id = fr.feedback_form_id
	left join crop_trait ct on fft.crop_trait_id = ct.crop_trait_id
	left join crop c on ct.crop_id = c.crop_id
	left join crop_trait_lang ctl on ct.crop_trait_id = ctl.crop_trait_id and ctl.sys_lang_id = __LANGUAGEID__
where fft.feedback_form_id = :feedbackformid
and fft.feedback_form_trait_id = coalesce(:feedbackformtraitid, fft.feedback_form_trait_id)
order by fft.sort_order 
