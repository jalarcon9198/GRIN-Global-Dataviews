select top 1 
	coalesce(i.web_availability_note, imp.web_availability_note) as availability_note from accession a 
left join inventory i on a.accession_id = i.accession_id
	join inventory_maint_policy imp on i.inventory_maint_policy_id = imp.inventory_maint_policy_id 
where a.accession_id = :accessionid 
and (i.web_availability_note is not null or imp.web_availability_note is not null) 
order by availability_note 