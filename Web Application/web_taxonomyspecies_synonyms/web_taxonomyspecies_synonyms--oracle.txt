select 
t.taxonomy_species_id, 
t.current_taxonomy_species_id, 
t.synonym_code, 
t.name,
COALESCE (t.forma_authority, t.subvariety_authority, t.variety_authority, t.subspecies_authority, t.species_authority, '') 
as authority
from taxonomy_species t
where t.current_taxonomy_species_id = :taxonomyid and t.synonym_code IS NOT NULL and t.taxonomy_species_id != :taxonomyid 
order by t.name