SELECT
	m.method_id, 
	m.name, 
	cto.crop_trait_id,
	(select 
		count(a.accession_id) 
   	from 
		accession a
   	join inventory i 
   		on a.accession_id = i.accession_id 
   	and i.inventory_id in 
   		(select 
			inventory_id 
   		from 
			crop_trait_observation
   		where 
			crop_trait_id = :descriptorid
   		and method_id = m.method_id)) as cnt 
from method m 
	join crop_trait_observation cto 
		on m.method_id = cto.method_id 
where 
	cto.crop_trait_id = :descriptorid
group by 
	m.method_id, m.name, cto.crop_trait_id
order by m.name
