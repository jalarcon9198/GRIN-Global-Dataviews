select 
	taxonomy_family_id as id,
	family_name as name
from
	taxonomy_family
where
	subfamily_name is null and 
	tribe_name is null and
	subtribe_name is null
order by
	family_name
