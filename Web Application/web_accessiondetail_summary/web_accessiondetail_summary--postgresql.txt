select 
    concat(coalesce(g.adm1,''), ' ', (select cvl.title from code_value cv 
                left join code_value_lang cvl 
					on cv.code_value_id = cvl.code_value_id 
					where cv.group_name = 'GEOGRAPHY_COUNTRY_CODE' and cvl.sys_lang_id = __LANGUAGEID__ and cv.value = (select g.country_code from geography g join accession_source asr on 	       
               g.geography_id = asr.geography_id
               and asr.accession_id = :accessionid limit 1))) as developed_in,
    case when a.is_backed_up = 'Y' then sb1.site_long_name else '' end as backup_location,
    a.life_form_code,
    a.improvement_status_code,
    a.reproductive_uniformity_code,
    a.initial_received_form_code,
    a.initial_received_date,
    case when a.status_code = 'INACTIVE' then '<a href="site.aspx?id=-1">Not in the NPGS Collection.</a>'
    	else concat('<a href="site.aspx?id=',cast(s.site_id as varchar(50)), '">', s.site_long_name, '</a>') end as site_code,
   (select cvl.description from code_value cv left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id 
where cv.group_name = 'GERMPLASM_FORM' and cvl.sys_lang_id = __LANGUAGEID__ and cv.value = a.initial_received_form_code) as initial_material_type_desc,
    (select quantity_on_hand from inventory where accession_id = :accessionid AND is_distributable = 'Y' limit 1) as 
inventory_volume,
    ap.description as pedigree,
    asrc.source_type_code,
    a.status_code
from
    accession a
    left join accession_source asrc
        on a.accession_id = asrc.accession_id
    left join geography g
        on asrc.geography_id = g.geography_id
    left join inventory i
		on a.accession_id = i.accession_id
    left join accession_pedigree ap
        on a.accession_id = ap.accession_id
	left join cooperator c on a.owned_by = c.cooperator_id
	left join site s on c.site_id = s.site_id
	left join site sb1
	on a.backup_location1_site_id = sb1.site_id
where
    a.accession_id = :accessionid
limit 1