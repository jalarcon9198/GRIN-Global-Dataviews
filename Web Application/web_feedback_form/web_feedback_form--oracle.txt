SELECT
  ff.feedback_form_id,
  ff.title,
  ff.created_date,
  ff.created_by,
  ff.modified_date,
  ff.modified_by,
  ff.owned_date,
  ff.owned_by,
  coalesce(cc.first_name,'') || ' ' || coalesce(cc.last_name,'') as created_by_name,
  coalesce(mc.first_name,'') || ' ' || coalesce(mc.last_name,'') as modified_by_name,
  fr.feedback_report_id,
  fr.feedback_id,
  fr.title as report_name,
  f.title as program_title,
  (select count(*) from feedback_result_group frg where frg.feedback_report_id = fr.feedback_report_id) as result_count

FROM
    feedback_form ff
left join cooperator cc on ff.created_by = cc.cooperator_id
left join cooperator mc on ff.modified_by = mc.cooperator_id
left join feedback_report fr on ff.feedback_form_id = fr.feedback_form_id
left join feedback f on f.feedback_id = fr.feedback_id
where ff.feedback_form_id = coalesce(:feedbackformid, ff.feedback_form_id)
and ff.owned_by = coalesce(:cooperatorid, ff.owned_by)
order by ff.title 

 