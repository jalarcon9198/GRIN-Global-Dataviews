select distinct
	a.accession_id, 
	wor.status_code as order_status, 
	wori.web_order_request_item_id,
	wori.status_code,
	wori.distribution_form_code,
	concat(coalesce(a.accession_number_part1,''), ' ', coalesce(cast(a.accession_number_part2 as varchar),''), ' ', coalesce(a.accession_number_part3,'')) as pi_number,
	s.site_short_name as site,
	s.site_id,
	(select plant_name from accession_inv_name an join inventory i on an.inventory_id = i.inventory_id where a.accession_id = i.accession_id and plant_name_rank = (select MIN(plant_name_rank) from accession_inv_name an2 join inventory i2 on an2.inventory_id = i2.inventory_id where a.accession_id = i2.accession_id) limit 1) as top_name,
	a.taxonomy_species_id, (tg.genus_name || ' ' || t.species_name) as taxonomy_name, 
	(select coalesce(aipr.type_code, '') from accession_ipr aipr where aipr.accession_id = a.accession_id and type_code like 'MTA%') as type_code, wori.user_note as note 
from accession a  
	left join taxonomy_species t on a.taxonomy_species_id = t.taxonomy_species_id 
	left join taxonomy_genus tg on t.taxonomy_genus_id = tg.taxonomy_genus_id
	left join cooperator c on a.owned_by = c.cooperator_id
	left join site s on c.site_id = s.site_id
	left join web_order_request_item wori on a.accession_id = wori.accession_id 
	left join web_order_request wor on wori.web_order_request_id = wor.web_order_request_id
where 
	wori.web_order_request_id = :orderrequestid
order by 
	accession_id asc