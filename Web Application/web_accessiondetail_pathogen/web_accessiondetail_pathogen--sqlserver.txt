select distinct 
	iqs.inventory_quality_status_id,
	iqs.test_type_code,
	trim(CONCAT (COALESCE (i.inventory_number_part1, ''), ' ', COALESCE (CONVERT(nvarchar, i.inventory_number_part2), ''), ' ', COALESCE (i.inventory_number_part3, ''), ' ', COALESCE(i.form_type_code, ''))) as inventory_material,
	(select cvl.title from code_value cv left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id 
   		where cv.group_name = 'PATHOLOGY_TEST_TYPE' and cvl.sys_lang_id = __LANGUAGEID__ 
   		and cv.value = iqs.test_type_code) as test_type,
   	(select cvl.title from code_value cv left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id 
   		where cv.group_name = 'PATHOLOGY_TEST' and cvl.sys_lang_id = __LANGUAGEID__ 
   		and cv.value = iqs.contaminant_code) as test_contaminant,
        iqs.test_result_code,
   	iqs.required_replication_count,
   	iqs.started_count,
   	iqs.completed_count,
   	iqs.completed_date,
   	iqs.completed_date_code,
	iqs.started_date,
	iqs.started_date_code,
   	iqs.note 
	 
from 
	inventory_quality_status iqs
	join inventory i on iqs.inventory_id = i.inventory_id 
	join accession a on i.accession_id = a.accession_id 
	join cooperator c on a.owned_by = c.cooperator_id
	join site s on c.site_id = s.site_id
where 
	a.accession_id = :accessionid  
	and s.site_short_name in ('COR', 'HILO', 'PGQO')
order by 
	iqs.test_type_code,
	trim(CONCAT (COALESCE (i.inventory_number_part1, ''), ' ', COALESCE (CONVERT(nvarchar, i.inventory_number_part2), ''), ' ', COALESCE (i.inventory_number_part3, ''), ' ', COALESCE(i.form_type_code, '')))
  	

