SELECT
  distinct
  oreq.order_request_id,
    (coalesce(a.accession_number_part1,'') || ' ' || coalesce(cast(a.accession_number_part2 as varchar(50)), '') || coalesce(a.accession_number_part3,'')) as pi_number,

  cast(oreq.ordered_date as varchar(10)) as ordered_date, 
  f.title AS program_title, 
concat('<a href="~/feedback/participantresult.aspx?groupid=', cast(fres.feedback_result_group_id as varchar(50)), '">', fbr.title, '</a>') AS report_name,
cast(frg.due_date as varchar(10)) as due_date,
  case 
   when frg.submitted_date IS NOT NULL AND frg.accepted_date IS NULL then 'Submitted: Pending Review'
   when frg.submitted_date IS NOT NULL AND frg.accepted_date IS NOT NULL then 'Accepted'
   when frg.started_date IS NOT NULL AND frg.accepted_date IS NULL then 'Started'
 else ''
 end as status,
   ltrim(rtrim(coalesce(c.last_name,'') || ', ' || coalesce(c.first_name,''))) as requestor_name
FROM
    feedback f
    LEFT JOIN feedback_inventory fi
      ON  fi.feedback_id = f.feedback_id 
    inner JOIN order_request_item ori
      ON  fi.inventory_id = ori.inventory_id 
    LEFT JOIN order_request oreq
      ON  ori.order_request_id = oreq.order_request_id 
    LEFT JOIN cooperator c
      ON  oreq.requestor_cooperator_id = c.cooperator_id 
	left join feedback_report fbr
	on fbr.feedback_id = f.feedback_id
	left join feedback_result_group frg
	on frg.feedback_report_id = fbr.feedback_report_id
	   and frg.order_request_id = oreq.order_request_id 
	   and frg.participant_cooperator_id = oreq.requestor_cooperator_id
	left join feedback_result fres
	   on fres.inventory_id = fi.inventory_id
	      and fres.feedback_result_group_id = frg.feedback_result_group_id
	   left join inventory i on fi.inventory_id = i.inventory_id
	   left join accession a on i.accession_id = a.accession_id
WHERE frg.feedback_result_group_id IS NOT NULL
AND f.owned_by = :cooperatorid
ORDER BY oreq.order_request_id DESC