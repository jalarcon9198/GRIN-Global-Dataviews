select
	tcm.taxonomy_species_id, 
	tcm.common_crop_name,
	t.name,
	'<i>' + COALESCE (tg.genus_name, '') + ' ' +
	COALESCE (t.species_name, '') + '</i> ' +
	COALESCE (t.species_authority, '') + ' ' +
	(case when t.subspecies_name IS NOT NULL then 'subsp. <i>' + t.subspecies_name + '</i> ' + COALESCE (t.subspecies_authority, '') + ' ' else '' end) +
	(case when t.variety_name IS NOT NULL then 'var. <i>' + t.variety_name + '</i> ' + COALESCE (t.variety_authority, '') + ' ' else '' end) +
	(case when t.forma_name IS NOT NULL then 'forma <i>' + t.forma_name + '</i> ' + COALESCE (t.forma_authority, '') + ' ' else '' end) as taxonomy_name,
	case when tcm.common_crop_name  = tcm.alternate_crop_name  THEN 1 else 2  end as firstorder
from 
	taxonomy_crop_map tcm
	join taxonomy_species t 
		on tcm.taxonomy_species_id = t.taxonomy_species_id
	join taxonomy_genus tg
		on t.taxonomy_genus_id = tg.taxonomy_genus_id 
where 
	tcm.alternate_crop_name = :acn
	and tcm.common_crop_name is not null
order by 5,2