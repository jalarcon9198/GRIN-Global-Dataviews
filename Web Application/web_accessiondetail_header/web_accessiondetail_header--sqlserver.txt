select 
    concat(coalesce(a.accession_number_part1,''), ' ', coalesce(convert(varchar, a.accession_number_part2), ''), ' ', coalesce(a.accession_number_part3,'')) as pi_number,
    COALESCE (tg.genus_name, '') + ' ' +
	COALESCE (t.species_name, '') + ' ' +
	COALESCE (t.species_authority, '') + ' ' +
		(case when t.subspecies_name IS NOT NULL then 'subsp. ' + t.subspecies_name + ' ' + COALESCE (t.subspecies_authority, '') + ' ' else '' end) +
	(case when t.variety_name IS NOT NULL then 'var. ' + t.variety_name + ' ' + COALESCE (t.variety_authority, '') + ' ' else '' end) +
	(case when t.forma_name IS NOT NULL then t.forma_name + ' ' + COALESCE (t.forma_authority, '') + ' ' else '' end)as taxonomy_name_built,
    t.taxonomy_species_id,
    (select top 1 an.plant_name from accession_inv_name an join inventory i on an.inventory_id = i.inventory_id where i.accession_id = :accessionid AND an.category_code = 'CULTIVAR') as cultivar 
from
    accession a
    left join taxonomy_species t 
		on a.taxonomy_species_id = t.taxonomy_species_id
	left join taxonomy_genus tg
		on t.taxonomy_genus_id = tg.taxonomy_genus_id
	left join taxonomy_family tf
		on tg.taxonomy_family_id = tf.taxonomy_family_id
where 
    a.accession_id = :accessionid
 