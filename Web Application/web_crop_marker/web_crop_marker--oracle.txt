select 
	gm.genetic_marker_id, 
	gm.name, 
	gm.poly_type_code as poly_type,
	s.site_id,
	s.site_short_name
from 
	genetic_marker gm 
	join cooperator c 
		on gm.owned_by = c.cooperator_id
	join site s 
		on c.site_id = s.site_id
where
	gm.crop_id = :cropid
order by
	gm.name, gm.poly_type_code 