select distinct
	asrc.source_type_code as type_code,
	asrc.accession_source_id,
	asrc.source_date,
	asrc.source_date_code,
	LTRIM(RTRIM(concat(g.adm4, ' ', g.adm3, ' ', g.adm2, ' ', g.adm1))) as state_full_name,
	(select cvl.title 
                from code_value cv left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id
                where cv.group_name = 'GEOGRAPHY_COUNTRY_CODE' and cvl.sys_lang_id = __LANGUAGEID__ 
               and cv.value = (select g.country_code from geography g join accession_source asr on g.geography_id = asr.geography_id
               and asr.accession_id = :accessionid  limit 1) limit 1) as country_name,
        collector_verbatim_locality as verbatim,
        environment_description as habitat,
        quantity_collected,
        unit_quantity_collected_code,
	(case when (COALESCE(cast(asrc.latitude as varchar(50)), '\b') = '\b') or (COALESCE(cast(asrc.longitude as varchar(50)), '\b') = '\b') then ' '
		else ('Locality: ' || COALESCE (asrc.formatted_locality, '') 
		    || ' Latitude: '  
		    || cast(cast(asrc.latitude as integer) as varchar(50))
		    || ' deg. ' 
		    || cast(CAST((asrc.latitude - cast(asrc.latitude as integer)) * 60 as integer) as varchar(50)) 
		    || ' min. '
		    || cast(((asrc.latitude - CAST(asrc.latitude as integer)) * 60 - CAST((asrc.latitude - cast(asrc.latitude as integer)) * 60 as integer)) * 60 as varchar(50))
		    || ' sec.' ||  (case when asrc.latitude > 0 then ' North ' else ' South ' end) || '(' || cast(asrc.latitude as varchar(50)) || ')'   
		    || ', '
		    || 'Longitude: ' 
		    || cast(cast(asrc.longitude as integer) as varchar(50))
		    || ' deg. ' 
		    || cast(CAST((asrc.longitude - cast(asrc.longitude as integer)) * 60 as integer) as varchar(50)) 
		    || ' min. '
		    || cast(((asrc.longitude - CAST(asrc.longitude as integer)) * 60 - CAST((asrc.longitude - cast(asrc.longitude as integer)) * 60 as integer)) * 60 as varchar(50))
		    || ' sec.' || (case when asrc.longitude > 0 then ' East ' else ' West ' end) ||  '(' || cast(asrc.longitude as varchar(50)) || ') '   
		    || '<a onclick=''javascript:return true;'' href=''maps.aspx?id=' || cast(asrc.accession_id as varchar(50)) || '''> GoogleMap </a>' )
		    end) as locality, 
	asrc.elevation_meters,
	asrc.note,
	case when asrc.is_web_visible = 'N' or exists (select tu.economic_usage_code from accession a join taxonomy_use tu on a.taxonomy_species_id = tu.taxonomy_species_id where tu.economic_usage_code in ('FWT', 'FWE', 'CITESI', 'CITESII', 'CITESIII') and a.accession_id = :accessionid) then 'Y' else 'N' end as Nolocation
from 
	accession_source asrc
	left join geography g on asrc.geography_id = g.geography_id
	left join code_value cv on cv.value = g.country_code
	left join code_value_lang cvl on cvl.code_value_id = cv.code_value_id
	join accession a on asrc.accession_id = a.accession_id
where 
	asrc.accession_id = :accessionid
	and coalesce(cv.group_name, 'GEOGRAPHY_COUNTRY_CODE') = 'GEOGRAPHY_COUNTRY_CODE'
	and coalesce(cvl.sys_lang_id, __LANGUAGEID__) = __LANGUAGEID__