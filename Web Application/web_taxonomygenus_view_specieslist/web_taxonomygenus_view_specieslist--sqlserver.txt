select 
	('<a href="taxonomydetail.aspx?id=' + convert(nvarchar, taxonomy_species_id) + '">' + 
	'<i>' + COALESCE (tg.genus_name, '') + ' ' +
	COALESCE (t.species_name, '') + '</i> ' +
	COALESCE (t.species_authority, '') + ' ' +
	(case when t.subspecies_name IS NOT NULL then 'subsp. <i>' + t.subspecies_name + '</i> ' + COALESCE (t.subspecies_authority, '') + ' ' else '' end) +
	(case when t.variety_name IS NOT NULL then 'var. <i>' + t.variety_name + '</i> ' + COALESCE (t.variety_authority, '') + ' ' else '' end) +
	(case when t.subvariety_name IS NOT NULL then 'var. <i>' + t.subvariety_name + '</i> ' + COALESCE (t.subvariety_authority, '') + ' ' else '' end) +
	(case when t.forma_name IS NOT NULL then 'forma <i>' + t.forma_name + '</i> ' + COALESCE (t.forma_authority, '') + ' ' else '' end)) as name
from 
	taxonomy_species t 
	join taxonomy_genus tg on t.taxonomy_genus_id = tg.taxonomy_genus_id
	and t.taxonomy_genus_id in
	(select taxonomy_genus_id from taxonomy_genus where :columntype =
		(select :columntype from taxonomy_genus where taxonomy_genus_id = :taxonomygenusid))
order by 
	t.species_name