select
	c.citation_id,
	c.literature_id,
	COALESCE (c.author_name, l.editor_author_name) as author,
	c.citation_year,
	COALESCE (c.citation_title, l.reference_title) as title,
	case when (COALESCE (l.standard_abbreviation, l.abbreviation)) = l.abbreviation then '(' + l.abbreviation + ')'
		else l.standard_abbreviation
		end
	 as abbrev,
	COALESCE (c.reference, '') as refernece,
	c.note as cit_note,
	l.note
from  
	citation c
	left join literature l
		on c.literature_id = l.literature_id
where 
	c.method_id = :methodid

Union

select
	c.citation_id,
	c.literature_id,
	COALESCE (c.author_name, l.editor_author_name) as author,
	c.citation_year,
	COALESCE (c.citation_title, l.reference_title) as title,
	case when (COALESCE (l.standard_abbreviation, l.abbreviation)) = l.abbreviation then '(' + l.abbreviation + ')'
		else l.standard_abbreviation
		end
	 as abbrev,
	COALESCE (c.reference, '') as refernece,
	c.note as cit_note,
	l.note
from  
	citation c
	left join literature l
		on c.literature_id = l.literature_id
where 
	c.accession_ipr_id = :iprid

Union

select
	c.citation_id,
	c.literature_id,
	COALESCE (c.author_name, l.editor_author_name) as author,
	c.citation_year,
	COALESCE (c.citation_title, l.reference_title) as title,
	case when (COALESCE (l.standard_abbreviation, l.abbreviation)) = l.abbreviation then '(' + l.abbreviation + ')'
		else l.standard_abbreviation
		end
	 as abbrev,
	COALESCE (c.reference, '') as refernece,
	c.note as cit_note,
	l.note
from  
	citation c
	left join literature l
		on c.literature_id = l.literature_id
where 
	c.accession_pedigree_id = :pedigreeid

Union

select
	c.citation_id,
	c.literature_id,
	COALESCE (c.author_name, l.editor_author_name) as author,
	c.citation_year,
	COALESCE (c.citation_title, l.reference_title) as title,
	case when (COALESCE (l.standard_abbreviation, l.abbreviation)) = l.abbreviation then '(' + l.abbreviation + ')'
		else l.standard_abbreviation
		end
	 as abbrev,
	COALESCE (c.reference, '') as refernece,
	c.note as cit_note,
	l.note
from  
	citation c
	left join literature l
		on c.literature_id = l.literature_id
where 
	c.genetic_marker_id = :markerid

order by author asc