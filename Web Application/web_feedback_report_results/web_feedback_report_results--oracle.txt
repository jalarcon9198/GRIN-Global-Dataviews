SELECT
  frg.feedback_result_group_id,
  fr.feedback_result_id,
  frg.feedback_report_id,
  frg.participant_cooperator_id,
  frg.order_request_id,
  fr.inventory_id,
  fr.created_date,
  fr.created_by,
  fr.modified_date,
  fr.modified_by,
  fr.owned_date,
  fr.owned_by
FROM
	feedback_result_group frg
	left join feedback_result fr
		on frg.feedback_result_group_id = fr.feedback_result_group_id


