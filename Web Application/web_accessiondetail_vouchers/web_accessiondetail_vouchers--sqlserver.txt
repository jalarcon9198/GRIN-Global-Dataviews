select
    av.accession_inv_voucher_id as id,	    
    'Herbarium specimen' as v_type,
    av.vouchered_date,
    coop.cooperator_id,
    COALESCE (coop.last_name, '') + ', ' + COALESCE (coop.first_name, '') + ', ' + COALESCE (coop.organization, '') as taken_by,
    COALESCE (av.collector_voucher_number, '') as caption,
    av.voucher_location as location,
    COALESCE (i.inventory_number_part1, '') + ' ' + COALESCE (CAST (i.inventory_number_part2 as NVARCHAR), '') + ' ' + COALESCE (i.inventory_number_part3, '') as inv_sample,
    COALESCE(av.note, '') as note
from accession_inv_voucher av
    left join cooperator coop
        on av.collector_cooperator_id = coop.cooperator_id
    left join inventory i
        on av.inventory_id = i.inventory_id
where i.accession_id = :accessionid

 