select 
	m.name,
   	coalesce(g.adm1, '') as state_name,
   	coalesce (cvl.title, g.country_code, '') as country_name,
   	m.materials_and_methods as methods
from 
	method m 
    	left join geography g
		on m.geography_id = g.geography_id 
	left join code_value cv 
		on cv.value = g.country_code
	left join code_value_lang cvl 
		on cvl.code_value_id = cv.code_value_id
where
	m.method_id = :methodid
	and coalesce(cv.group_name, 'GEOGRAPHY_COUNTRY_CODE') = 'GEOGRAPHY_COUNTRY_CODE'
	and coalesce(cvl.sys_lang_id, __LANGUAGEID__) = __LANGUAGEID__
	and coalesce(cvl.sys_lang_id, __LANGUAGEID__) = __LANGUAGEID__