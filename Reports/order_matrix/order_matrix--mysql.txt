SELECT o.order_type_code,CAST(YEAR(o.completed_date) as CHAR) AS year,c.category_code,
  s.site_short_name, g.country_code, COUNT(*) AS count
FROM
    order_request o
    LEFT JOIN order_request_item i
      ON  o.order_request_id = i.order_request_id 
    LEFT JOIN cooperator c
      ON  o.final_recipient_cooperator_id = c.cooperator_id 
    LEFT JOIN site s
      ON  c.site_id = s.site_id 
    LEFT JOIN geography g
      ON  c.geography_id = g.geography_id 
WHERE i.status_code in ('SHIPPED') and YEAR(o.completed_date) > 1995
    and c.category_code is not null
GROUP by o.order_type_code,YEAR(o.completed_date), c.category_code, s.site_short_name, g.country_code
ORDER by o.order_type_code,YEAR(o.completed_date), c.category_code, s.site_short_name, g.country_code