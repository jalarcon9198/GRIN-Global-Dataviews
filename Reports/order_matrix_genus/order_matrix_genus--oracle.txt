select o.order_type_code,to_char(o.completed_date,'YYYY')as year,tg.genus_name, COUNT(*)as count
from cooperator c,order_request o,order_request_item i,inventory inv,
	accession a, taxonomy_species ts, taxonomy_genus tg
where o.order_request_id = i.order_request_id and
	o.order_type_code = 'DI' and i.status_code = 'SHIPPED'
	and to_char(o.completed_date,'YYYY') > 1995
	and o.final_recipient_cooperator_id = c.cooperator_id 
	and i.inventory_id = inv.inventory_id and inv.accession_id = a.accession_id
	and a.taxonomy_species_id = ts.taxonomy_species_id 
	and ts.taxonomy_genus_id = tg.taxonomy_genus_id
        and tg.genus_name is not null
group by o.order_type_code,to_char(o.completed_date,'YYYY'),tg.genus_name
order by o.order_type_code,to_char(o.completed_date,'YYYY'),tg.genus_name