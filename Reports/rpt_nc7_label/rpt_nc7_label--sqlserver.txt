SELECT 
   i.inventory_id
  ,i.inventory_number_part1
  ,i.inventory_number_part2
  ,i.inventory_number_part3
  ,i.form_type_code
  ,i.accession_id
  ,i.inventory_maint_policy_id
  ,(select s.site_id from site s where s.site_id in (select c.site_id from cooperator c where c.cooperator_id = i.owned_by)) AS site_id
  ,i.is_distributable
  ,i.is_auto_deducted
  ,i.is_available
  ,i.availability_status_code
  ,i.availability_status_note
  ,i.availability_start_date
  ,i.availability_end_date
  ,i.web_availability_note
  ,i.quantity_on_hand
  ,i.quantity_on_hand_unit_code
  ,i.distribution_default_form_code
  ,i.distribution_default_quantity
  ,i.distribution_unit_code
  ,i.distribution_critical_quantity
  ,i.regeneration_critical_quantity
  ,i.pathogen_status_code
  ,i.storage_location_part1
  ,i.storage_location_part2
  ,i.storage_location_part3
  ,i.storage_location_part4
  ,i.latitude
  ,i.longitude
  ,i.rootstock
  ,i.parent_inventory_id
  ,i.backup_inventory_id
  ,i.hundred_seed_weight
  ,i.pollination_method_code
  ,i.pollination_vector_code
  ,i.preservation_method_id
  ,i.regeneration_method_id
  ,i.plant_sex_code
  ,i.propagation_date_code
  ,i.propagation_date
  ,i.note
  ,(SELECT TOP(1) plant_name FROM accession_inv_name ain
     INNER JOIN inventory i2 ON i2.inventory_id = ain.inventory_id WHERE i.accession_id = i2.accession_id
     ORDER BY ain.plant_name_rank + CASE WHEN i2.form_type_code = '**' THEN 0 WHEN i2.inventory_id = i.inventory_id THEN 10000 ELSE 100000 END) AS top_name
  ,(SELECT TOP(1) plant_name FROM accession_inv_name ain
     INNER JOIN inventory i2 ON i2.inventory_id = ain.inventory_id WHERE i.accession_id = i2.accession_id AND i2.form_type_code = '**'
     ORDER BY ain.plant_name_rank + CASE WHEN i2.form_type_code = '**' THEN 0 WHEN i2.inventory_id = i.inventory_id THEN 10000 ELSE 100000 END) AS accession_name
  ,(SELECT SUBSTRING((SELECT '; ' + ain.plant_name AS [text()] FROM accession_inv_name ain INNER JOIN inventory i2 ON i2.inventory_id = ain.inventory_id WHERE i.accession_id = i2.accession_id AND i2.form_type_code = '**' ORDER BY ain.plant_name_rank + CASE WHEN i2.form_type_code = '**' THEN 0 WHEN i2.inventory_id = i.inventory_id THEN 10000 ELSE 100000 END FOR XML PATH ('')), 2, 1000)) AS plant_name
  ,(SELECT SUBSTRING((SELECT '; ' + ain.plant_name AS [text()] FROM accession_inv_name ain INNER JOIN inventory i2 ON i2.inventory_id = ain.inventory_id WHERE i.accession_id = i2.accession_id AND i2.inventory_id = i.inventory_id ORDER BY ain.plant_name_rank + CASE WHEN i2.form_type_code = '**' THEN 0 WHEN i2.inventory_id = i.inventory_id THEN 10000 ELSE 100000 END FOR XML PATH ('')), 2, 1000)) AS inventory_name
  ,(SELECT SUBSTRING((SELECT '; ' + ain.plant_name AS [text()] FROM accession_inv_name ain INNER JOIN inventory i2 ON i2.inventory_id = ain.inventory_id WHERE i.accession_id = i2.accession_id ORDER BY ain.plant_name_rank + CASE WHEN i2.form_type_code = '**' THEN 0 WHEN i2.inventory_id = i.inventory_id THEN 10000 ELSE 100000 END FOR XML PATH ('')), 2, 1000)) AS all_name
  ,(SELECT taxonomy_species_id FROM accession a WHERE i.accession_id = a.accession_id) AS taxonomy_species_id
  ,(SELECT s.geography_id FROM accession_source s WHERE i.accession_id = s.accession_id AND accession_source_id = (SELECT MIN(accession_source_id) FROM accession_source s2 WHERE i.accession_id = s2.accession_id AND is_origin = 'Y')) AS geography_id
  ,(SELECT TOP 1 percent_viable FROM inventory_viability iv WHERE iv.inventory_id = i.inventory_id
     ORDER BY iv.tested_date DESC, inventory_viability_id DESC) AS percent_viable
  ,(SELECT TOP 1 tested_date FROM inventory_viability iv WHERE iv.inventory_id = i.inventory_id
     ORDER BY iv.tested_date DESC) AS tested_date
  ,(SELECT TOP(1) aig.group_name FROM accession_inv_group aig 
     INNER JOIN accession_inv_group_map aigm ON aig.accession_inv_group_id = aigm.accession_inv_group_id AND i.inventory_id = aigm.inventory_id
     ORDER BY aig.accession_inv_group_id DESC) AS group_name
  ,(SELECT a.initial_received_date FROM accession a WHERE i.accession_id = a.accession_id) AS initial_received_date
  ,(SELECT MIN(asrc.source_date) FROM accession_source asrc WHERE i.accession_id = asrc.accession_id) AS source_date
  ,nc7.nc7_site_inventory_id
  ,nc7.pollination_control_code
  ,nc7.farm_field_identifier_code
  ,nc7.location_type_code
  ,nc7.location_low
  ,nc7.location_high
  ,nc7.sublocation_type_code
  ,nc7.sublocation_low
  ,nc7.sublocation_high
  ,nc7.old_inventory_identifier
  ,nc7.inventory_site_note
  ,nc7.inventory_location1_latitude
  ,nc7.inventory_location1_longitude
  ,nc7.inventory_location1_precision
  ,nc7.inventory_location2_latitude
  ,nc7.inventory_location2_longitude
  ,nc7.inventory_location2_precision
  ,nc7.inventory_datum_code
  ,nc7.coordinates_apply_to_code
  ,nc7.coordinates_comment
  ,nc7.coordinates_voucher_location
  ,nc7.irregular_inventory_location
  ,nc7.is_increase_success_flag
  ,nc7.reason_unsuccessfull1_code
  ,nc7.reason_unsuccessfull2_code
  ,nc7.reason_unsuccessfull3_code
  ,nc7.reason_unsuccessfull_note
  ,ia.started_date
  ,ia.action_name_code
FROM inventory i
LEFT JOIN nc7_site_inventory nc7 ON i.inventory_id = nc7.inventory_id
LEFT JOIN inventory_action ia ON i.inventory_id = ia.inventory_id AND ia.action_name_code = 'received'
WHERE i.inventory_id IN (:inventoryid)

UNION

SELECT 
   i.inventory_id
  ,i.inventory_number_part1
  ,i.inventory_number_part2
  ,i.inventory_number_part3
  ,i.form_type_code
  ,i.accession_id
  ,i.inventory_maint_policy_id
  ,(select s.site_id from site s where s.site_id in (select c.site_id from cooperator c where c.cooperator_id = i.owned_by)) AS site_id
  ,i.is_distributable
  ,i.is_auto_deducted
  ,i.is_available
  ,i.availability_status_code
  ,i.availability_status_note
  ,i.availability_start_date
  ,i.availability_end_date
  ,i.web_availability_note
  ,i.quantity_on_hand
  ,i.quantity_on_hand_unit_code
  ,i.distribution_default_form_code
  ,i.distribution_default_quantity
  ,i.distribution_unit_code
  ,i.distribution_critical_quantity
  ,i.regeneration_critical_quantity
  ,i.pathogen_status_code
  ,i.storage_location_part1
  ,i.storage_location_part2
  ,i.storage_location_part3
  ,i.storage_location_part4
  ,i.latitude
  ,i.longitude
  ,i.rootstock
  ,i.parent_inventory_id
  ,i.backup_inventory_id
  ,i.hundred_seed_weight
  ,i.pollination_method_code
  ,i.pollination_vector_code
  ,i.preservation_method_id
  ,i.regeneration_method_id
  ,i.plant_sex_code
  ,i.propagation_date_code
  ,i.propagation_date
  ,i.note
  ,(SELECT TOP(1) plant_name FROM accession_inv_name ain
     INNER JOIN inventory i2 ON i2.inventory_id = ain.inventory_id WHERE i.accession_id = i2.accession_id
     ORDER BY ain.plant_name_rank + CASE WHEN i2.form_type_code = '**' THEN 0 WHEN i2.inventory_id = i.inventory_id THEN 10000 ELSE 100000 END) AS top_name
  ,(SELECT TOP(1) plant_name FROM accession_inv_name ain
     INNER JOIN inventory i2 ON i2.inventory_id = ain.inventory_id WHERE i.accession_id = i2.accession_id AND i2.form_type_code = '**'
     ORDER BY ain.plant_name_rank + CASE WHEN i2.form_type_code = '**' THEN 0 WHEN i2.inventory_id = i.inventory_id THEN 10000 ELSE 100000 END) AS accession_name
  ,(SELECT SUBSTRING((SELECT '; ' + ain.plant_name AS [text()] FROM accession_inv_name ain INNER JOIN inventory i2 ON i2.inventory_id = ain.inventory_id WHERE i.accession_id = i2.accession_id AND i2.form_type_code = '**' ORDER BY ain.plant_name_rank + CASE WHEN i2.form_type_code = '**' THEN 0 WHEN i2.inventory_id = i.inventory_id THEN 10000 ELSE 100000 END FOR XML PATH ('')), 2, 1000)) AS plant_name
  ,(SELECT SUBSTRING((SELECT '; ' + ain.plant_name AS [text()] FROM accession_inv_name ain INNER JOIN inventory i2 ON i2.inventory_id = ain.inventory_id WHERE i.accession_id = i2.accession_id AND i2.inventory_id = i.inventory_id ORDER BY ain.plant_name_rank + CASE WHEN i2.form_type_code = '**' THEN 0 WHEN i2.inventory_id = i.inventory_id THEN 10000 ELSE 100000 END FOR XML PATH ('')), 2, 1000)) AS inventory_name
  ,(SELECT SUBSTRING((SELECT '; ' + ain.plant_name AS [text()] FROM accession_inv_name ain INNER JOIN inventory i2 ON i2.inventory_id = ain.inventory_id WHERE i.accession_id = i2.accession_id ORDER BY ain.plant_name_rank + CASE WHEN i2.form_type_code = '**' THEN 0 WHEN i2.inventory_id = i.inventory_id THEN 10000 ELSE 100000 END FOR XML PATH ('')), 2, 1000)) AS all_name
  ,(SELECT taxonomy_species_id FROM accession a WHERE i.accession_id = a.accession_id) AS taxonomy_species_id
  ,(SELECT s.geography_id FROM accession_source s WHERE i.accession_id = s.accession_id AND accession_source_id = (SELECT MIN(accession_source_id) FROM accession_source s2 WHERE i.accession_id = s2.accession_id AND is_origin = 'Y')) AS geography_id
  ,(SELECT TOP 1 percent_viable FROM inventory_viability iv WHERE iv.inventory_id = i.inventory_id
     ORDER BY iv.tested_date DESC, inventory_viability_id DESC) AS percent_viable
  ,(SELECT TOP 1 tested_date FROM inventory_viability iv WHERE iv.inventory_id = i.inventory_id
     ORDER BY iv.tested_date DESC) AS tested_date
  ,(SELECT TOP(1) aig.group_name FROM accession_inv_group aig 
     INNER JOIN accession_inv_group_map aigm ON aig.accession_inv_group_id = aigm.accession_inv_group_id AND i.inventory_id = aigm.inventory_id
     ORDER BY aig.accession_inv_group_id DESC) AS group_name
  ,(SELECT a.initial_received_date FROM accession a WHERE i.accession_id = a.accession_id) AS initial_received_date
  ,(SELECT MIN(asrc.source_date) FROM accession_source asrc WHERE i.accession_id = asrc.accession_id) AS source_date
  ,nc7.nc7_site_inventory_id
  ,nc7.pollination_control_code
  ,nc7.farm_field_identifier_code
  ,nc7.location_type_code
  ,nc7.location_low
  ,nc7.location_high
  ,nc7.sublocation_type_code
  ,nc7.sublocation_low
  ,nc7.sublocation_high
  ,nc7.old_inventory_identifier
  ,nc7.inventory_site_note
  ,nc7.inventory_location1_latitude
  ,nc7.inventory_location1_longitude
  ,nc7.inventory_location1_precision
  ,nc7.inventory_location2_latitude
  ,nc7.inventory_location2_longitude
  ,nc7.inventory_location2_precision
  ,nc7.inventory_datum_code
  ,nc7.coordinates_apply_to_code
  ,nc7.coordinates_comment
  ,nc7.coordinates_voucher_location
  ,nc7.irregular_inventory_location
  ,nc7.is_increase_success_flag
  ,nc7.reason_unsuccessfull1_code
  ,nc7.reason_unsuccessfull2_code
  ,nc7.reason_unsuccessfull3_code
  ,nc7.reason_unsuccessfull_note
  ,ia.started_date
  ,ia.action_name_code
FROM inventory i
LEFT JOIN nc7_site_inventory nc7 ON i.inventory_id = nc7.inventory_id
LEFT JOIN inventory_action ia ON i.inventory_id = ia.inventory_id AND ia.action_name_code = 'received'
WHERE i.accession_id IN (:accessionid)
UNION

SELECT 
   i.inventory_id
  ,i.inventory_number_part1
  ,i.inventory_number_part2
  ,i.inventory_number_part3
  ,i.form_type_code
  ,i.accession_id
  ,i.inventory_maint_policy_id
  ,(select s.site_id from site s where s.site_id in (select c.site_id from cooperator c where c.cooperator_id = i.owned_by)) AS site_id
  ,i.is_distributable
  ,i.is_auto_deducted
  ,i.is_available
  ,i.availability_status_code
  ,i.availability_status_note
  ,i.availability_start_date
  ,i.availability_end_date
  ,i.web_availability_note
  ,i.quantity_on_hand
  ,i.quantity_on_hand_unit_code
  ,i.distribution_default_form_code
  ,i.distribution_default_quantity
  ,i.distribution_unit_code
  ,i.distribution_critical_quantity
  ,i.regeneration_critical_quantity
  ,i.pathogen_status_code
  ,i.storage_location_part1
  ,i.storage_location_part2
  ,i.storage_location_part3
  ,i.storage_location_part4
  ,i.latitude
  ,i.longitude
  ,i.rootstock
  ,i.parent_inventory_id
  ,i.backup_inventory_id
  ,i.hundred_seed_weight
  ,i.pollination_method_code
  ,i.pollination_vector_code
  ,i.preservation_method_id
  ,i.regeneration_method_id
  ,i.plant_sex_code
  ,i.propagation_date_code
  ,i.propagation_date
  ,i.note
  ,(SELECT TOP(1) plant_name FROM accession_inv_name ain
     INNER JOIN inventory i2 ON i2.inventory_id = ain.inventory_id WHERE i.accession_id = i2.accession_id
     ORDER BY ain.plant_name_rank + CASE WHEN i2.form_type_code = '**' THEN 0 WHEN i2.inventory_id = i.inventory_id THEN 10000 ELSE 100000 END) AS top_name
  ,(SELECT TOP(1) plant_name FROM accession_inv_name ain
     INNER JOIN inventory i2 ON i2.inventory_id = ain.inventory_id WHERE i.accession_id = i2.accession_id AND i2.form_type_code = '**'
     ORDER BY ain.plant_name_rank + CASE WHEN i2.form_type_code = '**' THEN 0 WHEN i2.inventory_id = i.inventory_id THEN 10000 ELSE 100000 END) AS accession_name
  ,(SELECT SUBSTRING((SELECT '; ' + ain.plant_name AS [text()] FROM accession_inv_name ain INNER JOIN inventory i2 ON i2.inventory_id = ain.inventory_id WHERE i.accession_id = i2.accession_id AND i2.form_type_code = '**' ORDER BY ain.plant_name_rank + CASE WHEN i2.form_type_code = '**' THEN 0 WHEN i2.inventory_id = i.inventory_id THEN 10000 ELSE 100000 END FOR XML PATH ('')), 2, 1000)) AS plant_name
  ,(SELECT SUBSTRING((SELECT '; ' + ain.plant_name AS [text()] FROM accession_inv_name ain INNER JOIN inventory i2 ON i2.inventory_id = ain.inventory_id WHERE i.accession_id = i2.accession_id AND i2.inventory_id = i.inventory_id ORDER BY ain.plant_name_rank + CASE WHEN i2.form_type_code = '**' THEN 0 WHEN i2.inventory_id = i.inventory_id THEN 10000 ELSE 100000 END FOR XML PATH ('')), 2, 1000)) AS inventory_name
  ,(SELECT SUBSTRING((SELECT '; ' + ain.plant_name AS [text()] FROM accession_inv_name ain INNER JOIN inventory i2 ON i2.inventory_id = ain.inventory_id WHERE i.accession_id = i2.accession_id ORDER BY ain.plant_name_rank + CASE WHEN i2.form_type_code = '**' THEN 0 WHEN i2.inventory_id = i.inventory_id THEN 10000 ELSE 100000 END FOR XML PATH ('')), 2, 1000)) AS all_name
  ,(SELECT taxonomy_species_id FROM accession a WHERE i.accession_id = a.accession_id) AS taxonomy_species_id
  ,(SELECT s.geography_id FROM accession_source s WHERE i.accession_id = s.accession_id AND accession_source_id = (SELECT MIN(accession_source_id) FROM accession_source s2 WHERE i.accession_id = s2.accession_id AND is_origin = 'Y')) AS geography_id
  ,(SELECT TOP 1 percent_viable FROM inventory_viability iv WHERE iv.inventory_id = i.inventory_id
     ORDER BY iv.tested_date DESC, inventory_viability_id DESC) AS percent_viable
  ,(SELECT TOP 1 tested_date FROM inventory_viability iv WHERE iv.inventory_id = i.inventory_id
     ORDER BY iv.tested_date DESC) AS tested_date
  ,(SELECT TOP(1) aig.group_name FROM accession_inv_group aig 
     INNER JOIN accession_inv_group_map aigm ON aig.accession_inv_group_id = aigm.accession_inv_group_id AND i.inventory_id = aigm.inventory_id
     ORDER BY aig.accession_inv_group_id DESC) AS group_name
  ,(SELECT a.initial_received_date FROM accession a WHERE i.accession_id = a.accession_id) AS initial_received_date
  ,(SELECT MIN(asrc.source_date) FROM accession_source asrc WHERE i.accession_id = asrc.accession_id) AS source_date
  ,nc7.nc7_site_inventory_id
  ,nc7.pollination_control_code
  ,nc7.farm_field_identifier_code
  ,nc7.location_type_code
  ,nc7.location_low
  ,nc7.location_high
  ,nc7.sublocation_type_code
  ,nc7.sublocation_low
  ,nc7.sublocation_high
  ,nc7.old_inventory_identifier
  ,nc7.inventory_site_note
  ,nc7.inventory_location1_latitude
  ,nc7.inventory_location1_longitude
  ,nc7.inventory_location1_precision
  ,nc7.inventory_location2_latitude
  ,nc7.inventory_location2_longitude
  ,nc7.inventory_location2_precision
  ,nc7.inventory_datum_code
  ,nc7.coordinates_apply_to_code
  ,nc7.coordinates_comment
  ,nc7.coordinates_voucher_location
  ,nc7.irregular_inventory_location
  ,nc7.is_increase_success_flag
  ,nc7.reason_unsuccessfull1_code
  ,nc7.reason_unsuccessfull2_code
  ,nc7.reason_unsuccessfull3_code
  ,nc7.reason_unsuccessfull_note
  ,ia.started_date
  ,ia.action_name_code 
FROM inventory i
LEFT JOIN nc7_site_inventory nc7 ON i.inventory_id = nc7.inventory_id
LEFT JOIN accession_inv_group_map aigm ON i.inventory_id = aigm.inventory_id
LEFT JOIN accession_inv_group aig ON aigm.accession_inv_group_id = aig.accession_inv_group_id
LEFT JOIN inventory_action ia ON i.inventory_id = ia.inventory_id AND ia.action_name_code = 'received'
WHERE aig.accession_inv_group_id IN (:accessioninvgroupid)
