SELECT
  oi.order_request_item_id,
  oi.order_request_id,
  oi.sequence_number,
  i.accession_id,
  oi.inventory_id,
  oi.name,
  oi.external_taxonomy,
  oi.quantity_shipped,
  oi.quantity_shipped_unit_code,
  oi.note,
(select s.geography_id from accession_source s where a.accession_id = s.accession_id and s.accession_source_id = ( select MIN(s2.accession_source_id) from accession_source s2 where a.accession_id = s2.accession_id and is_origin = 'Y')) AS geography_id,
    (SELECT c1.last_name) AS last_name_1,
  (SELECT c1.first_name) AS first_name_1,
  (SELECT c1.address_line1) AS address_line1_1,
  (SELECT c1.address_line2) AS address_line2_1,
  (SELECT c1.address_line3) AS address_line3_1,
  (SELECT c1.city) AS city_1,
  (SELECT g1.adm1) AS adm1_1,
  (SELECT c1.postal_index) AS postal_index_1,
  (SELECT g1.country_code) AS country_code_1,
  (SELECT c2.last_name) AS last_name_2,
  (SELECT c2.first_name) AS first_name_2,
  (SELECT c2.address_line1) AS address_line1_2,
  (SELECT c2.address_line2) AS address_line2_2,
  (SELECT c2.address_line3) AS address_line3_2,
  (SELECT c2.city) AS city_2,
  (SELECT g2.adm1) AS adm1_2,
  (SELECT c2.postal_index) AS postal_index_2,
  (SELECT g2.country_code) AS country_code_2,
  (SELECT c3.last_name) AS last_name_3,
  (SELECT c3.first_name) AS first_name_3,
  (SELECT c3.address_line1) AS address_line1_3,
  (SELECT c3.address_line2) AS address_line2_3,
  (SELECT c3.address_line3) AS address_line3_3,
  (SELECT c3.city) AS city_3,
  (SELECT g3.adm1) AS adm1_3,
  (SELECT c3.postal_index) AS postal_index_3,
  (SELECT g3.country_code) AS country_code_3,
  oi.created_date,
  oi.created_by,
  oi.modified_date,
  oi.modified_by,
  oi.owned_date,
  oi.owned_by
FROM
    order_request_item AS oi,
    order_request AS o,
    inventory AS i,
    accession AS a,
    cooperator AS c1,
    cooperator AS c2,
    cooperator AS c3,
    geography g1,
    geography g2,
    geography g3
WHERE
    oi.inventory_id = i.inventory_id and
    i.accession_id = a.accession_id and
    o.order_request_id = oi.order_request_id and
    o.requestor_cooperator_id = c1.cooperator_id and
    g1.geography_id = c1.geography_id and
    o.ship_to_cooperator_id = c2.cooperator_id and
    g2.geography_id = c2.geography_id and
    o.final_recipient_cooperator_id = c3.cooperator_id and
    g3.geography_id = c3.geography_id and
    i.accession_id in (:accessionid)
UNION
SELECT
  oi.order_request_item_id,
  oi.order_request_id,
  oi.sequence_number,
  i.accession_id,
  oi.inventory_id,
  oi.name,
  oi.external_taxonomy,
  oi.quantity_shipped,
  oi.quantity_shipped_unit_code,
  oi.note,
(select s.geography_id from accession_source s where a.accession_id = s.accession_id and s.accession_source_id = ( select MIN(s2.accession_source_id) from accession_source s2 where a.accession_id = s2.accession_id and is_origin = 'Y')) AS geography_id,
    (SELECT c1.last_name) AS last_name_1,
  (SELECT c1.first_name) AS first_name_1,
  (SELECT c1.address_line1) AS address_line1_1,
  (SELECT c1.address_line2) AS address_line2_1,
  (SELECT c1.address_line3) AS address_line3_1,
  (SELECT c1.city) AS city_1,
  (SELECT g1.adm1) AS adm1_1,
  (SELECT c1.postal_index) AS postal_index_1,
  (SELECT g1.country_code) AS country_code_1,
  (SELECT c2.last_name) AS last_name_2,
  (SELECT c2.first_name) AS first_name_2,
  (SELECT c2.address_line1) AS address_line1_2,
  (SELECT c2.address_line2) AS address_line2_2,
  (SELECT c2.address_line3) AS address_line3_2,
  (SELECT c2.city) AS city_2,
  (SELECT g2.adm1) AS adm1_2,
  (SELECT c2.postal_index) AS postal_index_2,
  (SELECT g2.country_code) AS country_code_2,
  (SELECT c3.last_name) AS last_name_3,
  (SELECT c3.first_name) AS first_name_3,
  (SELECT c3.address_line1) AS address_line1_3,
  (SELECT c3.address_line2) AS address_line2_3,
  (SELECT c3.address_line3) AS address_line3_3,
  (SELECT c3.city) AS city_3,
  (SELECT g3.adm1) AS adm1_3,
  (SELECT c3.postal_index) AS postal_index_3,
  (SELECT g3.country_code) AS country_code_3,
  oi.created_date,
  oi.created_by,
  oi.modified_date,
  oi.modified_by,
  oi.owned_date,
  oi.owned_by
FROM
    order_request_item AS oi,
    order_request AS o,
    inventory AS i,
    accession AS a,
    cooperator AS c1,
    cooperator AS c2,
    cooperator AS c3,
    geography g1,
    geography g2,
    geography g3
WHERE
    oi.inventory_id = i.inventory_id and
    i.accession_id = a.accession_id and
    o.order_request_id = oi.order_request_id and
    o.requestor_cooperator_id = c1.cooperator_id and
    g1.geography_id = c1.geography_id and
    o.ship_to_cooperator_id = c2.cooperator_id and
    g2.geography_id = c2.geography_id and
    o.final_recipient_cooperator_id = c3.cooperator_id and
    g3.geography_id = c3.geography_id and
    oi.inventory_id in (:inventoryid)
UNION
SELECT
  oi.order_request_item_id,
  oi.order_request_id,
  oi.sequence_number,
  i.accession_id,
  oi.inventory_id,
  oi.name,
  oi.external_taxonomy,
  oi.quantity_shipped,
  oi.quantity_shipped_unit_code,
  oi.note,
(select s.geography_id from accession_source s where a.accession_id = s.accession_id and s.accession_source_id = ( select MIN(s2.accession_source_id) from accession_source s2 where a.accession_id = s2.accession_id and is_origin = 'Y')) AS geography_id,
    (SELECT c1.last_name) AS last_name_1,
  (SELECT c1.first_name) AS first_name_1,
  (SELECT c1.address_line1) AS address_line1_1,
  (SELECT c1.address_line2) AS address_line2_1,
  (SELECT c1.address_line3) AS address_line3_1,
  (SELECT c1.city) AS city_1,
  (SELECT g1.adm1) AS adm1_1,
  (SELECT c1.postal_index) AS postal_index_1,
  (SELECT g1.country_code) AS country_code_1,
  (SELECT c2.last_name) AS last_name_2,
  (SELECT c2.first_name) AS first_name_2,
  (SELECT c2.address_line1) AS address_line1_2,
  (SELECT c2.address_line2) AS address_line2_2,
  (SELECT c2.address_line3) AS address_line3_2,
  (SELECT c2.city) AS city_2,
  (SELECT g2.adm1) AS adm1_2,
  (SELECT c2.postal_index) AS postal_index_2,
  (SELECT g2.country_code) AS country_code_2,
  (SELECT c3.last_name) AS last_name_3,
  (SELECT c3.first_name) AS first_name_3,
  (SELECT c3.address_line1) AS address_line1_3,
  (SELECT c3.address_line2) AS address_line2_3,
  (SELECT c3.address_line3) AS address_line3_3,
  (SELECT c3.city) AS city_3,
  (SELECT g3.adm1) AS adm1_3,
  (SELECT c3.postal_index) AS postal_index_3,
  (SELECT g3.country_code) AS country_code_3,
  oi.created_date,
  oi.created_by,
  oi.modified_date,
  oi.modified_by,
  oi.owned_date,
  oi.owned_by
FROM
    order_request_item AS oi,
    order_request AS o,
    inventory AS i,
    accession AS a,
    cooperator AS c1,
    cooperator AS c2,
    cooperator AS c3,
    geography g1,
    geography g2,
    geography g3
WHERE
    oi.inventory_id = i.inventory_id and
    i.accession_id = a.accession_id and
    o.order_request_id = oi.order_request_id and
    o.requestor_cooperator_id = c1.cooperator_id and
    g1.geography_id = c1.geography_id and
    o.ship_to_cooperator_id = c2.cooperator_id and
    g2.geography_id = c2.geography_id and
    o.final_recipient_cooperator_id = c3.cooperator_id and
    g3.geography_id = c3.geography_id and
    oi.order_request_id in (:orderrequestid)
